#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#ordenar_imgs.py

import os
import datetime

def nombre_correg_fecha(directorio):
   '''
   Devuelve una tupla de 3 listas: 1ra el path de todos los 
   archivos .png, 2da nombres sin fechas de los .png y 
   3ra la fecha en timestamp.
   Pre: path del directorio donde quiero buscar los png
   Pos: 3 listas con el path, nombre corregido y fecha(timestamp)
   de los archivos png.
   '''
    path_archivos = []
    nombres_corregidos = []
    fechas = []
    for root, dirs, files in os.walk(directorio):
        for name in files:
            if name[-3:] == 'png': #el name es un str, y lo manejo como tal
                path_archivos.append(os.path.join(root, name))
                fecha = datetime.datetime.strptime(name[-12:-4], '%Y%m%d')
                fechas.append(fecha.timestamp())
                nombres_corregidos.append(name[:-13] + name[-4:])
    return path_archivos, nombres_corregidos, fechas

def modificar_mover(directorio_inicial, directorio_final):
    '''
    Modifica la fecha de los archivos .png segun lo indicado en 
    el nombre, los renombra y los mueve a el directorio_final
    indicado (lo crea si no existe).
    Pre: directorio_inicial donde se deben buscar los .png y el
    directorio_final de destino.
    Pos: Carpeta segun directorio_final con los archivos .png
    renombrados y con la fecha de acceso y modificación cambiados.
    '''
    archivos, nombre, fecha = nombre_correg_fecha(directorio_inicial)
    archivos_png = zip(archivos, nombre, fecha) #creo un zip con las tres listas
    
    if not os.path.isdir(directorio_final): #Si el directorio_final no existe lo crea
        os.mkdir(directorio_final)
    
    for archivo, nombre, fecha in archivos_png:
        os.utime(archivo, (fecha, fecha)) #modifico la fecha
        os.rename(archivo, os.path.join(directorio_final, nombre)) #renombro y muevo el archivo
    
    for root, dirs, files in os.walk(directorio_inicial, topdown = False):
        for name in dirs:
            path = os.path.join(root,name)
            if len(os.listdir(path)) == 0: #Si el directorio está vacío lo elimino
                os.rmdir(path)
    print('Done.')

def main(parametros):
    if len(parametros) != 3:
        raise SystemExit(f'Uso adecuado: {sys.argv[0]} ' 'ej:./Data/ordenar' 'ej:./Data/nuevo_directorio')
    modificar_mover(parametros[1], parametros[2])   


if __name__ == '__main__':
    import sys
    main(sys.argv)

            