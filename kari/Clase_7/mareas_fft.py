#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scipy import signal 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

directorio1 = os.path.join('./Data', 'OBS_SHN_SF-BA.csv')
df = pd.read_csv(directorio1, index_col = ['Time'], parse_dates = True)

directorio2 = os.path.join('./Data', 'OBS_Zarate_2013A.csv')
df_z = pd.read_csv(directorio2, index_col = ['Time'], parse_dates = True)


inicio = '2013-01'
fin = '2013-06'
alturas_za = df_z[inicio:fin]['H_Zarate'].to_numpy()
alturas_ba = df[inicio:fin]['H_BA'].to_numpy()

def calcular_fft(y, freq_sampleo = 24.0):
    '''y debe ser un vector con números reales
    representando datos de una serie temporal.
    freq_sampleo está seteado para considerar 24 datos por unidad.
    Devuelve dos vectores, uno de frecuencias 
    y otro con la transformada propiamente.
    La transformada contiene los valores complejos
    que se corresponden con respectivas frecuencias.'''
    N = len(y)
    freq = np.fft.fftfreq(N, d = 1/freq_sampleo)[:N//2]
    tran = (np.fft.fft(y)/N)[:N//2]
    return freq, tran


freq_ba, fft_ba = calcular_fft(alturas_ba)

plt.plot(freq_ba, np.abs(fft_ba))
plt.xlabel("Frecuencia")
plt.ylabel("Potencia (energía)")
plt.xlim(0,4)
plt.ylim(0,20)
# me quedo solo con el último pico
pico_ba = signal.find_peaks(np.abs(fft_ba), prominence = 8)[0][-1]
#se grafican los picos como circulitos rojos
plt.scatter(freq_ba[pico_ba], np.abs(fft_ba)[pico_ba], facecolor='r')
plt.title("Espectro de Potencias Bs.As.")
plt.show()

freq_za, fft_za = calcular_fft(alturas_za)

plt.plot(freq_za, np.abs(fft_za), c= 'r')
plt.xlabel("Frecuencia")
plt.ylabel("Potencia (energía)")
plt.xlim(0,4)
plt.ylim(0,20)
# me quedo solo con el último pico
pico_za = signal.find_peaks(np.abs(fft_za), prominence = 5)[0][-1]
#se grafican los picos como circulitos rojos
plt.scatter(freq_za[pico_za], np.abs(fft_za)[pico_za], facecolor='r')
plt.title("Espectro de Potencias Bs.As.")
plt.show()

ang_ba = np.angle(fft_ba)[pico_ba]
ang_za = np.angle(fft_za)[pico_za]
freq = freq_ba[pico_za]
ang2h = 24 / (2*np.pi*freq)
delta_t = int(round((ang_ba - ang_za) * ang2h))
delta_h = np.abs(fft_ba[0]) - np.abs(fft_za[0]) 

#Gráfico para verificar la corrección del desfasaje
inicio2 = '2013-04-01'
fin2 = '2013-04-15'
pd.DataFrame([df[inicio2:fin2]['H_BA'].shift(delta_t) - delta_h, df_z[inicio2:fin2]['H_Zarate']]).T.plot()

'''
El tiempo que le toma a la onda de marea llegar de Buenos Aires a Zárate 
es 2.91hs.
La onda llega atenuada a zárate y en la transformada creo que eso se puede vizualizar
con que la potencia en promedio es menor para Zárate.
'''










