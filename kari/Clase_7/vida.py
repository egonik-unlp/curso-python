#vida.py
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Ejercicio 7.1: Segundos vividos

from datetime import datetime

def segundos_vividos(fecha_nacimento):
    '''
    Calcula la cantidad de segundos que has vivido.
    Pre: fecha_nacimiento en formato 'dd/mm/AAAA'
    Pos: Cantidad de segundos vividos asumiento que naciste 
    a las 00:00hs.
    '''
    now = datetime.now() 
    nacimiento = datetime.strptime(fecha_nacimento, '%d/%m/%Y')
    segundos_vividos = (now - nacimiento).total_seconds()
    return segundos_vividos
    
