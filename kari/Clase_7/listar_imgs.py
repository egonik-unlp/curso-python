#listar_imgs.py

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

def buscar_png(directorio):
    '''
    Devuelve el nombre de los archivos .png que se encuentren en el 
    directorio dado o en algun subdirectorio de él.
    Pre: path del archivo, si se ejecuta como modulo principal,
    se debe ejecutar desde la carpeta ejercicios_python.
    Pos: lista de los archivos .png y el directorio en el 
    que se encuentra c/u.
    '''
    
    solo_png = []
    for root, dirs, files in os.walk(directorio):
        for name in files:
            if name[-3:] == 'png': 
                solo_png.append(os.path.join(root, name))
    if len(solo_png) == 0:
        print('No hay archivos .png en el directorio')
    return solo_png

def main(parametros):
    if len(parametros) != 2:
        raise SystemExit(f'Uso adecuado: {sys.argv[0]} ' 'ej:./Data/ordenar')
    from pprint import pprint
    pprint(buscar_png(parametros[1]))    


if __name__ == '__main__':
    import sys
    main(sys.argv)