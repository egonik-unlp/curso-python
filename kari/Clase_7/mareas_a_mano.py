#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#mareas_a_mano.py
#Ejercicio 7.10:

import pandas as pd
import os
import numpy as np

directorio = os.path.join('./Data', 'OBS_SHN_SF-BA.csv')

df = pd.read_csv(directorio, index_col = ['Time'], parse_dates = True)
dh = df['12-25-2014':].copy()

min_sf = dh[dh['H_SF'] == dh['H_SF'].min()].index.values
min_ba = dh[dh['H_BA'] == dh['H_BA'].min()].index.values
diferencia = min_ba - min_sf 
delta_t = int(diferencia/np.timedelta64(1, 'h')) #idea sacada de stackoverflow

delta_h = dh['H_SF'].max() - dh['H_BA'].max()

pd.DataFrame([dh['H_SF'].shift(delta_t) - delta_h, dh['H_BA']]).T.plot()


