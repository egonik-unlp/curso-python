#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#arbolado_parques_veredas.py
#Ejercicio 7.9: Comparando especies en parques y en veredas

import pandas as pd
import os


def altura_diametro(dir_parque, dir_vereda):
    '''
    Devuelve dos dataframes con el nombre cientifico,
    la altura y diametro correspondientes a los csv de 
    parques y veredas.
    Pre: path de los archivos en orden dir_parque, dir_vereda.
    Pos: df de parque y df de veredas con nombre cientifico,
    diametro y la altura (mismo nombre).
    '''   
    
    df_parques = pd.read_csv(dir_parque)
    df_veredas = pd.read_csv(dir_vereda)
    
    cols_v = ['nombre_cientifico','diametro_altura_pecho', 'altura_arbol']
    cols_p = ['nombre_cie', 'diametro', 'altura_tot']
    hyd_parques = df_parques[cols_p].copy()
    hyd_veredas = df_veredas[cols_v].copy()
    
    rename = {'nombre_cie': 'nombre_cientifico', 
              'diametro': 'diametro_altura_pecho', 
              'altura_tot': 'altura_arbol'}
    hyd_parques = hyd_parques.rename(columns = rename) #renombro el df de parques
    
    return hyd_parques, hyd_veredas


def alt_diam_especie(dir_parque, dir_vereda, especie_parque, especie_vereda):
    '''
    Devuelve un dataframe con la altura y diametro de la especie 
    seleccionada, diferenciando con la columna 'ambiente' si
    proviene de parque o vereda.
    Pre: path de los archivos y nombre de la especie segun parque o
    vereda.
    Pos: df con diametro y la altura de una especie determinada 
    distinguiendo el origen por la columna ambiente
    '''  
    
    hyd_parques, hyd_veredas = altura_diametro(dir_parque, dir_vereda)
    
    seleccion_parques = hyd_parques[hyd_parques['nombre_cientifico'] == especie_parque].copy()
    seleccion_veredas = hyd_veredas[hyd_veredas['nombre_cientifico'] == especie_vereda].copy()
    
    ambiente_parque = ['parque'] * seleccion_parques.shape[0] #armo la columna ambiente multiplicando por el nro de filas del df
    ambiente_vereda = ['vereda'] * seleccion_veredas.shape[0]
    
    seleccion_parques['ambiente'] = ambiente_parque #agrego la columna ambiente
    seleccion_veredas['ambiente'] = ambiente_vereda
    
    df_especie = pd.concat([seleccion_veredas, seleccion_parques])
    
    return df_especie

def boxplot_comp_especie(dir_parque, dir_vereda, especie_parque, especie_vereda, parametro):
    '''
    Realiza un boxplot de la variable elegida: 'altura_arbol' 
    o 'diametro_altura_pecho' de la especie seleccionada.
    Pre: path de los archivos, nombre de la especie para parque
    y vereda, y la variable elegida.
    Pos: boxplot comparando la 'altura_arbol' o el 'diametro_altura_pecho'
    de una misma especieen parques y veredas.
    '''
    df_especie = alt_diam_especie(dir_parque, dir_vereda, especie_parque, especie_vereda)
    df_especie.boxplot(parametro, by = 'ambiente')
    

directorio_vereda = os.path.join('./Data', 'arbolado-publico-lineal-2017-2018.csv')
directorio_parque = os.path.join('./Data', 'arbolado-en-espacios-verdes.csv')   
    
boxplot_comp_especie(directorio_parque, directorio_vereda, 'Tipuana Tipu', 'Tipuana tipu', 'altura_arbol')
boxplot_comp_especie(directorio_parque, directorio_vereda, 'Tipuana Tipu', 'Tipuana tipu', 'diametro_altura_pecho')

