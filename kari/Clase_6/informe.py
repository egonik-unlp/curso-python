#tabla_informe.py

import fileparse
import sys

#leer_camion
def leer_camion(nombre_archivo):    
    '''Lee un archivo .csv que contiene los datos de carga de un camión y devuelve una lista de diccionarios, 
    donde cada diccionario corresponde a un producto, número de cajones y precio'''   
    with open(nombre_archivo, 'rt') as file:
        camion = fileparse.parse_csv(file, select=['nombre','cajones','precio'], types=[str,int,float])               
    return camion

#leer_precios
def leer_precios(nombre_archivo):    
    '''Lee un archivo .csv sin encabezado con columna 1 = mercadería, columna 2 = precios y 
    devuelve un diccionario con la mercadería como clave'''
    with open(nombre_archivo, 'rt') as file:
        precios = dict(fileparse.parse_csv(file,types=[str,float], has_headers=False))                  
    return precios

#hacer_informe
def hacer_informe(archivo_camion, archivo_precios):
    '''Dado nu archivo .csv que contiene los datos de carga de un camión y
    un archivo .csv sin encabezado con columna 1 = mercadería y  
    columna 2 = precios, hace un informe de las ganancias por producto'''
    camion = leer_camion(archivo_camion)
    precios = leer_precios(archivo_precios)
    informe = []    
    for c_line in camion:    
        cambio = precios[c_line['nombre']] - c_line['precio'] 
        tupla = (c_line['nombre'], c_line['cajones'], precios[c_line['nombre']], cambio)        
        informe.append(tupla)    
    return informe
 
 
def imprimir_informe(informe):
    '''Imprime una tabla con el informe de ganancias/pérdidas
    de la carga de un camión'''
    encabezado = ('Nombre', 'Cajones', 'Precio', 'Cambio')
    print(f'{encabezado[0]:>10s} {encabezado[1]:>10s} {encabezado[2]:>10s} {encabezado[3]:>10s}')
    print(('-' * 10 + ' ') * len(encabezado))
    for nombre, cajones, precio, cambio in informe:
        print(f'{nombre:>10s} {cajones:>10d}',f"{f'${precio:.2f}':>10s}", f'{cambio:>10.2f}')

def informe_camion(nombre_archivo_camion, nombre_archivo_precios):
    '''Realiza el informe e imprime una tabla con los resultados
    a partir de dos archivos .csv correspondientes a la carga del camión
    y a los precios de venta'''
    informe = hacer_informe(nombre_archivo_camion, nombre_archivo_precios)
    imprimir_informe(informe)

    
def main(parametros):
    ''' Imprime el informe del camión. Toma como parámetros
    una lista compuesta de la siguiente manera:
    ['nombre archivo', 'path del archivo camion', 'path del archivo precios']
    '''
    informe_camion(parametros[1], parametros[2])
        

if __name__ == '__main__':
    main(sys.argv)