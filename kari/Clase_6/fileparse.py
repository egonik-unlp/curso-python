# fileparse.py
import csv

def parse_csv(file, select = None, types=None, has_headers=True, silence_errors = False):
    '''
    Parsea un archivo CSV en una lista de registros.
    Se puede seleccionar sólo un subconjunto de las columnas, determinando el parámetro select, que debe ser una lista de nombres de las columnas a considerar.
    '''
    if select and not has_headers:
        raise RuntimeError("Para seleccionar, necesito encabezados.")
    filas = csv.reader(file)   
    registros = []
    # Lee los encabezados del archivo
    if has_headers:
        encabezados = next(filas)

        # Si se indicó un selector de columnas,
        #    buscar los índices de las columnas especificadas.
        # Y en ese caso achicar el conjunto de encabezados para diccionarios

        if select:
            indices = [encabezados.index(nombre_columna) for nombre_columna in select]
            encabezados = select
        else:
            indices = []

        for i, fila in enumerate(filas, start = 1):
            try:                    
                if not fila:    # Saltear filas vacías
                    continue
                # Filtrar la fila si se especificaron columnas
                if indices:
                    fila = [fila[index] for index in indices]

                # Convertir el tipo de los datos recuperados antes de devolverlos
                if types:
                    fila = [func(val) for func, val in zip(types, fila)]            
    
                # Armar el diccionario
                registro = dict(zip(encabezados, fila))
                registros.append(registro)
            except ValueError as e:
                if not silence_errors:
                    print(f'Row {i}: No pude convertir {fila}')
                    print(f'Row {i}: Motivo: {e}')
                
    else:
        for fila in filas:
            if not fila:    # Saltear filas vacías
                continue
            if types:
                fila = [func(val) for func, val in zip(types, fila)]            
            # Armar el tupla
            registro = tuple(fila)
            registros.append(registro)

    return registros

