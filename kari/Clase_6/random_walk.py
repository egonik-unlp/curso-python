#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#random_walk.py

import numpy as np
import matplotlib.pyplot as plt

def randomwalk(largo):
    pasos=np.random.randint(-1,2,largo)    
    return pasos.cumsum()

# Lista de 12 caminatas al azar
def walks(n, pasos):
    '''
    Realiza 'n' random walks del número de pasos deseados.
    Pre: n y pasos deben ser números enteros.
    Pos: Devuelve un array con n random walks de tantos 'pasos' deseados.
    '''
    lista = []
    for i in range(n):
        a = randomwalk(pasos)
        lista.append(a)
    return np.array(lista)

# Encuentro la caminata que en promedio menos se aleja del cero

def caminata_promedio_cercana(array):
    '''
    Devuelve el número de fila  correspondiente a la caminata que
    en promedio menos se aleja del cero.
    '''
    cercana = [abs(np.mean(fila)) for fila in array]
    return cercana.index(min(cercana))


# Encuentro la caminata que más se aleja del cero en algún momento   
def caminata_mas_alejada(array):
    '''
    Devuelve el número de fila correspondiente a la caminata que
    más se aleja del cero.
    '''
    mas_alejada = [max(abs(fila)) for fila in array]
    return mas_alejada.index(max(mas_alejada))


def informe_randomwalks(n, pasos):
    '''
    Simula n random walks con un número de pasos
    y realiza trés gráficos juntos:
    - las n random walks del número de pasos deseados, 
    - la más alejada de ellas y
    - la más cercana al cero en promedio.
    Pre: n y pasos son números enteros.
    Pos: n random walks de un número determinado de pasos graficadas
    junto con la caminata que más se aleja y la que menos se aleja en 
    promedio.
    
    '''
    array_walks = walks(n, pasos)
    mas_alejada = array_walks[caminata_mas_alejada(array_walks)]
    mas_cercana = array_walks[caminata_promedio_cercana(array_walks)]
    
    plt.subplot(2, 1, 1)     
    for fila in array_walks:   
        plt.plot(fila)
    plt.title('12 Caminatas al azar')
    plt.xticks([])
    #El limite del eje y lo pongo según el valor de la caminata que más se aleja.
    plt.ylim(-abs(mas_alejada).max() * 1.1, abs(mas_alejada).max() * 1.1)
    plt.yticks([-400, 0, +400]) 
    
    
    plt.subplot(2, 2, 3)
    plt.plot(mas_alejada)
    plt.title('La caminata que más se aleja')
    plt.xticks([])
    plt.ylim(-abs(mas_alejada).max() * 1.1, abs(mas_alejada).max() * 1.1)
    plt.yticks([-400, 0, +400]) 
    
    
    plt.subplot(2, 2, 4)
    plt.plot(mas_cercana)
    plt.title('La caminata que menos se aleja')
    plt.xticks([])
    plt.ylim(-abs(mas_alejada).max() * 1.1, abs(mas_alejada).max() * 1.1)
    plt.yticks([-400, 0, +400])   
         
    plt.show()

informe_randomwalks(12, 100000)