#documentación.py
#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def valor_absoluto(n):
    '''
    Dado n devuelve el valor absoluto.
    Pre: n es un número
    Pos: Si n es mayor o igual a 0 devuelve n sino -n.
    '''
    if n >= 0:
        return n
    else:
        return -n
    
'No hay invariantes de ciclo'
#%%

def suma_pares(l):
    '''
    Calcula la sumatoria de los números pares de una lista dada.
    Pre: lista de números enteros.
    Pos: La suma de los números pares. Si no los hubieras devuelve 0.
    '''    
    res = 0
    for e in l:
        if e % 2 ==0:
            res += e
        else:
            res += 0

    return res

'''res es un invatiante de ciclo dado que contiene el valor de la suma
de los elementos pares ya recorridos'''

#%%

def veces(a, b):
    '''
    Suma 'b' veces 'a' .
    Pre: a y b son números enteros positivos.
    Pos: Devuelve la multiplicación de a por b.
    '''   
    res = 0
    nb = b
    while nb != 0:  
        #print(nb * a + res)
        res += a
        nb -= 1
    return res

''' res es un invariante de ciclo dado que contiene el valor de la 
suma de a tantas veces como el valor de 'b' en el ciclo.'''

#%%

def collatz(n):
    '''
    Calcula el número de pasos que se realizan para reducir 'n' a 1
    aplicando la conjetura de Collatz. 
    Pre: n pertenece a los números naturales.
    Pos: Devuelve el número de pasos para llegar a 1 empezando en 'n'
    si se aplicara la conjetura de Collatz.
    '''   
    res = 1

    while n != 1:
        if n % 2 == 0:
            n = n//2
        else:
            n = 3 * n + 1
        res += 1

    return res

''' El invariante de ciclo es res, donde representa el número de pasos
realizados hasta el momento en el ciclo'''