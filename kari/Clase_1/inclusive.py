# inclusive.py

frase = 'todos somos programadores'
palabras = frase.split()
frase_t = []

for palabra in palabras:
    if palabra[-1] == 'o':
        frase_t += [palabra[:-1] + 'e']
    elif len(palabra) > 1 and palabra[-2] == 'o':
        frase_t += [palabra[:-2] + 'e' + palabra[-1]]
    else:
        frase_t += [palabra]
            
frase_t = " ".join(frase_t)
print(frase_t)

# Resultado para 'Los hermanos sean unidos porque ésa es la ley primera': Les hermanes sean unides porque ésa es la ley primera

# Resultado para '¿cómo transmitir a los otros el infinito Aleph?': ¿cóme transmitir a les otres el infinite Aleph?

# Resultado para 'Todos, tu también': 'Todos, tu también'.

