# geringoso.py

cadena = input('Escribe una palabra: ')
capadepenapa = ''
vocales = 'aeiouAEIOUáéíóú'

for c in cadena:
    capadepenapa += c
    if c in vocales:
        capadepenapa += 'p' + c

print(capadepenapa)

# Resultado para 'apa': apapapa
# Resultado para 'boligoma': bopolipigopomapa
