#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#ticker.py

from vigilante import vigilar
import csv

def elegir_columnas(filas, columnas):
    for fila in filas:
        yield [fila[index] for index in columnas]


def cambiar_tipos(datos, tipos):
    for fila in datos:
        yield [func(val) for func, val in zip(tipos, fila)]

def hace_dicts(datos, encabezados):
    for fila in datos:
        yield dict(zip(encabezados, fila))    
    
def parsear_datos(archivo):
    datos = csv.reader(archivo)
    datos = elegir_columnas(datos, [0, 1, 2])
    datos = cambiar_tipos(datos, [str, float, int])
    datos = hace_dicts(datos, ['nombre', 'precio', 'volumen'])
    return datos    

def filtrar_datos(datos, nombres):
    fila = (fila for fila in datos if fila['nombre'] in nombres)
    return fila

def ticker(camion_file, log_file, fmt):
    import informe
    import formato_tabla
    camion = informe.leer_camion(camion_file)
    datos = parsear_datos(vigilar(log_file))
    datos = filtrar_datos(datos, camion)
    formateador = formato_tabla.crear_formateador(fmt) #Uso el formato_tabla de trabajos previos
    formateador.encabezado(['Nombre', 'Precio', 'Volumen'])
    filadata = ((fila['nombre'], f"{fila['precio']:0.2f}", 
                         str(fila['volumen'])) for fila in datos)
    for fila in filadata:
        formateador.fila(fila)

if __name__ == '__main__':
    ticker('Data/camion.csv', 'Data/mercadolog.csv', 'txt')
    
    
    
    
    
    
    
    
    
    
    