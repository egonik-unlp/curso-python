#tabla_informe.py

import fileparse
from lote import Lote
import formato_tabla
from camion import Camion

#leer_camion
def leer_camion(nombre_archivo):    
    '''Lee un archivo .csv que contiene los datos de carga de un camión y devuelve una lista de diccionarios, 
    donde cada diccionario corresponde a un producto, número de cajones y precio'''   
    with open(nombre_archivo, 'rt') as file:
        camion_dict = fileparse.parse_csv(file, select=['nombre','cajones','precio'], types=[str,int,float])               
        camion = [Lote(d['nombre'], d['cajones'], d['precio']) for d in camion_dict]
    return Camion(camion)

#leer_precios
def leer_precios(nombre_archivo):    
    '''Lee un archivo .csv sin encabezado con columna 1 = mercadería, columna 2 = precios y 
    devuelve un diccionario con la mercadería como clave'''
    with open(nombre_archivo, 'rt') as file:
        precios = dict(fileparse.parse_csv(file,types=[str,float], has_headers=False))                  
    return precios

#hacer_informe
def hacer_informe(camion, precios):
    '''Dados los datos de carga de un camion y los precios de venta
    (ambos de tipo lote) hace un informe de las ganancias por producto
    '''
    informe = []    
    for c_line in camion:    
        cambio = precios[c_line.nombre] - c_line.precio
        tupla = (c_line.nombre, c_line.cajones, precios[c_line.nombre], cambio)        
        informe.append(tupla)    
    return informe
 
 
def imprimir_informe(informe, formateador):
    '''Imprime una tabla con el informe de ganancias/pérdidas
    de la carga de un camión a partir de una tupla (nombre, cajones,
    precios, cambio)'''
    formateador.encabezado(['Nombre', 'Cajones', 'Precio', 'Cambio'])
    for nombre, cajones, precio, cambio in informe:
        rowdata = [nombre, str(cajones), f'{precio:0.2f}', f'{cambio:0.2f}']
        formateador.fila(rowdata)

def informe_camion(archivo_camion, archivo_precios, fmt = 'txt'):
    '''
    Crea un informe con la carga de un camión
    a partir de archivos camion y precio.
    El formato predeterminado de la salida es txt
    Alternativas: csv o html
    '''
    camion = leer_camion(archivo_camion)
    precios = leer_precios(archivo_precios)
    data_informe = hacer_informe(camion, precios)
    formateador = formato_tabla.crear_formateador(fmt)
    imprimir_informe(data_informe, formateador)

    
def main(parametros):
    ''' Imprime el informe del camión. Toma como parámetros
    una lista compuesta de la siguiente manera:
    ['nombre archivo', 'path del archivo camion', 'path del archivo precios']
    '''
    if len(parametros) < 3:
        raise SystemExit('Uso: %s archivo_camion archivo_precios' % parametros[0])
    if len(sys.argv) == 4: 
        informe_camion(parametros[1], parametros[2], parametros[3])
    else:
        informe_camion(parametros[1], parametros[2])

if __name__ == '__main__':
    import sys
    main(sys.argv)
    