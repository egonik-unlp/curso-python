#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#lote.py

class Lote:
    
    def __init__(self, nombre, cajones, precio):
        self.nombre = nombre
        self.cajones = cajones
        self.precio = precio
        
    def costo(self):
        costo = self.cajones * self.precio
        return costo
    
    def vender(self, cajones):
        self.cajones -= cajones

    def __str__(self):
        return '(%s, %d, %.2f)' % (self.nombre, self.cajones, self.precio)
    
    def __repr__(self):
        return 'Lote(%r, %d, %0.2f)' % (self.nombre, self.cajones, self.precio)

    def __eq__(self, otro):
        if not isinstance(otro, Lote):
            return NotImplemented
        return self.nombre == otro.nombre and self.cajones == otro.cajones and self.precio == otro.precio

    def __ne__(self, otro):
        if not isinstance(otro, Lote):
            return NotImplemented
        return self.nombre != otro.nombre and self.cajones != otro.cajones and self.precio != otro.precio
 