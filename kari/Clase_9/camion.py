#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#camion.py

class Camion:
    '''
    Una clase que representa la carga de un camion.
    
    Atributos
    ---------
    _lote: Lote
        Un lote compuesto del nombre del producto, nº de cajones y precio.
    
    '''
    def __init__(self, lotes):
        self._lotes = lotes

    def __iter__(self):
        return self._lotes.__iter__()

    def __len__(self):
        return len(self._lotes)

    def __getitem__(self, index):
        return self._lotes[index]

    def __contains__(self, nombre):
        return any([lote.nombre == nombre for lote in self._lotes])

    def precio_total(self):
        return sum(l.costo() for l in self._lotes)

    def contar_cajones(self):
        from collections import Counter
        cantidad_total = Counter()
        for l in self._lotes:
            cantidad_total[l.nombre] += l.cajones
        return cantidad_total
    
    def __str__(self):
        return '(%s)' % (self._lotes)
    
    def __repr__(self):
        return 'Camion(%r)' % (self._lotes)
    
    def __setitem__(self, index, value):
        self._lotes[index] = value
        
    def __delitem__(self, value): #Solo se puede aplicar si en la clase Lote se definió __eq__ (lo dejo más abajo)
        self._lotes.remove(value)
        
    def __eq__(self, otro): #Compara línea a línea. Si fueran dos objetos iguales pero con distinto orden de lineas da False
        if not isinstance(otro, Camion):
            return NotImplemented
        if len(otro._lotes) != len(self._lotes):
            return False
        for i in range(len(self._lotes)):
            if self._lotes[i] !=  otro._lotes[i]:
                return False
        return True
    
    def __ne__(self, otro): #Compara línea a línea, si fueran iguales pero con distinto orden de lineas da True
        if not isinstance(otro, Camion):
            return NotImplemented
        if len(otro._lotes) == len(self._lotes):
            return True
        for i in range(len(self._lotes)):
            if self._lotes[i] ==  otro._lotes[i]:
                return True
        return False
    

class Lote:
    '''
    Una clase que representa un conjunto de unidades de venta
    de un producto informando cantidad del mismo y precio.
    
    Atributos
    ---------
    nombre: str
        Nombre del producto.
    cajones: int
        Cantidad de cajones del producto.
    precio: float
        Precio por cajón al que se compró el producto.
    
    '''
    def __init__(self, nombre, cajones, precio):
        self.nombre = nombre
        self.cajones = cajones
        self.precio = precio
        
    def costo(self):
        costo = self.cajones * self.precio
        return costo
    
    def vender(self, cajones):
        self.cajones -= cajones

    def __str__(self):
        return '(%s, %d, %.2f)' % (self.nombre, self.cajones, self.precio)
    
    def __repr__(self):
        return 'Lote(%r, %d, %0.2f)' % (self.nombre, self.cajones, self.precio)

    def __eq__(self, otro):
        if not isinstance(otro, Lote):
            return NotImplemented
        return self.nombre == otro.nombre and self.cajones == otro.cajones and self.precio == otro.precio

    def __ne__(self, otro):
        if not isinstance(otro, Lote):
            return NotImplemented
        return self.nombre != otro.nombre and self.cajones != otro.cajones and self.precio != otro.precio
