#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#bbin_rec.py
#Ejercicio 10.11: Búsqueda binaria

def bbinaria_rec(lista, e):
    '''
    Búsqueda binaria de un elemento e en una lista.
    Pre: lista ordenada
    Pos: devuelve True si el elemento está en la lista o False si no lo está.
    '''
    if len(lista) == 0:
        res = False
    elif len(lista) == 1:
        res = lista[0] == e
    else:
        medio = len(lista)//2
        if lista[medio] == e:
            res = True
        elif lista[medio] < e:
            res = bbinaria_rec(lista[medio+1:], e)
        else:
            res = bbinaria_rec(lista[:medio], e)
    return res

a = [2,5,8,9,11,13,15,19,21]
bbinaria_rec(a, 15)
