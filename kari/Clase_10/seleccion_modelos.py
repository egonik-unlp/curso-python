#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#seleccion_modelos.py

from sklearn import linear_model
import numpy as np

def pot(x, n):
    '''Genera las n potencias del array x'''
    X = x.reshape(-1, 1)
    for i in range(n - 1):
        X = np.concatenate((X, (x**(i+2)).reshape(-1,1)), axis=1)
    return X

def AIC(k, ecm, num_params):
    '''Calcula el AIC de una regresión lineal múltiple de 'num_params' parámetros, ajustada sobre una muestra de 'k' elementos, y que da lugar a un error cuadrático medio 'ecm'.'''
    aic = k * np.log(ecm) + 2 * num_params
    return aic

#Creamos el array x
np.random.seed(3141) # semilla para fijar la aleatoriedad
N = 50
indep_vars = np.random.uniform(size = N, low = 0, high = 10)
r = np.random.normal(size = N, loc = 0.0, scale = 8.0) # residuos
dep_vars = 2 + 3*indep_vars + 2*indep_vars**2 + r 
y = dep_vars.reshape(-1,1) #para que la regresión lineal acepte el array

AICs = []
ECM = []
LM = []
for i in range(1, 9):
    x = pot(indep_vars, i)
    lm = linear_model.LinearRegression()  
    lm.fit(x, y)
    LM.append(lm)
    errores = y - (lm.predict(x))
    ecm = (errores**2).mean()
    ECM.append(ecm)
    aic = AIC(N, ecm, i+1)
    AICs.append(aic)
    print('-'*25)
    print(f'Grado del polinomio: {i}')
    print(f'Cantidad de parámetros: {i+1}')
    print(f'ECM: {ecm:.3f}')
    print(f'AIC: {aic:.3f}')
print('-'*25)
print(f'El polinomio que minimiza el AIC es de grado {np.argmin(AICs)+1}')


