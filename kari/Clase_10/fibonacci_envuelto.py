#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#fibonacci_envuelto.py
#Ejercicio 10.12: Envolviendo a Fibonacci

def fibonacci(n):
    """
    Toma un entero positivo n y
    devuelve el n-ésimo número de Fibonacci
    donde F(0) = 0 y F(1) = 1.
    """    
    def fibonacci_aux(n, dict_fibo):
        """
        Calcula el n-ésimo número de Fibonacci de forma recursiva
        utilizando un diccionario para almacenar los valores ya computados.
        dict_fibo es un diccionario que guarda en la clave 'k' el valor de F(k)
        """
        if n in dict_fibo.keys():
            F = dict_fibo[n]
        else:
            Fn_1 = fibonacci_aux(n-1, dict_fibo)[0]
            Fn_2 = fibonacci_aux(n-2, dict_fibo)[0]
            dict_fibo[n] = Fn_1 + Fn_2
        F = dict_fibo[n]
        return F, dict_fibo
            

    dict_fibo = {0:0, 1:1} 
    F, dict_fibo = fibonacci_aux(n, dict_fibo)
    return F

fibonacci(6)


