#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#larenga.py
#Ejercicio 10.9: Pascal

def pascal(n,k):
    '''
    Devuelve el número correspondiente a la fila n posición k en el 
    triángulo de Pascal.
    Pre: n y k números enteros iguales o mayores que cero. 
    Pos: número entero de la posición n, k en el triángulo de Pascal.
    '''
    if n == 1 or n == 0:
        res = 1
    elif k == 0 or k == n:
        res = 1
    else:
        res = pascal(n-1, k-1) + pascal(n-1, k)
        
    return res

#%%

#Para mejorar eficiencia hice otra con recursión de cola.

def pascal(n,k):
    '''
    Devuelve el número correspondiente a la fila n posición k en el 
    triángulo de Pascal.
    Pre: n y k números enteros iguales o mayores que cero. 
    Pos: número entero de la posición n, k en el triángulo de Pascal.
    '''
    def triangulo(n, lista = None):
        if lista is None: #Idea sacada de stackoverflow
            lista = [[1]]
        if n == 0:
            return lista
        else:
            fila = [[1] + [lista[-1][i] + lista[-1][i+1] for i in range(len(lista)-1)] + [1]]
        return triangulo(n-1, lista + fila)
    
    try:
        return triangulo(n)[n][k]
    except IndexError:
        print('Esa posición no existe. Intenta con otro número.')
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        