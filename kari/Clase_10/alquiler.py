#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#alquiler.py
#Ejercicio 10.14: precio_alquiler ~ superficie 

import numpy as np
import matplotlib.pyplot as plt


def ajuste_lineal_simple(x,y):
    a = sum(((x - x.mean())*(y-y.mean()))) / sum(((x-x.mean())**2))
    b = y.mean() - a*x.mean()
    return a, b

#Datos
superficie = np.array([150.0, 120.0, 170.0, 80.0])
alquiler = np.array([35.0, 29.6, 37.4, 21.0])

#Ajuste lineal
a, b = ajuste_lineal_simple(superficie, alquiler)
#Datos para crear recta de gráfico
grilla_x = np.linspace(start = min(superficie), stop = max(superficie), num = 1000)
grilla_y = grilla_x*a + b

#Gráficos
g = plt.scatter(superficie, alquiler)
r = plt.plot(grilla_x, grilla_y, c = 'g')

plt.title('Modelo de ajuste lineal')
plt.xlabel(f'Superficie (m²)')
plt.ylabel(f'Precio del alquiler (miles de pesos)')

plt.show()

#Errores
errores = alquiler - (a*superficie + b)
error_b = alquiler - alquiler.mean()
print(errores)
print("ECM:", (errores**2).mean())
print(f'R²:', (1 - (errores**2).mean()/(error_b**2).mean()))
