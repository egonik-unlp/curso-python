#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#hojas_ISO.py
#Ejercicio 10.13: Hojas ISO y recursión

def A(N):
    '''
    A partir de indicar el numero N devuelve el tamaño
    de la hoja AN en milímetro según la norma ISO 216.
    Pre: número entero mayor o igual a 0.
    Pos: tupla (ancho, largo) en mm.
    '''
    if N == 0:
        res = (841, 1189) 
    else:
        a, l = A(N-1)
        res = (l//2, a)  
          
    return res

#Ejemplo
if __name__ == '__main__':
    
    for i in range(11):
        print(f'A{i}: {A(i)[0]} x {A(i)[1]} mm')

