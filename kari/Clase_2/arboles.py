#arboles.py


def leer_parque(nombre_archivo, parque):
    
    '''Toma archivos .csv y devuelve una lista de diccionarios que corresponden a los árboles del parque elegido '''
    
    import csv
    arboles_totales = []    
    with open(nombre_archivo, 'rt') as archivo:
        data = csv.reader(archivo)
        header = next(data)        
        for row in data:
            record = dict(zip(header, row))
            record['long'] = float(record['long'])         # Los sgtes pasos son para transformar cada valor segun corresponda
            record['lat'] = float(record['lat'])
            record['id_arbol'] = int(record['id_arbol'])
            record['altura_tot'] = int(record['altura_tot'])
            record['diametro'] = int(record['diametro'])
            record['inclinacio'] = int(record['inclinacio'])
            record['id_especie'] = int(record['id_especie'])
            record['coord_x'] = float(record['coord_x'])
            record['coord_y'] = float(record['coord_y'])
            arboles_totales.append(record)    
    arboles_parque = []    
    for row in arboles_totales:
        if row['espacio_ve'] == parque:
            arboles_parque.append(row)        
    return arboles_parque


# Determinar las especies en una lista


def especies(lista_arboles):
    
    '''Toma una lista de árboles y devuelve el conjunto de especies presentes en la lista'''
    
    especies = []
    for row in lista_arboles:
        especies.append(row['nombre_com'])    
    especies_unicas = set(especies)    
    return especies_unicas


# Funcion que cuenta ejemplares 

def contar_ejemplares(lista_arboles):
    
    '''Toma una lista de árboles y devuelve el número de árboles  que corresponden a una misma especie.'''
    
    from collections import Counter    
    ejemplares = Counter()    
    for row in lista_arboles:
        ejemplares[row['nombre_com']] += 1    
    return ejemplares

# Ejemplo

a_parque = 'GENERAL PAZ'
b_parque = 'ANDES, LOS'
c_parque = 'CENTENARIO'

a = leer_parque('Data/arbolado.csv', a_parque)
b = leer_parque('Data/arbolado.csv', b_parque)
c= leer_parque('Data/arbolado.csv', c_parque)

a_contar = contar_ejemplares(a)
b_contar = contar_ejemplares(b)
c_contar = contar_ejemplares(c)
mas_comun = 5

print(f'Las {mas_comun} especies más frecuentes del parque {a_parque} son: ')
a_mas_comun = a_contar.most_common(mas_comun)
for i in a_mas_comun:
    print(f' {i[0]}: {i[1]} árboles')

print(f'Las {mas_comun} especies más frecuentes del parque {b_parque[-3:]} {b_parque[0:5]} son: ')
b_mas_comun = b_contar.most_common(mas_comun)
for i in b_mas_comun:
    print(f' {i[0]}: {i[1]} árboles')
    
print(f'Las {mas_comun} especies más frecuentes del parque {c_parque} son: ')
c_mas_comun = c_contar.most_common(mas_comun)
for i in c_mas_comun:
    print(f' {i[0]}: {i[1]} árboles')

# Output

#Las 5 especies más frecuentes del parque GENERAL PAZ son: 
 #Casuarina: 97 árboles
 #Tipa blanca: 54 árboles
 #Eucalipto: 49 árboles
 #Palo borracho rosado: 44 árboles
 #Fenix: 40 árboles
#Las 5 especies más frecuentes del parque LOS ANDES son: 
 #Jacarandá: 117 árboles
 #Tipa blanca: 28 árboles
 #Ciprés: 21 árboles
 #Palo borracho rosado: 18 árboles
 #Lapacho: 12 árboles
#Las 5 especies más frecuentes del parque CENTENARIO son: 
 #Plátano: 137 árboles
 #Jacarandá: 45 árboles
 #Tipa blanca: 42 árboles
 #Palo borracho rosado: 41 árboles
 #Fresno americano: 38 árboles

#Alturas de una especie en una lista

def obtener_alturas(lista_arboles, especie):
    
    '''Dada una lista de árboles y una especie devuelve una lista con las alturas de los ejemplares de esa especie'''
    
    altura = []    
    for row in lista_arboles:        
        if row['nombre_com'] == especie:
            altura.append(row['altura_tot'])        
    return altura

# Ejemplo
altura_a = obtener_alturas(a, 'Jacarandá') # a, b y c definidas previamente como leer_parque('Data/arbolado.csv', x_parque)
altura_b = obtener_alturas(b, 'Jacarandá')
altura_c = obtener_alturas(c, 'Jacarandá')

max_a = max(altura_a)
max_b = max(altura_b)
max_c = max(altura_c)

mean_a = sum(altura_a)/len(altura_a)
mean_b = sum(altura_b)/len(altura_b)
mean_c = sum(altura_c)/len(altura_c)
print('\n Alturas máximas y promedio del Jacarandá en distintos parques')
print(f'Parque {a_parque:>25s}', f"{f'{b_parque[-3:]} {b_parque[0:5]}':>10s}", f' {c_parque:>10s}')
print(f'Altura máxima {max_a:18.2f} {max_b:10.2f} {max_c:11.2f}')
print(f'Altura promedio {mean_a:>16.2f} {mean_b:10.2f} {mean_c:11.2f}')

#Output

 #Alturas máximas y promedio del Jacarandá en distintos parques
#Parque               GENERAL PAZ  LOS ANDES  CENTENARIO
#Altura máxima              16.00      25.00       18.00
#Altura promedio            10.20      10.54        8.96

 
# Lista de inclinaciones por especie de una lista

def obtener_inclinaciones(lista_arboles, especie):
    
    '''Dada una lista de árboles y una especie devuelve una lista con las inclinaciones de los ejemplares de esa especie'''
    
    inclinaciones = []    
    for row in lista_arboles:        
        if row['nombre_com'] == especie:
            inclinaciones.append(row['inclinacio'])        
    return inclinaciones 


# Especie con el ejemplar más inclinado

def especimen_mas_inclinado(lista_arboles):
    
    '''Dada una lista de árboles devuelve la especie que tiene el ejemplar más inclinado y su inclinación.'''
    
    especies_unicas = especies(lista_arboles)    #Uso función defenida en línea 35
    incl_especimenes = []    
    for especie in especies_unicas:        
        incl = obtener_inclinaciones(lista_arboles, especie) #Uso función defenida en línea 148
        mas_incl = max(incl)
        incl_especimenes.append((mas_incl, especie))        
    max_incl = max(incl_especimenes)
    return max_incl

# Ejemplo
incl_a = especimen_mas_inclinado(a)
incl_b = especimen_mas_inclinado(b)
incl_c = especimen_mas_inclinado(c)
print(f'\nEstudio de la inclinación')
print(f'- El ejemplar más inclinado del parque {a_parque} es el {incl_a[1]} con un valor de {incl_a[0]} grados.')
print(f'- El ejemplar más inclinado del parque', f"{f'{b_parque[-3:]} {b_parque[0:5]}':>10s}", f' es el {incl_b[1]} con un valor de {incl_b[0]} grados.')
print(f'- El ejemplar más inclinado del parque {c_parque} es el {incl_c[1]} con un valor de {incl_c[0]} grados.')

#Output

#Estudio de la inclinación
#- El ejemplar más inclinado del parque GENERAL PAZ es el Macrocarpa (Ciprés de Monterrey o Ciprés de Lambert) con un valor de 70 grados.
#- El ejemplar más inclinado del parque  LOS ANDES  es el Jacarandá con un valor de 30 grados.
#- El ejemplar más inclinado del parque CENTENARIO es el Falso Guayabo (Guayaba del Brasil) con un valor de 80 grados.


#Especie con más inclinada en promedio

def especie_promedio_mas_inclinada(lista_arboles):
    
    '''Dada una lista de árboles devuelve la especie que en promedio tiene la mayor inclinación y el promedio calculado.'''
    
    especies_unicas = especies(lista_arboles)           #Uso función defenida en línea 35
    incl_prom_especimenes = []    
    for especie in especies_unicas:        
        incl = obtener_inclinaciones(lista_arboles, especie)    #Uso función defenida en línea 148
        incl_prom = sum(incl)/len(incl)                         #Calculo el promedio
        incl_prom_especimenes.append((incl_prom, especie))        
    max_incl_prom = max(incl_prom_especimenes)
    return max_incl_prom

#Ejemplo 

prom_incl_a = especie_promedio_mas_inclinada(a)
prom_incl_b = especie_promedio_mas_inclinada(b)
prom_incl_c = especie_promedio_mas_inclinada(c)

print(f'\nEstudio de la inclinación promedio')
print(f'- El ejemplar más inclinado en promedio del parque {a_parque} es el {prom_incl_a[1]} con un valor promedio de {prom_incl_a[0]} grados.')
print(f'- El ejemplar más inclinado en promedio del parque', f"{f'{b_parque[-3:]} {b_parque[0:5]}':>10s}", f' es el {prom_incl_b[1]} con un valor promedio de {prom_incl_b[0]} grados.')
print(f'- El ejemplar más inclinado en promedio del parque {c_parque} es el {prom_incl_c[1]} con un valor promedio de {prom_incl_c[0]} grados.')

#Output

#Estudio de la inclinación promedio
#- El ejemplar más inclinado en promedio del parque GENERAL PAZ es el No Determinable con un valor promedio de 25.0 grados.
#- El ejemplar más inclinado en promedio del parque  LOS ANDES  es el Álamo plateado con un valor promedio de 25.0 grados.
#- El ejemplar más inclinado en promedio del parque CENTENARIO es el Rosa de Siria con un valor promedio de 25.0 grados.
