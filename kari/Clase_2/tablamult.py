#tablamult.py

base = list(range(0, 10))
tabla = []
for i in base:
    lista = [i, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for v in range(1,10):
        lista[v+1] = base[i] + lista[v]
    tabla.append(lista)
    
print(f'{base[0]:>8d} {base[1]:>3d} {base[2]:>3d} {base[3]:>3d} {base[4]:>3d} {base[5]:>3d} {base[6]:>3d} {base[7]:>3d} {base[8]:>3d} {base[9]:>3d}')
print('---------------------------------------------')
for a, b, c, d, e, f, g, h, i, j, k  in tabla:
    print(f'{a}: {b:>5d} {c:>3d} {d:>3d} {e:>3d} {f:>3d} {g:>3d} {h:>3d} {i:>3d} {j:>3d} {k:>3d}')



