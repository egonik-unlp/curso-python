#costo_commandline.py

import csv
import sys

# Definición de la función

def costo_camion(nombre_archivo):  
    
    '''Calcula el costo total del camion de un archivo (cajones*precio).'''
    
    precio_total = 0.0    
    with open(nombre_archivo, 'rt') as camion:                 
        rows = csv.reader(camion)
        headers = next(rows)          #Descarto la primera fila        
        for nline, row in enumerate(rows, 1): 
            try:                
                cajones = int(row[1])
                precio = float(row[2])
                precio_total += cajones * precio
            except ValueError:
                #Indica la fila en dónde se encuentra el valor faltante (el conteo no incluye el header)
                print(f'Error en la fila número {nline} que corresponde a {row}')                 
    return precio_total

# Indicaciones para pasarle al programa el nombre del archivo cuando se lo llama desde la línea de comandos

if len(sys.argv) == 2:
    nombre_archivo = sys.argv[1]
else:
    nombre_archivo = 'Data/camion.csv'

# Cálculo del costo
costo = costo_camion(nombre_archivo)
print(f'Costo total: ${costo}')

# Output
# Costo total: $47671.15
# Con 'Data/missing.csv' = Error en la fila número 4 que corresponde a ['Mandarina', '', '51.23']
#                         Error en la fila número 7 que corresponde a ['Naranja', '', '70.44']
#                         Costo total: $30381.15


