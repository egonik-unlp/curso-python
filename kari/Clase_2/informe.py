# informe.py

#leer_camion


def leer_camion(nombre_archivo):
    
    '''Lee un archivo .csv que contiene los datos de carga de un camión y devuelve una lista de diccionarios, donde cada diccionario corresponde a un producto'''   
    
    import csv
    camion = []      
    with open(nombre_archivo, 'rt') as f:                 
        rows = csv.reader(f)
        headers = next(rows)           
        for nline, row in enumerate(rows, start = 1):             # nline sirve para identficar la línea dónde hubo un error
            record = dict(zip(headers, row))
            try:
                record['cajones'] = int(record['cajones'])          
                record['precio'] = float(record['precio'])          
                camion.append(record)
            except ValueError:
                #Indica dónde ocurre el error
                print(f'Fila {nline}: No pude interpretar: {row}')                 
    return camion


#leer_precios

def leer_precios(nombre_archivo):
    
    '''Lee un archivo .csv sin encabezado con columna 1 = mercadería, columna 2 = precios y devuelve un diccionario con la mercadería como clave'''
    
    import csv
    precios = {}      
    with open(nombre_archivo, 'rt') as f:                 
        rows = csv.reader(f)        
        for row in rows: 
            try:               
                precios[row[0]] = float(row[1])
            except IndexError:
                pass                
    return precios 


#costo_camion

def costo_camion(nombre_archivo):
    
    '''Lee un archivo .csv con datos sobre la carga del camión y calcula el costo total de los bienes.'''
    
    import csv
    precio_total = 0.0    
    with open(nombre_archivo) as camion:                 
        rows = csv.reader(camion)
        headers = next(rows)        
        for nline, row in enumerate(rows, start = 1): 
            record = dict(zip(headers, row))
            try:
                cajones = int(record['cajones'])
                precio = float(record['precio'])
                precio_total += cajones * precio
            except ValueError:
                print(f'Fila {nline}: No pude interpretar: {row}')                
    return precio_total


#recaudacion

def recaudacion(camion, precios):
    recaudacion = 0.0 
    for c_line in camion:
        recaudacion += precios[c_line['nombre']] * c_line['cajones']
    return recaudacion
            

# Balance

camion = leer_camion('Data/fecha_camion.csv')
precios = leer_precios('Data/precios.csv')
costo = costo_camion('Data/fecha_camion.csv')
venta = recaudacion(camion, precios)

print(f'Costo del camión: ${costo:0.2f}')
print(f'Total recaudado: ${venta:0.2f}')
print(f'Balance: ${venta - costo:0.2f}')


# Output con Data/camion.csv

'''
Costo del camión: $47671.15
Total recaudado: $62986.10
Balance: $15314.95
'''

# Output con Data/fecha_camion.csv
'''
Costo del camión: $47671.15
Total recaudado: $62986.10
Balance: $15314.95
'''


