#tabla_informe.py

#leer_camion


def leer_camion(nombre_archivo):
    
    '''Lee un archivo .csv que contiene los datos de carga de un camión y devuelve una lista de diccionarios, donde cada diccionario corresponde a un producto'''   
    
    import csv
    camion = []      
    with open(nombre_archivo, 'rt') as f:                 
        rows = csv.reader(f)
        headers = next(rows)           
        for nline, row in enumerate(rows, start = 1):             # nline sirve para identficar la línea dónde hubo un error
            record = dict(zip(headers, row))
            try:
                record['cajones'] = int(record['cajones'])          
                record['precio'] = float(record['precio'])          
                camion.append(record)
            except ValueError:
                #Indica dónde ocurre el error
                print(f'Fila {nline}: No pude interpretar: {row}')                 
    return camion


#leer_precios

def leer_precios(nombre_archivo):
    
    '''Lee un archivo .csv sin encabezado con columna 1 = mercadería, columna 2 = precios y devuelve un diccionario con la mercadería como clave'''
    
    import csv
    precios = {}      
    with open(nombre_archivo, 'rt') as f:                 
        rows = csv.reader(f)        
        for row in rows: 
            try:               
                precios[row[0]] = float(row[1])
            except IndexError:
                pass                
    return precios 


#costo_camion

def costo_camion(nombre_archivo):
    
    '''Lee un archivo .csv con datos sobre la carga del camión y calcula el costo total de los bienes.'''
    
    import csv
    precio_total = 0.0    
    with open(nombre_archivo) as camion:                 
        rows = csv.reader(camion)
        headers = next(rows)        
        for nline, row in enumerate(rows, start = 1): 
            record = dict(zip(headers, row))
            try:
                cajones = int(record['cajones'])
                precio = float(record['precio'])
                precio_total += cajones * precio
            except ValueError:
                print(f'Fila {nline}: No pude interpretar: {row}')                
    return precio_total

#hacer_informe

def hacer_informe(camion, precios):    
    informe = []    
    for c_line in camion:    
        cambio = precios[c_line['nombre']] - c_line['precio'] 
        tupla = (c_line['nombre'], c_line['cajones'], precios[c_line['nombre']], cambio)        
        informe.append(tupla)    
    return informe
    

camion = leer_camion('Data/camion.csv')
precios = leer_precios('Data/precios.csv')
informe = hacer_informe(camion, precios)

encabezado = ('Nombre', 'Cajones', 'Precio', 'Cambio')
print(f'{encabezado[0]:>10s} {encabezado[1]:>10s} {encabezado[2]:>10s} {encabezado[3]:>10s}')
print('---------- ---------- ---------- ----------')
for nombre, cajones, precio, cambio in informe:
    print(f'{nombre:>10s} {cajones:>10d}',f"{f'${precio:.2f}':>10s}", f'{cambio:>10.2f}')
    
    
# Output
'''
    Nombre    Cajones     Precio     Cambio
---------- ---------- ---------- ----------
      Lima        100     $40.22       8.02
   Naranja         50    $106.28      15.18
     Caqui        150    $105.46       2.02
 Mandarina        200     $80.89      29.66
   Durazno         95     $73.48      33.11
 Mandarina         50     $80.89      15.79
   Naranja        100    $106.28      35.84
'''

