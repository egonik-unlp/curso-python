#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
import numpy as np


def experimento(n):
    '''
    Calcula el promedio del score de n repeticiones de los métodos de clasificación 
    k-nn, decision Trees y Random forest.
    Pre: n número enteri
    Pos: impresión del score promedio de n repeticiones de los métodos de
    clasificación k-nn, decision Trees y Random forest.
    '''
    iris_dataset = load_iris()
    scores = np.zeros((3, n))
    
    for i in range(n):
        X_train, X_test, y_train, y_test = train_test_split(
            iris_dataset['data'], iris_dataset['target'])

        #k-nn
        knn = KNeighborsClassifier(n_neighbors = 1)
        knn.fit(X_train, y_train)
        scores[0,i] = knn.score(X_test, y_test)
        
        #Tree
        clf = DecisionTreeClassifier()
        clf.fit(X_train, y_train)
        scores[1,i] = clf.score(X_test, y_test)
        
        #Random forest
        rfc = RandomForestClassifier()
        rfc.fit(X_train, y_train)
        scores[2,i] = rfc.score(X_test, y_test)
        
    promedio = scores.mean(axis = 1)
    print(f'Test set score: \n k-nn {promedio[0]:>19.3f} \n Decision tree {promedio[1]:>10.3f} \n Random Forest {promedio[0]:>10.3f}')

experimento(100)


