#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#time_ordenamiento.py


def ord_seleccion(lista):
    """Ordena una lista de elementos según el método de selección.
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""

    # posición final del segmento a tratar
    n = len(lista) - 1

    # mientras haya al menos 2 elementos para ordenar
    while n > 0:
        # posición del mayor valor del segmento
        p = buscar_max(lista, 0, n)
        # intercambiar el valor que está en p con el valor que
        # está en la última posición del segmento
        lista[p], lista[n] = lista[n], lista[p]

        # reducir el segmento en 1
        n = n - 1

def buscar_max(lista, a, b):
    """Devuelve la posición del máximo elemento en un segmento de
       lista de elementos comparables.
       La lista no debe ser vacía.
       a y b son las posiciones inicial y final del segmento"""

    pos_max = a
    for i in range(a + 1, b + 1):
        if lista[i] > lista[pos_max]:
            pos_max = i
    return pos_max

#%%
def ord_insercion(lista):
    """Ordena una lista de elementos según el método de inserción.
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""
       
    for i in range(len(lista) - 1):
        # Si el elemento de la posición i+1 está desordenado respecto
        # al de la posición i, reubicarlo dentro del segmento [0:i]
        if lista[i + 1] < lista[i]:
            reubicar(lista, i + 1)


def reubicar(lista, p):
    """Reubica al elemento que está en la posición p de la lista
       dentro del segmento [0:p-1].
       Pre: p tiene que ser una posicion válida de lista."""

    v = lista[p]

    # Recorrer el segmento [0:p-1] de derecha a izquierda hasta
    # encontrar la posición j tal que lista[j-1] <= v < lista[j].
    j = p
    comp = 0
    while j > 0 and v < lista[j - 1]:
        # Desplazar los elementos hacia la derecha, dejando lugar
        # para insertar el elemento v donde corresponda.
        lista[j] = lista[j - 1]
        j -= 1

    lista[j] = v
    return comp

#%%
def ord_burbujeo(lista):
    '''Ordena una lista de elementos según el ordenamiento de burbuja.
    Pre: los elementos de la lista deben ser comparables.
    Post: la lista está ordenada.    
    '''
    n = len(lista)
    while n > 1:
        aux = False
        for i in range(1, n):
            if lista[i-1] > lista[i]:
                invertir(lista, i-1, i)
                aux = True
        n -= 1
        if not aux: #si no hubo inversiones detengo el algoritmo
            break  

def invertir(lista, a, b):
    temp = lista[a]
    lista[a] = lista[b]
    lista[b] = temp

#%%
    
def merge_sort(lista):
    """Ordena lista mediante el método merge sort.
       Pre: lista debe contener elementos comparables.
       Devuelve: una nueva lista ordenada."""
       
    if len(lista) < 2:
        return lista
    else:
        medio = len(lista) // 2
        izq = merge_sort(lista[:medio])
        der = merge_sort(lista[medio:])
        lista_nueva = merge(izq, der)
    return lista_nueva

def merge(lista1, lista2):
    """Intercala los elementos de lista1 y lista2 de forma ordenada.
       Pre: lista1 y lista2 deben estar ordenadas.
       Devuelve: una lista con los elementos de lista1 y lista2."""
    i, j = 0, 0
    resultado = []
    
    while(i < len(lista1) and j < len(lista2)):
        if (lista1[i] < lista2[j]):
            resultado.append(lista1[i])
            i += 1
        else:
            resultado.append(lista2[j])
            j += 1

    # Agregar lo que falta de una lista
    resultado += lista1[i:]
    resultado += lista2[j:]

    return resultado

#%%
    
import timeit as tt
import numpy as np
import matplotlib.pyplot as plt
import random

def generar_lista(N): 
    '''
    Genera una lista con número random entre 1 y 1000 de largo N.
    Pre: N es un entero mayor a cero.
    Pos: Lista de nº enteros random de lago N.
    '''
    return [random.randint(1, 1000) for i in range(N)]


def experimento_timeit_orden(listas, num):
    """
    Realiza un experimento usando timeit para evaluar los cuatro métodos
    de ordenamiento de listas con las listas pasadas como entrada
    y devuelve los tiempos de ejecución para cada lista
    en una tupla de cuatro vectores.
    El parámetro 'listas' debe ser una lista de listas.
    El parámetro 'num' indica la cantidad de repeticiones a ejecutar el método para cada lista.
    """
    tiempos_seleccion = []
    tiempos_insercion = []
    tiempos_burbuja = []
    tiempos_mergesort = []
    
    global lista
    
    for lista in listas:
     
        # evalúo el método de selección
        # en una copia nueva para cada iteración
        tiempo_seleccion = tt.timeit('ord_seleccion(lista.copy())', number = num, globals = globals())
        tiempo_insercion = tt.timeit('ord_insercion(lista.copy())', number = num, globals = globals())
        tiempo_burbuja = tt.timeit('ord_burbujeo(lista.copy())', number = num, globals = globals())
        tiempo_mergesort = tt.timeit('merge_sort(lista.copy())', number = num, globals = globals())
        # guardo el resultado
        tiempos_seleccion.append(tiempo_seleccion)
        tiempos_insercion.append(tiempo_insercion)
        tiempos_burbuja.append(tiempo_burbuja)
        tiempos_mergesort.append(tiempo_mergesort)
        
    # paso los tiempos a arrays
    tiempos_seleccion = np.array(tiempos_seleccion)
    tiempos_insercion = np.array(tiempos_insercion)
    tiempos_burbuja = np.array(tiempos_burbuja)
    tiempos_mergesort = np.array(tiempos_mergesort)
    
    return np.array([tiempos_seleccion, tiempos_insercion, tiempos_burbuja, tiempos_mergesort])


listas = []
for N in range(1, 257):
    listas.append(generar_lista(N))

tiempos_orden = experimento_timeit_orden(listas, 10)

plt.plot(tiempos_orden[0], label = 'Selección')
plt.plot(tiempos_orden[1], label = 'Inserción')
plt.plot(tiempos_orden[2], label = 'Burbujeo')
plt.plot(tiempos_orden[3], label = 'Merge sort')
plt.title('Comparación de tiempos de ejecución')
plt.ylabel('Tiempo (s)')
plt.xlabel('Largo de la cadena')
plt.legend()
plt.show()