#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#comparaciones_ordenamiento.py

def ord_seleccion(lista):
    """Ordena una lista de elementos según el método de selección.
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""

    # posición final del segmento a tratar
    n = len(lista) - 1
    comp_o = 0
    # mientras haya al menos 2 elementos para ordenar
    while n > 0:
        # posición del mayor valor del segmento
        p, comp = buscar_max(lista, 0, n)
        comp_o += comp
        # intercambiar el valor que está en p con el valor que
        # está en la última posición del segmento
        lista[p], lista[n] = lista[n], lista[p]
#        print("DEBUG: ", p, n, lista)

        # reducir el segmento en 1
        n = n - 1
    return comp_o

def buscar_max(lista, a, b):
    """Devuelve la posición del máximo elemento en un segmento de
       lista de elementos comparables.
       La lista no debe ser vacía.
       a y b son las posiciones inicial y final del segmento"""
    comp = 0
    pos_max = a
    for i in range(a + 1, b + 1):
        comp +=1
        if lista[i] > lista[pos_max]:
            pos_max = i
    return pos_max, comp

#%%
def ord_insercion(lista):
    """Ordena una lista de elementos según el método de inserción.
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""
    comp_o = 0
    for i in range(len(lista) - 1):
        # Si el elemento de la posición i+1 está desordenado respecto
        # al de la posición i, reubicarlo dentro del segmento [0:i]
        if lista[i + 1] < lista[i]:
            comp_o += reubicar(lista, i + 1)
        comp_o +=1
#        print("DEBUG: ", lista)
    return comp_o

def reubicar(lista, p):
    """Reubica al elemento que está en la posición p de la lista
       dentro del segmento [0:p-1].
       Pre: p tiene que ser una posicion válida de lista."""

    v = lista[p]

    # Recorrer el segmento [0:p-1] de derecha a izquierda hasta
    # encontrar la posición j tal que lista[j-1] <= v < lista[j].
    j = p
    comp = 0
    while j > 0 and v < lista[j - 1]:
        # Desplazar los elementos hacia la derecha, dejando lugar
        # para insertar el elemento v donde corresponda.
        lista[j] = lista[j - 1]
        j -= 1
        comp +=1

    lista[j] = v
    return comp

#%%
def ord_burbujeo(lista):
    '''Ordena una lista de elementos según el ordenamiento de burbuja.
    Pre: los elementos de la lista deben ser comparables.
    Post: la lista está ordenada.    
    '''
    comp = 0
    n = len(lista)
    while n > 1:
        aux = False
        for i in range(1, n):
            comp +=1
            if lista[i-1] > lista[i]:
                invertir(lista, i-1, i)
                aux = True
        n -= 1
        if not aux: #si no hubo inversiones detengo el algoritmo
            break  
    return comp

def invertir(lista, a, b):
    temp = lista[a]
    lista[a] = lista[b]
    lista[b] = temp

#%%
    
def merge_sort(lista):
    """Ordena lista mediante el método merge sort.
       Pre: lista debe contener elementos comparables.
       Devuelve: una nueva lista ordenada."""
       
    if len(lista) < 2:
        return lista, 0
    else:
        medio = len(lista) // 2
        izq, counti = merge_sort(lista[:medio])
        der, countd = merge_sort(lista[medio:])
        lista_nueva, comp1 = merge(izq, der)
    count = countd + counti + comp1
    return lista_nueva, count

def merge(lista1, lista2):
    """Intercala los elementos de lista1 y lista2 de forma ordenada.
       Pre: lista1 y lista2 deben estar ordenadas.
       Devuelve: una lista con los elementos de lista1 y lista2."""
    i, j = 0, 0
    resultado = []
    comp = 0
    
    while(i < len(lista1) and j < len(lista2)):
        comp += 1
        if (lista1[i] < lista2[j]):
            resultado.append(lista1[i])
            i += 1
        else:
            resultado.append(lista2[j])
            j += 1

    # Agregar lo que falta de una lista
    resultado += lista1[i:]
    resultado += lista2[j:]

    return resultado, comp

#%%
import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model

def generar_lista(N): 
    '''
    Genera una lista con número random entre 1 y 1000 de largo N.
    Pre: N es un entero mayor a cero.
    Pos: Lista de nº enteros random de lago N.
    '''
    return [random.randint(1, 1000) for i in range(N)]

def comparaciones_ordenamiento(N):
    '''
    Devuelve un vector de N elemntos con la cantidad de comparaciones que hacen 
    los algoritmos de ordenamiento por selección, inserción, burbuja y merge sort
    en listas de tamaño 1 hasta N con números random entre 1 y 1000.
    Pre: N es un entero mayor a cero.
    Pos: tupla con 4 vectores de N elementos correspondiente al nº de comparaciones 
    que realizan los algoritmos de ordenamiento por selección, inserción, burbuja y
    merge sort en listas de tamaño 1 hasta N.
    '''
    comparaciones_seleccion = np.random.rand(N)
    comparaciones_insercion = np.random.rand(N)
    comparaciones_burbujeo = np.random.rand(N)
    comparaciones_mergesort = np.random.rand(N)
    
    for i in range(1,N+1):
        lista = generar_lista(i)
        comparaciones_seleccion[i-1] = ord_seleccion(lista[:])
        comparaciones_insercion[i-1] = ord_insercion(lista[:])
        comparaciones_burbujeo[i-1] = ord_burbujeo(lista[:])
        comparaciones_mergesort[i-1] = merge_sort(lista[:])[1]
        
    return (comparaciones_seleccion, comparaciones_insercion, comparaciones_burbujeo, comparaciones_mergesort)


x = np.arange(1,257).reshape(-1,1)
xc= x**2
y = comparaciones_ordenamiento(256)

#Regresión cuadrática para que la curva de Inserción sea suave 
lm1 = linear_model.LinearRegression()   
lm1.fit(xc, np.array(y[1]).reshape(-1,1))
#Regresión cuadrática para que la curva de Burbuja sea suave 
lm2 = linear_model.LinearRegression()   
lm2.fit(xc, np.array(y[2]).reshape(-1,1))


plt.plot(x, y[0], c = 'lime', label = 'Selección')
plt.plot(x, lm1.predict(xc), c = 'blue', label = 'Inserción')
#plt.plot(x, y[1], c = 'gray', label = 'Inserción') #Curva sin regresión
plt.plot(x, lm2.predict(xc), c = 'red', label = 'Burbuja', linestyle='dashed')
plt.plot(x, y[3], c = 'orangered', label = 'Merge sort', linestyle='dashed')
plt.title('Comparación de algoritmos de ordenamiento')
plt.xlabel('Tamaño de la lista')
plt.ylabel('Número de comparaciones')
plt.legend()
plt.show()

'''
Se puede observar que el ordenamiento por selección y burbuja presentan una
complejidad similar de orden cuadrático, mientras que el ordenamiento por
inserción muestra el mismo orden de complejidad pero realiza un número 
significativamente menor de comparaciones. Sin embargo, el algoritmo que
mejor se desempeña es el merge sort, el cual muestra un comportamiento
que podría aproximarse al lineal y realizaando así un número de comparaciones
aún menor que el de inserción.
'''

