#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#burbujeo.py
#Ejercicio 11.2: burbujeo

def ord_burbujeo(lista):
    '''Ordena una lista de elementos según el ordenamiento de burbuja.
    Pre: los elementos de la lista deben ser comparables.
    Post: la lista está ordenada.    
    '''
    
    n = len(lista)
    while n > 1:
        aux = False
        for i in range(1, n):
            if lista[i-1] > lista[i]:
                invertir(lista, i-1, i)
                aux = True
        n -= 1
        if not aux: #si no hubo inversiones detengo el algoritmo
            break        
                
def invertir(lista, a, b):
    temp = lista[a]
    lista[a] = lista[b]
    lista[b] = temp
      
'''
En el peor de los casos, el algoritmo puede consumir tiempo del orden de n^2 (O(n^2)).
Este caso se da cuando el menor de los números esté en la última posición. Si así
fuera, hay que hacer n-1 intercambios para que este número llegue a la posción cero.
Pero como los números solo se intercambian una sola vez en cada pasada (excepto el mayor)
hay que hacer n-1 recorridos, en los cuales se recorre la lista n-1 veces
primero, en el segundo n-2 y así sucesivamente. 
Por lo tanto tenemos:
T(N) ~ c * (1 + 2 + 3 + 4 + ... + (n-1)) ~ c * (n-1)*n/2 ~ c * n²
'''    
lista_1 = [1, 2, -3, 8, 1, 5]
ord_burbujeo(lista_1)
lista_1
lista_2 = [1, 2, 3, 4, 5]
ord_burbujeo(lista_2)
lista_2
lista_3 = [0, 9, 3, 8, 5, 3, 2, 4]
ord_burbujeo(lista_3)
lista_3
lista_4 = [10, 8, 6, 2, -2, -5]
ord_burbujeo(lista_4)
lista_4
lista_5 = [2, 5, 1, 0]
ord_burbujeo(lista_5)
lista_5
