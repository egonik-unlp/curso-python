#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#iris_seaborn.py

import seaborn as sns
from sklearn.datasets import load_iris
import pandas as pd

iris_dataset = load_iris()
iris_dataframe = pd.DataFrame(iris_dataset['data'], columns = iris_dataset.feature_names)
iris_dataframe['target'] = iris_dataset['target']

#Gráfico
g = sns.pairplot(data = iris_dataframe, hue = 'target', palette = 'pastel', markers=["o", "s", "D"])
title = 'Flores Iris'
g._legend.set_title(title)
labels = ['setosa', 'versicolor', 'virginica']
for t, l in zip(g._legend.texts, labels): 
    t.set_text(l)    
g.fig.subplots_adjust(top=0.968, bottom=0.08, left=0.07, right=0.88, hspace=0.056, wspace=0.075)

