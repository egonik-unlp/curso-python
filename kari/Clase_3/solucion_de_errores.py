# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#solucion_de_errores.py

#Ejercicios de errores en el código
#%%
#Ejercicio 3.1. Función tiene_a()
#Comentario: El error está en que el programa evalúa solo la primera letra de la expresión. Si esta es verdadera o falsa, sale y no sigue con la otra.
#   Lo corregí eliminando el 'else' y devolviendo el False solo cuando termine el ciclo while.
#   A continuación va el código corregido
    
def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False
        

tiene_a('UNSAM 2020')
tiene_a('abracadabra')
tiene_a('La novela 1984 de George Orwell')

#%%
#Ejercicio 3.2: Sintaxis
#Comentario: El error estaba en la sintaxis, faltaban los dos puntos ':' al final de 'tiene_a(expresion)' y del if. Además, la evaluación del condicional se escribió con un solo = y al final los booleanos en python tiene que escribirse en inglés 'False'. 
#   Lo corregí agregando ':' en dónde se indicó previamente, cambiando el '=' por '==' y el 'Falso' por 'False'.
#   A continuación va el código corregido

def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False

tiene_a('UNSAM 2020')
tiene_a('La novela 1984 de George Orwell')


#%%
#Ejercicio 3.3: Tipos
#Comentario: El error estaba en que al aplicar la función al número 1984 al ser un entero (tipo int), la función len() no se puede aplicar porque es para cadenas.
#   Lo corregí cambiando el número 1984 a cadena, es decir '1984'.
#   A continuación va el código corregido

def tiene_uno(expresion):
    n = len(expresion)
    i = 0
    tiene = False
    while (i<n) and not tiene:
        if expresion[i] == '1':
            tiene = True
        i += 1
    return tiene


tiene_uno('UNSAM 2020')
tiene_uno('La novela 1984 de George Orwell')
tiene_uno('1984')

#%%
#Ejercicio 3.4: Alcances
#Comentario: La función no devolvía ningún valor (devolvía None).
#   Lo corregí agregando un 'return c' al final de la definición de la función.
#   A continuación va el código corregido

def suma(a,b):
    c = a + b
    return c
    
a = 2
b = 3
c = suma(a,b)
print(f"La suma da {a} + {b} = {c}")

#%%
#Ejercicio 3.5: Pisando memoria
#Comentario: El error estaba en definir el diccionario 'registro' fuera del ciclo for. Ya que cuando se hace el append la variable 'camion' queda apuntando al mismo lugar de memoria que registro y cuando se modifica el registro se modifica ese lugar de memoria provocando que finalmente la lista camion tenga x componentes apuntando a registro que es solo la última fila del csv..
#   Lo corregí creando el diccionario vacío 'registro' dentro del ciclo for.
#   A continuación va el código corregido

import csv
from pprint import pprint

def leer_camion(nombre_archivo):
    camion=[]
    with open(nombre_archivo,"rt") as f:
        filas = csv.reader(f)
        encabezado = next(filas)
        for fila in filas:
            registro={}
            registro[encabezado[0]] = fila[0]
            registro[encabezado[1]] = int(fila[1])
            registro[encabezado[2]] = float(fila[2])
            camion.append(registro)

    return camion

camion = leer_camion("Data/camion.csv")
pprint(camion)

