#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 10:57:37 2020

@author: karina
"""

#busqueda_en_listas.py
#Ejercicio 3.6: Búsquedas de un elemento

#%%
def buscar_u_elemento(lista, elemento):    
    '''Dada una lista, indica la posición de la última 
    aparición del elemento en la misma.
    '''    
    posicion = -1
    for i, v in enumerate(lista):
        if v == elemento:
            posicion = i            
    return posicion
    
buscar_u_elemento([1,2,3,2,3,4],1) # Resultado:  0
buscar_u_elemento([1,2,3,2,3,4],2) # Resultado:  3
buscar_u_elemento([1,2,3,2,3,4],3) # Resultado:  4
buscar_u_elemento([1,2,3,2,3,4],5) # Resultado: -1

#%%

def buscar_n_elemento(lista, elemento):    
     '''Dada una lista, indica el número de veces que 
    aparece el elemento en la misma.
    '''    
    i = 0
    for v in lista:
        if v == elemento:
            i+= 1
    return i

buscar_n_elemento([1,2,3,2,3,4,3,3],1) # Resultado: 1
buscar_n_elemento([1,2,3,2,3,4,3,3],2) # Resultado: 2
buscar_n_elemento([1,2,3,2,3,4,3,3],3) # Resultado: 4
buscar_n_elemento([1,2,3,2,3,4,3,3],5) # Resultado: 0

#%%
#Ejercicio 3.7: Búsqueda de máximo y mínimo

def maximo(lista):
    '''Devuelve el máximo de una lista, 
    la lista debe ser no vacía.
    '''
    import math
    max = -math.inf
    for i in lista:
        if i > max:
            max = i
    return max
    
maximo([1,2,7,2,3,4]) # Resultado: 7
maximo([1,2,3,4]) # Resultado: 4
maximo([-5,4]) # Resultado: 4
maximo([-5,-4]) # Resultado: -4


#%%

def minimo(lista):
    '''Devuelve el mínimo de una lista, 
    la lista debe ser no vacía.
    '''
    import math
    min = math.inf
    for i in lista:
        if i < min:
            min = i
    return min
    
minimo([1,2,7,2,3,4]) # Resultado: 1
minimo([0,1,2,3,4]) # Resultado: 0
minimo([-5,4]) # Resultado: -5
minimo([-15,-5,-4]) # Resultado: -15

   