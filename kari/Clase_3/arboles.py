#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 17:12:45 2020

@author: karina
"""

#arboles.py

#Ejercicio 3.18: Lectura de todos los árboles

def leer_parque(nombre_archivo):
    import csv
    with open(nombre_archivo, 'rt') as file:
        data = csv.reader(file)
        header = next(data)
        tipo = [float, float, int, int, int, int, int, str, str, str, str, str, str, str, str, float, float]
        arboleda = [{head: func(val) for head, func, val in zip(header, tipo, row)} for row in data]
    return arboleda

arboleda = leer_parque('Data/arbolado.csv')

#%%

#Ejercicio 3.19: Lista de altos de Jacarandá

h = [arbol['altura_tot'] for arbol in arboleda if arbol['nombre_com'] == 'Jacarandá']

#%%

#Ejercicio 3.20: Lista de altos y diámetros de Jacarandá

h_diam = [(arbol['altura_tot'], arbol['diametro']) for arbol in arboleda if arbol['nombre_com'] == 'Jacarandá']

#%%

#Ejercicio 3.21: Diccionario con medidas

def medidas_de_especies(especies,arboleda):
    h_diam_especie = {especie: [(arbol['altura_tot'], arbol['diametro']) for arbol in arboleda if arbol['nombre_com'] == especie] for especie in especies} 
    return h_diam_especie


especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']
hyd = medidas_de_especies(especies, arboleda)
