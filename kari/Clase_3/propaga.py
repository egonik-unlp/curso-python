#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 13:17:34 2020

@author: karina
"""

#propaga.py
#Ejercicio 3.9: Propagación

def propagar(lista):
    for i in range(len(lista)-1):   #Recorro la lista desde el inicio.
        if lista[i] == 1 and lista[i+1] == 0:
            lista[i+1] = 1
    for i in range(len(lista)-1, 0, -1):    #Recorro la lista desde el final hacia el inicio.
        if lista[i] == 1 and lista[i-1] == 0:
            lista[i-1] = 1
    return lista
                
propagar([ 0, 0, 0,-1, 1, 0, 0, 0,-1, 0, 1, 0, 0])
# Out: [0, 0, 0, -1, 1, 1, 1, 1, -1, 1, 1, 1, 1]
propagar([ 0, 0, 0, 1, 0, 0])
# Out: [1, 1, 1, 1, 1, 1]             
