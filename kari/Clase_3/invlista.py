#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 11:58:23 2020

@author: karina
"""

#invlista.py
#Ejercicio 3.8: Invertir una lista

def invertir_lista(lista):
    invertida = []
    for e in lista: 
        invertida.insert(0, e)
    return invertida


invertir_lista([1, 2, 3, 4, 5])
# Out: [5, 4, 3, 2, 1]
invertir_lista(['Bogotá', 'Rosario', 'Santiago', 'San Fernando', 'San Miguel'])
# Out: ['San Miguel', 'San Fernando', 'Santiago', 'Rosario', 'Bogotá']
