#generala.py
#Ejercicio 4.7: Generala no necesariamente servida


def tirar(n):
    '''
    Devuelve una lista con cinco dados generados aleatoriamente.
    '''
    import random
    tirada = [random.randint(1,6) for i in range(n)]
    return tirada


def buscar_generala():
    '''
    Devuelve una lista de cinco dados luego de tres tiradas en donde se
    conservaron los dados que más se repiten en cada tirada.
    '''
    from collections import Counter  
    tirada = tirar(5)
    for v in range(2):  #Hago dos tiradas más guardando los dados que más se repitieron.
        contar = Counter()      
        for i in tirada:
           contar[i] += 1 #Cuento los dados se obtuvieron
        mas_salio = contar.most_common(1) #Busco el dado que se repitó en la tirada (es una lista con una tupla)
        dado_rep = mas_salio[0][0] #Dado que más salió
        veces_salio = mas_salio[0][1]  #Veces que salió el dado
        tirada = [dado_rep]*veces_salio  + tirar(5 - veces_salio) #Reemplazo tirada por el dado que más salio, las veces que salió y le sumo una tirada de los dados restantes.
    return tirada  
      
def es_generala(lista):
    '''
    Devuelve True si y sólo si los cinco dados de la lista tirada son iguales.
    '''
    es = True
    n = len(lista)
    for i in range(n-1):
        if lista[i] != lista[i+1]:
            es = False
            return es
    return es

N=1000000
G = sum([es_generala(buscar_generala()) for i in range(N)])
prob = G/N
print(f'Tiré {N} veces, de las cuales {G} saqué generala.')
print(f'Podemos estimar la probabilidad de sacar generala servida mediante {prob:.6f}.')

#Out
#Tiré 1000000 veces, de las cuales 45909 saqué generala.
#Podemos estimar la probabilidad de sacar generala servida mediante 0.045909.