#figuritas.py

import random
import numpy as np 

def crear_album(figus_total):
    album = np.zeros(figus_total, dtype=np.int64)
    return album
    
def album_incompleto(album):
    incompleto = False
    if 0 in album:
        incompleto = True
    return incompleto

def comprar_figus(figus_total):
    figurita = random.randint(1, figus_total)
    return figurita

def comprar_paquete(figus_total, figus_paquete):
    paquete = np.array([random.randint(1, figus_total) for i in range(figus_paquete)])
    return paquete

#Con figus individuales
def cuantas_figus(figus_total):
    album = crear_album(figus_total)
    while album_incompleto(album):
        figurita = comprar_figus(figus_total)
        album[figurita-1] += 1  #Ej. la figurita 1 está en la posición cero.
    return sum(album)

#Con paquetes
def cuantos_paquetes(figus_total, figus_paquete):
    album = crear_album(figus_total)
    n_paquetes = 0
    while album_incompleto(album):
        n_paquetes += 1
        paquete = comprar_paquete(figus_total, figus_paquete)
        for i in paquete:
            album[i-1] += 1  #Ej. la figurita 1 está en la posición cero.
    return n_paquetes


#Ejercicio 4.25:Probabilidad de llenar el albun con n_paquetes

n_repeticiones = 100
figus_total = 670
figus_paquete = 5
n_paquetes = 850

n_paquetes_hasta_llenar = np.array([cuantos_paquetes(figus_total,figus_paquete) for i in range(n_repeticiones)])
probabilidad = np.mean(n_paquetes_hasta_llenar <= n_paquetes)
print(f'La probabilidad de completar el album con menos de {n_paquetes} paquetes es {probabilidad:.5f}') 


#Ejercicio 4.26: Plotear el histograma
import matplotlib.pyplot as plt
n_paquetes_hasta_llenar = np.array([cuantos_paquetes(figus_total,figus_paquete) for i in range(n_repeticiones)])
plt.hist(n_paquetes_hasta_llenar,bins=45)

#Ejercicio 4.27: Chance del 90% de completar el album

cuanto = 0.9
for i in range(max(n_paquetes_hasta_llenar)):
    if np.mean(n_paquetes_hasta_llenar <= i) >= cuanto:
        print(f'Necesito {i} paquetes para tener una probabilidad del {cuanto} de llenar el album.')
        break


#Ejercicio 4.28: sin figus repetidas
def comprar_paquete_sin_repetir(figus_total, figus_paquete):
    figus = [i for i in range(figus_total)]
    paquete= np.array(random.sample(figus, k = figus_paquete))   
    return paquete

def cuantos_paquetes_sin_repetir(figus_total, figus_paquete):
    album = crear_album(figus_total)
    n_paquetes = 0
    while album_incompleto(album):
        n_paquetes += 1
        paquete = comprar_paquete_sin_repetir(figus_total, figus_paquete)
        for i in paquete:
            album[i-1] += 1  #Ej. la figurita 1 está en la posición cero.
    return n_paquetes

#Probabilidad

n_repeticiones = 100
figus_total = 670
figus_paquete = 5
n_paquetes = 850
n_paquetes_hasta_llenar_v2 = np.array([cuantos_paquetes_sin_repetir(figus_total,figus_paquete) for i in range(n_repeticiones)])
probabilidad_v2 = np.mean(n_paquetes_hasta_llenar_v2 <= n_paquetes)
print(f'La probabilidad de completar el album con menos de {n_paquetes} paquetes sin figuritas repetidas es {probabilidad_v2:.5f}') 

# Chance del 90% de completar el album

cuanto = 0.9
for i in range(max(n_paquetes_hasta_llenar_v2)):
    if np.mean(n_paquetes_hasta_llenar_v2 <= i) >= cuanto:
        print(f'Necesito {i} paquetes sin figuritas repetidas para tener una probabilidad del {cuanto} de llenar el album.')
        break

#Ejercicio 4.29: Cooperar vs competir
def cuantos_paquetes_amigos(figus_total, figus_paquete, n_amigos):
    albums = np.zeros(shape = (n_amigos, figus_total)) #Creo matriz donde cada fila es un amigo
    n_paquetes = 0
    while album_incompleto(albums):
        n_paquetes += 1
        paquete = comprar_paquete(figus_total, figus_paquete)
        for i in paquete:
            for v in range(n_amigos): #Recorre cada album
                if albums[v][i-1] == 0:
                    albums[v][i-1] += 1  #Ej. la figurita 1 está en la posición cero del album v.
                    break #Rompo el ciclo para evitar que le ponga la misma figurita en todos los albums
                else:
                    pass
    return n_paquetes

#Probabilidad de llenar el albun con n_paquetes

n_repeticiones = 100
figus_total = 670
figus_paquete = 5
n_paquetes = 400
n_amigos = 5

n_paquetes_hasta_llenar = np.array([cuantos_paquetes_amigos(figus_total,figus_paquete, n_amigos) for i in range(n_repeticiones)])
probabilidad = np.mean(n_paquetes_hasta_llenar <= (n_amigos * n_paquetes))
print(f'La probabilidad de completar el album con menos de {n_paquetes} paquetes cada amigo es {probabilidad:.5f}') 


#Chance del 90% de completar el album

cuanto = 0.9
for i in range(max(n_paquetes_hasta_llenar)):
    if np.mean(n_paquetes_hasta_llenar <= i) >= cuanto:
        print(f'Necesito {i} paquetes para tener una probabilidad del {cuanto} de llenar los {n_amigos} albums.')
        break

'''
Out
La probabilidad de completar el album con menos de 850 paquetes es 0.34000
Necesito 1168 paquetes para tener una probabilidad del 0.9 de llenar el album.
La probabilidad de completar el album con menos de 850 paquetes sin figuritas repetidas es 0.29000
Necesito 1077 paquetes sin figuritas repetidas para tener una probabilidad del 0.9 de llenar el album.
La probabilidad de completar el album con menos de 400 paquetes cada amigo es 0.59000
Necesito 2244 paquetes para tener una probabilidad del 0.9 de llenar los 5 albums.
'''