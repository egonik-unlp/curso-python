#plotear_temperaturas.py
#Ejercicio 4.14: Empezando a plotear

def plotear_temperaturas(archivo):
    
    import matplotlib.pyplot as plt
    import numpy as np
    temperaturas = np.load(archivo)
    plt.hist(temperaturas,bins=50)
    plt.show()

plotear_temperaturas('Data/Temperaturas.npy')
    
