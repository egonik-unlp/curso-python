#termometro.py
#Ejercicio 4.11: Gaussiana y Ejercicio 4.13: Guardar temperaturas

def termometro(N, temp_real, dstd):
    '''
    Dado el valor real de temperatura y la desviación estandar
    genera N mediciones de temperatura con errores que siguen una
    distribución Gaussiana e imprime el valor máximo, mínimo, la media,
    mediana, el Q1 y Q3.
    '''
    
    import random
    import math
    import numpy as np
    temp = [random.normalvariate(temp_real, dstd) for i in range(N)]
    np.save('Data/Temperaturas', temp)
    temp_ord = sorted(temp)
    maximo = max(temp)
    minimo = min(temp)
    media = sum(temp)/len(temp)
    if N% 2 == 0: #N mediciones pares
        mediana = (temp_ord[int(N/2)] + temp_ord[int(N/2 + 1)])/2 #Promedio de los valores que están en el medio
    else:
        mediana = temp_ord[int((N + 1)/2)]
    if (N + 1) % 4 == 0: #N mediciones que pueden dividirse en un número de cuartiles enteros
        Q1 = temp_ord[int((N + 1)/4)]
        Q3 = temp_ord[int(3*(N + 1)/4)]
    else: #Para cuartiles con decimales
        #Se calculan con la formula Qk = k(N+1)/4 siendo k el cuartil. 
        #Al ser decimal se calcula como Qk = temp[i] + d*(temp[i] - temp[i+1]) siendo d la parte decimal al calcular Qk e i la parte entera de Qk (Qk está entre el elemento i e i+1)
        i1 = math.trunc((N + 1)/4) #Parte entera de Q1
        d1 = ((N + 1)/4 - i1) #Parte decimal de Q1
        Q1 = temp[i1] + d1*(temp[i1] - temp[i1+1])   
        i3 = math.trunc(3*(N + 1)/4)
        d3 = (3*(N + 1)/4 - i3) #Q3 = 3*Q1
        Q3 =  temp[i3] + d3*(temp[i3] - temp[i3+1])  
    print(f'Valor máximo: {maximo:.2f}') 
    print(f'Valor minimo: {minimo:.2f}')
    print(f'Media: {media:.2f}')
    print(f'Mediana (Q2): {mediana:.2f}')
    print(f'Q1: {Q1:.2f}')
    print(f'Q3: {Q3:.2f}')
    
termometro(999, 37.5, 0.2)

'''
Out
Valor máximo: 38.08
Valor minimo: 36.86
Media: 37.49
Mediana (Q2): 37.49
Q1: 37.36
Q3: 37.62
'''