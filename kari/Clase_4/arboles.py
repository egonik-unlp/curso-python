#arboles.py

#Ejercicio 3.18: Lectura de todos los árboles

def leer_arboles(nombre_archivo):
    import csv
    with open(nombre_archivo, 'rt') as file:
        data = csv.reader(file)
        header = next(data)
        tipo = [float, float, int, int, int, int, int, str, str, str, str, str, str, str, str, float, float]
        arboleda = [{head: func(val) for head, func, val in zip(header, tipo, row)} for row in data]
    return arboleda

arboleda = leer_arboles('Data/arbolado-en-espacios-verdes.csv')


#Ejercicio 3.19: Lista de altos de Jacarandá

altos = [arbol['altura_tot'] for arbol in arboleda if arbol['nombre_com'] == 'Jacarandá']


#Ejercicio 3.20: Lista de altos y diámetros de Jacarandá

altos_diam = [(arbol['altura_tot'], arbol['diametro']) for arbol in arboleda if arbol['nombre_com'] == 'Jacarandá']

#Ejercicio 3.21: Diccionario con medidas

def medidas_de_especies(especies,arboleda):
    diam_h_especie = {especie: [(arbol['diametro'], arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com'] == especie] for especie in especies} 
    return diam_h_especie


especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']
dyh = medidas_de_especies(especies, arboleda)

#%%
#Ejercicio 4.30: Histograma de altos de Jacarandás

def histograma_alturas(nombre_archivo, especie):
    '''
    Grafica un histograma de la altura de la especie seleccionada
    usando los datos del archivo seleccionado
    '''    
    import os
    import matplotlib.pyplot as plt
    file = os.path.join('Data', nombre_archivo)
    arboleda = leer_arboles(file)
    altos = [arbol['altura_tot'] for arbol in arboleda if arbol['nombre_com'] == especie]
    plt.hist(altos,bins=50)
    plt.show()
    
histograma_alturas('arbolado-en-espacios-verdes.csv', 'Jacarandá')

#Ejercicio 4.31: Scatterplot (diámetro vs alto) de Jacarandás

def scatterplot_dyh(nombre_archivo, especie):
    '''
    Grafica un histograma de la altura versus el diámetro
    de la especie seleccionada
    usando los datos del archivo seleccionado
    '''    
    import matplotlib.pyplot as plt
    import numpy as np
    import os
    file = os.path.join('Data', nombre_archivo)
    arboleda = leer_arboles(file)    
    h = np.array([arbol['altura_tot'] for arbol in arboleda if arbol['nombre_com'] == especie])
    d = np.array([arbol['diametro'] for arbol in arboleda if arbol['nombre_com'] == especie])
    plt.scatter(d,h, alpha = 0.25)
    plt.xlabel('Diámetro (cm)')
    plt.ylabel('Alto (m)')
    plt.title('Relación diámetro-alto para Jacarandás')
    plt.show()
    
scatterplot_dyh('arbolado-en-espacios-verdes.csv', 'Jacarandá')

#Ejercicio 4.32: Scatterplot para diferentes especies

def scatterplot_dyh_especies(nombre_archivo, especies):
    '''
    Grafica un histograma de la altura versus el diámetro
    de las especies seleccionadas
    usando los datos del archivo seleccionado
    '''    
    import os
    import matplotlib.pyplot as plt    
    file = os.path.join('Data', nombre_archivo)
    arboleda = leer_arboles(file)
    medidas = medidas_de_especies(especies, arboleda)
    for especie in especies:
        plt.scatter(*zip(*medidas[especie]), alpha = 0.40, label = especie) 
        plt.xlabel('Diámetro (cm)')
        plt.ylabel('Alto (m)')
        plt.title('Relación diámetro-alto para '+ especie)
        plt.ylim(0,50) 
        plt.xlim(0,120)
    plt.legend()
    plt.show()

especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']

scatterplot_dyh_especies('arbolado-en-espacios-verdes.csv', especies)


