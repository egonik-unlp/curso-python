#envido.py
#Ejercicio 4.8: Envido


def repartir_cartas():
    import random
    valores = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12]
    palos = ['oro', 'copa', 'espada', 'basto']
    naipes = [[valor, palo] for valor in valores for palo in palos]
    mano = random.sample(naipes,k=3)
    return mano

def es_envido(lista, cuanto): 
    '''
    Devuelve True cuando la mano (lista) tiene envido igual al valor dado en cuanto.
    La función solo es válida para envido igual o mayor a 21.
    '''
    es = False
    resta = cuanto - 20
    negras = [10, 11, 12]
    lista_c = lista.copy() 
    for i in lista_c:   #Sustituyo por cero el valor de las cartas negras (10, 11, 12)
        if i[0] in negras:
            i[0] = 0
    if lista_c[0][1] == lista_c[1][1] == lista_c[2][1]: #Caso en donde tengo flor y solo se considera que los dos cartas mayores forman el envido.
        lista_ord = sorted(lista_c, reverse = True)
        if lista_ord[0][0] + lista_ord[1][0] == resta: #Al ordenar puedo sumar las dos cartas mayores ubicadas en los dos primeros lugares.
            es = True
    else:    
        for i in lista_c:        
            for v in range(len(lista_c)-1):
                if lista_c[v][1] == i[1] and i[0] != lista_c[v][0] and i[0] + lista_c[v][0] == resta: #Primero verifico si son del mismo palo luego que no sea si mismo y luego que la suma da el envido que busco.
                    es = True
    return es

for cuanto in range(31,34):
    N=100000
    G = sum([es_envido(repartir_cartas(), cuanto) for i in range(N)])
    prob = G/N
    print(f'- Tiré {N} veces, de las cuales {G} saqué envido igual a {cuanto}.')
    print(f'  Podemos estimar la probabilidad de sacar envido igual a {cuanto} {prob:.6f}.')

'''
Out
- Tiré 1000000 veces, de las cuales 29421 saqué envido igual a 31.
  Podemos estimar la probabilidad de sacar envido igual a 31 0.029421.
- Tiré 1000000 veces, de las cuales 14952 saqué envido igual a 32.
  Podemos estimar la probabilidad de sacar envido igual a 32 0.014952.
- Tiré 1000000 veces, de las cuales 15550 saqué envido igual a 33.
  Podemos estimar la probabilidad de sacar envido igual a 33 0.015550.

 '''