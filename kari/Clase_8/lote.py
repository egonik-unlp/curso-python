#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#lote.py

class Lote:
    
    def __init__(self, nombre, cajones, precio):
        self.nombre = nombre
        self.cajones = cajones
        self.precio = precio
        
    def costo(self):
        costo = self.cajones * self.precio
        return costo
    
    def vender(self, cajones):
        self.cajones -= cajones

    def __str__(self):
        return f'({self.nombre}, {self.cajones}, {self.precio})'
    
    def __repr__(self):
        return f'Lote({self.nombre}, {self.cajones}, {self.precio})'
