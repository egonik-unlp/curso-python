#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#canguros_buenos.py
#Ejercicio 8.11: Canguros buenos y canguros malos

class Canguro():
    def __init__(self):
        self.contenido_marsupio = []
    
    def meter_en_marsupio(self, obj):
        self.contenido_marsupio.append(obj)
       
    def __str__(self):
        aux = []
        for i in self.contenido_marsupio:
            if isinstance(i, Canguro): #si i es de clase Canguro vuelve a llamar al método (recursión para que imprima todos los niveles de Canguro)
                aux.append(i.__str__())
            else:
                aux.append(f'{i}')
        return '[' + ', '.join(aux) + ']'

#%%
# canguro_malo.py
"""Este código continene un 
bug importante y dificil de ver
"""

class Canguro:
    """Un Canguro es un marsupial."""
    
    def __init__(self, nombre, contenido = []):
        """Inicializar los contenidos del marsupio.

        nombre: string
        contenido: contenido inicial del marsupio, lista.
        """
        self.nombre = nombre
        self.contenido_marsupio = contenido[:] #corrección del error

    def __str__(self):
        """devuelve una representación como cadena de este Canguro.
        """
        t = [ self.nombre + ' tiene en su marsupio:' ]
        for obj in self.contenido_marsupio:
            s = '    ' + object.__str__(obj)
            t.append(s)
        return '\n'.join(t)

    def meter_en_marsupio(self, item):
        """Agrega un nuevo item al marsupio.

        item: objecto a ser agregado
        """
        self.contenido_marsupio.append(item)

#%%

madre_canguro = Canguro('Madre')

madre_canguro.meter_en_marsupio('billetera')
madre_canguro.meter_en_marsupio('llaves del auto')
madre_canguro.meter_en_marsupio(cangurito)
cangurito = Canguro('gurito')

print(madre_canguro)
print(cangurito)

'''
El error se encuentra en crear la lista contenido en la definición del __init__
y asignársela al contenido_marsupio. Luego al realizar el método append en la
lista, esta modifica la lista contenido vacía que se creó a la cual apuntan
todas las instancias creadas de Canguro. Incluso si se reddefine madre_canguro
no se vacía la lista, sino que sigue apuntando al mismo lugar de memoria.
Esto se soluciona copiando la lista contenido en __init__ en vez de asignarla
directamente.
'''