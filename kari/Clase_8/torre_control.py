#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#torre_control.py
#Ejercicio 8.12: Torre de Control

class Cola:
    '''Representa a una cola, con operaciones de encolar y desencolar.
    El primero en ser encolado es tambien el primero en ser desencolado.
    '''

    def __init__(self):
        '''Crea una cola vacia.'''
        self.items = []

    def encolar(self, x):
        '''Encola el elemento x.'''
        self.items.append(x)

    def desencolar(self):
        '''Elimina el primer elemento de la cola 
        y devuelve su valor. 
        Si la cola esta vacia, levanta ValueError.'''
        if self.esta_vacia():
            raise ValueError('La cola esta vacia')
        return self.items.pop(0)

    def esta_vacia(self):
        '''Devuelve 
        True si la cola esta vacia, 
        False si no.'''
        return len(self.items) == 0
    
    def __str__(self): #Defino la función str
        '''
        Devuelve una cadena del objeto Cola.
        '''
        return ', '.join(self.items)
    
#%%

class TorreDeControl():
    '''
    Modela el trabajo de una torre de control de un aeropuerto con 
    una pista de aterrizaje. Los aviones que están esperando para aterrizar 
    tienen prioridad sobre los que están esperando para despegar.
    '''
    def __init__(self):
        '''
        Crea dos objetos Cola vacíos: 
            1. Vuelos por partir
            2. Vuelos por aterrizar (prioritarios)
        '''
        self.partir = Cola() 
        self.aterrizar = Cola()
    
    def nuevo_arribo(self, avion):
        '''
        Encola el avion en el atributo aterrizar.
        '''
        self.aterrizar.encolar(avion)
        
    def nueva_partida(self, avion):
        '''
        Encola el avion en el atributo partir.
        '''
        self.partir.encolar(avion)
    
    def ver_estado(self):
        '''
        Imprime los vuelos que están por aterrizar y por partir.
        '''
        print(f'Vuelos esperando para aterrizar: {self.aterrizar}')
        print(f'Vuelos esperando para despegar: {self.partir}')
    
    def asignar_pista(self):
        '''
        Asigna los vuelos a la pista de aterrizaje priorizando
        los aterrizajes sobre los depegues. Si no hay aviones en 
        espera lo indica
        '''
        try:
            if not self.aterrizar.esta_vacia():
                aux = self.aterrizar.desencolar()
                print(f'El vuelo {aux} aterrizó con éxito')
            else:
                aux = self.partir.desencolar()
                print(f'El vuelo {aux} despegó con éxito')
        except ValueError: #El método desencolar de la clase Cola levanta un ValueError si la lista está vacía, con esto lo tomo 
            print('No hay vuelos en espera.')

torre = TorreDeControl()
torre.nuevo_arribo('AR156')
torre.nueva_partida('KLM1267')
torre.nuevo_arribo('AR32')
torre.ver_estado()
torre.asignar_pista()

        
        
        
        
        
        
        