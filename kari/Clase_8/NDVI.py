#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#NDVI.py

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as color
import matplotlib.patches as mpatches
import os

#Ejercicio 8.15: Ver una banda
def crear_img_png(carpeta, banda):
    
    
    for root, dirs, files in os.walk(carpeta):
        for i in files:
            if i[-10] == str(banda):
                data = np.load(os.path.join(root, i))
                q = 0.1
                vmin = np.percentile(data.flatten(), q)
                vmax = np.percentile(data.flatten(), 100-q)      
                g = plt.figure()
                g.set_size_inches(16,8)
                g = plt.imshow(data, vmin= vmin, vmax= vmax) 
                plt.colorbar(g, shrink=0.75)     
                plt.title(f'Banda {banda}') 
                plt.savefig(f'Data/clip/img_banda{banda}.png', dpi=100)

#Ejercicio 8.16: Histogramas
def crear_hist_png(carpeta, banda, bins):
    
    for root, dirs, files in os.walk(carpeta):
        for i in files:
            if i[-10] == str(banda):
                data = np.load(os.path.join(root, i))     
                g = plt.figure()
                g.set_size_inches(16,8)
                q = 0.1
                vmin = np.percentile(data.flatten(), q)
                vmax = np.percentile(data.flatten(), 100-q)  
                plt.hist(data.flatten(), bins, range=(vmin, vmax))    
                plt.title(f'Banda {banda}') 
                plt.savefig(f'Data/clip/hist_banda{banda}.png', dpi=100)

#Ejercicio 8.17: Máscaras binarias
#a
for i in range(1,8):    
    crear_img_png('Data/clip', i)
    crear_hist_png('Data/clip', i, 100)


#b
data = []

for i in range(1,8):
    data.append(np.load(f'Data/clip/LC08_L1TP_225084_20180213_20180222_01_T1_sr_band{i}_clip.npy'))
#Tomo banda 1    
data[0] = np.where(data[0]<0.25, 0, data[0])
data[0] = np.where(data[0]>0.25, 1, data[0])

plt.imshow(data[0]) #La banda corresponde a vegetación

#Ejercicio 8.18: Clasificación manual

#NDVI

infrarrojo_cercano = data[4]
rojo = data[3]

NDVI = (infrarrojo_cercano - rojo) / (infrarrojo_cercano + rojo)

conditions = [0 > NDVI, (0 < NDVI)&(NDVI < 0.1), (0.1 < NDVI)&(NDVI < 0.25), (0.25 < NDVI)&(NDVI < 0.4),(NDVI > 0.4)]
choices = [0, 1, 2, 3, 4]
clase_ndvi = np.select(conditions, choices)


colors = ['black', 'yellow', 'yellowgreen', 'green', 'darkgreen']
bounds = [0, 1, 2, 3, 4, 5]
cmap = color.ListedColormap(colors)
norm = color.BoundaryNorm(bounds, cmap.N)
plt.title('Landsat 8 - Clases de NDVI')
label = ['Sin vegetación','Área desnuda', 'Vegetación baja', 'Vegetación moderada', 'Vegetación densa']
patches = [mpatches.Patch(color= cmap(i), label=label[i]) for i in range(len(label))]
plt.legend(handles=patches, bbox_to_anchor=(1.03, 1), loc='upper left', borderaxespad=0. )
plt.imshow(clase_ndvi, cmap = cmap, norm = norm)
plt.tight_layout()
plt.show()



