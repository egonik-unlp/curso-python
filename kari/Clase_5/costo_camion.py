#costo_camion.py

import informe_funciones as informe

def costo_camion(archivo_camion):    
    '''Lee un archivo .csv con datos sobre la carga del camión 
    (producto, nº cajones y precio) y calcula el costo total de los bienes.'''
    precio_total = 0.0
    camion = informe.leer_camion(archivo_camion)
    for line in camion:
        precio_total += line['cajones'] * line['precio']
    return(precio_total)

