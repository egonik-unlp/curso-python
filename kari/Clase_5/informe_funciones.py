#tabla_informe.py

import fileparse

#leer_camion
def leer_camion(nombre_archivo):    
    '''Lee un archivo .csv que contiene los datos de carga de un camión y devuelve una lista de diccionarios, 
    donde cada diccionario corresponde a un producto, número de cajones y precio'''   
    return fileparse.parse_csv(nombre_archivo, select=['nombre','cajones','precio'], types=[str,int,float])               

#leer_precios
def leer_precios(nombre_archivo):    
    '''Lee un archivo .csv sin encabezado con columna 1 = mercadería, columna 2 = precios y 
    devuelve un diccionario con la mercadería como clave'''
    return dict(fileparse.parse_csv(nombre_archivo,types=[str,float], has_headers=False))                  

#hacer_informe
def hacer_informe(archivo_camion, archivo_precios):
    '''Dado nu archivo .csv que contiene los datos de carga de un camión y
    un archivo .csv sin encabezado con columna 1 = mercadería y  
    columna 2 = precios, hace un informe de las ganancias por producto'''
    camion = leer_camion(archivo_camion)
    precios = leer_precios(archivo_precios)
    informe = []    
    for c_line in camion:    
        cambio = precios[c_line['nombre']] - c_line['precio'] 
        tupla = (c_line['nombre'], c_line['cajones'], precios[c_line['nombre']], cambio)        
        informe.append(tupla)    
    return informe
 
 
def imprimir_informe(informe):
    '''Imprime una tabla con el informe de ganancias/pérdidas
    de la carga de un camión'''
    encabezado = ('Nombre', 'Cajones', 'Precio', 'Cambio')
    print(f'{encabezado[0]:>10s} {encabezado[1]:>10s} {encabezado[2]:>10s} {encabezado[3]:>10s}')
    print(('-' * 10 + ' ') * len(encabezado))
    for nombre, cajones, precio, cambio in informe:
        print(f'{nombre:>10s} {cajones:>10d}',f"{f'${precio:.2f}':>10s}", f'{cambio:>10.2f}')

def informe_camion(nombre_archivo_camion, nombre_archivo_precios):
    '''Realiza el informe e imprime una tabla con los resultados
    a partir de dos archivos .csv correspondientes a la carga del camión
    y a los precios de venta'''
    informe = hacer_informe(nombre_archivo_camion, nombre_archivo_precios)
    imprimir_informe(informe)
    
informe_camion('Data/camion.csv', 'Data/precios.csv')

# Output
'''
    Nombre    Cajones     Precio     Cambio
---------- ---------- ---------- ----------
      Lima        100     $40.22       8.02
   Naranja         50    $106.28      15.18
     Caqui        150    $105.46       2.02
 Mandarina        200     $80.89      29.66
   Durazno         95     $73.48      33.11
 Mandarina         50     $80.89      15.79
   Naranja        100    $106.28      35.84
'''

