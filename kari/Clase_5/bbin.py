#bbin.py

def donde_insertar(lista, x, verbose = False):
    '''Búsqueda binaria
    Precondición: la lista está ordenada
    Devuelve la tupla (True, p) tal que lista[p] == x, si x está en lista
    si x no está en la lista, devuelve False y la posición donde
    podría insertar el elemento (False, p).
    '''
    
    if verbose:
        print(f'[DEBUG] izq |der |medio')        
    pos = -1 # Inicializo respuesta, el valor no fue encontrado
    izq = 0
    der = len(lista) - 1
    aparece = False     #Lleva control de si x aparece o no en la lista
    while izq <= der:
        medio = (izq + der) // 2
        if verbose:
            print(f'[DEBUG] {izq:3d} |{der:>3d} |{medio:3d}')
        if lista[medio] == x:
            pos = medio         # elemento encontrado!
        if lista[medio] > x:
            der = medio - 1     # descarto mitad derecha
        else:                   # if lista[medio] < x:
            izq = medio + 1     # descarto mitad izquierda
    if pos == -1:               # si no se encontró el elemento, busco la posición
        if lista[medio] < x:    
            pos = medio + 1
        if lista[medio] > x:
            pos = medio
    else:
        aparece = True          # x aparece en la lista
    return aparece, pos

#%%

def insertar(lista, x):
    '''Precondición: la lista está ordenada
    Si x está en la lista, devuelve su posición, de no ser así
    devuelve la lista con x insertada en ella y la posición en forma
    de tupla (lista, posición).
    '''
    aparece, pos = donde_insertar(lista, x) #Asigno los dos valores que devuelve la función a dos variables
    if aparece:
        return pos #Si x está en la lista devuelva la posición
    else:
        lista.insert(pos, x)    #Inserta el elemento en la posición
        return lista, pos   

