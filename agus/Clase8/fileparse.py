def parse_csv(filas, select=None, types=[], has_headers= True, silence_errors= False):
    '''
    Parsea un iterable proveniente de un archivo CSV en una lista de registros.
    Devuelve una lista de diccionarios si el archivo tiene encabezados
    De lo contrario devuelve una lista de tuplas
    '''
    if select and not has_headers:
        raise RuntimeError("Para seleccionar, necesito encabezados.")
   
    registros = []
    if has_headers:
        encabezados = next(filas) # Lee los encabezados del archivo

        # Si se indicó un selector de columnas,
        #    buscar los índices de las columnas especificadas.
        # Y achicar el conjunto de encabezados para diccionarios

        if select:
            indices = [encabezados.index(col_name) for col_name in select]
            encabezados = select
        else:
            indices = []

        for i, fila in enumerate(filas):
            if not fila:    # Saltear filas vacías
                continue
            # Filtrar la fila si se especificaron columnas
            if indices:
                fila = [fila[index] for index in indices]
            # Convertir los strings si se especificaron tipos de datos
            if types:
                try:
                    fila = [func(val) for func, val in zip(types, fila)]
                except ValueError as e:
                    if not silence_errors:
                        print(f"Fila {i}: No se puede convertir {fila}")
                        print(f"Motivo: {e}")
                    continue
            # Armar el diccionario
            registro = dict(zip(encabezados, fila))
            registros.append(registro)
    else: 
        for fila in filas:
            if not fila:    # Saltear filas vacías
                continue
            # Convertir los strings si se especificaron tipos de datos
            if types:
                fila = [func(val) for func, val in zip(types, fila) ]
            # Armar tupla
            registro = tuple(fila)
            registros.append(registro)

    return registros