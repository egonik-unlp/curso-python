# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 15:56:41 2020

@author: Agustin
"""

class Cola:
    '''Representa a una cola, con operaciones de encolar y desencolar.
    El primero en ser encolado es tambien el primero en ser desencolado.
    '''

    def __init__(self):
        '''Crea una cola vacia.'''
        self.items = []

    def encolar(self, x):
        '''Encola el elemento x.'''
        self.items.append(x)

    def desencolar(self):
        '''Elimina el primer elemento de la cola 
        y devuelve su valor. 
        Si la cola esta vacia, levanta ValueError.'''
        if self.esta_vacia():
            raise ValueError('La cola esta vacia')
        return self.items.pop(0)

    def esta_vacia(self):
        '''Devuelve 
        True si la cola esta vacia, 
        False si no.'''
        return len(self.items) == 0
    
    def __str__(self):
        return f"Cola: {', '.join(self.items)}"

class TorreDeControl(Cola):
    def __init__(self):
        self.arribos = Cola()
        self.partidas = Cola()
        
    def nuevo_arribo(self, nombre):
        '''
        Agrega un nuevo avion a la cola de arribos, esperando para aterrizar
        nombre: string
        '''
        self.arribos.encolar(nombre)
        
    def nueva_partida(self, nombre):
        '''
        Agrega un nuevo avion a la cola de partidas, esperando para despegar
        nombre: string
        '''
        self.partidas.encolar(nombre)
        
    def asignar_pista(self):
        '''
        Asigna una pista a un avión, priorizando los arribos
        Imprime un aviso con el nombre del vuelo que aterrizó o despegó
        '''
        if self.arribos.esta_vacia() and self.partidas.esta_vacia():
            print("No hay vuelos en espera.")
        elif self.arribos.esta_vacia():
            vuelo = self.partidas.desencolar()
            print(f"El vuelo {vuelo} despegó con éxito")
        else:
            vuelo = self.arribos.desencolar()
            print(f"El vuelo {vuelo} aterrizó con éxito")
    
    def ver_estado(self):
        '''
        Imprime el estado de las colas de arribos y partidas
        '''
        if not self.arribos.esta_vacia():
            print(f"Vuelos esperando para aterrizar: {', '.join(self.arribos.items)}")
        if not self.partidas.esta_vacia():
            print(f"Vuelos esperando para despegar: {', '. join(self.partidas.items)}")
        if self.arribos.esta_vacia() and self.partidas.esta_vacia():
            print ("No hay vuelos en espera")
            