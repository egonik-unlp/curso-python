# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 14:49:03 2020

@author: Agustin
"""
#%% Canguro A (clase escrita según las instrucciones del ejercicio)
class Canguro:
    def __init__(self):
        self.contenido_marsupio = []
        
    def meter_en_marsupio(self, obj):
        '''
        Agrega un nuevo objeto (de cualquier tipo) al marsupio

        '''
        self.contenido_marsupio.append(obj)
    
    def __str__(self):
        return f"Canguro que tiene en su marsupio: {self.contenido_marsupio}"


madre = Canguro()
cangurito = Canguro()
print("madre = " , madre)
print("cangurito = " , cangurito)
madre.meter_en_marsupio(cangurito)
print()
print("madre = " , madre)
print("cangurito =", cangurito)

#%% Canguro Malo (clase con error corregido)
"""Este código continene un 
bug importante y dificil de ver
"""

class Canguro:
    """Un Canguro es un marsupial."""
    
    def __init__(self, nombre, contenido=[]):
        """Inicializar los contenidos del marsupio.

        nombre: string
        contenido: contenido inicial del marsupio, lista.
        """
        self.nombre = nombre
        self.contenido_marsupio = contenido.copy()
        # El error era que al no pasar un contenido como parámetro,
        # se asignaba la lista vacía "contenido" al marsupio, y esta lista
        # vacía era la misma para todos los canguros, por lo tanto al modificar
        # el contenido_marsupio de un canguro, se modificaba el de todos.
        # La modificación que introduje es el metodo copy(). Al hacer una copia,
        # el contenido de cada marsupio es un objeto separado.

    def __str__(self):
        """devuelve una representación como cadena de este Canguro.
        """
        t = [ self.nombre + ' tiene en su marsupio:' ]
        for obj in self.contenido_marsupio:
            s = '    ' + object.__str__(obj)
            t.append(s)
        return '\n'.join(t)

    def meter_en_marsupio(self, item):
        """Agrega un nuevo item al marsupio.

        item: objecto a ser agregado
        """
        self.contenido_marsupio.append(item)

#%%
madre_canguro = Canguro('Madre')
cangurito = Canguro('gurito')
madre_canguro.meter_en_marsupio('billetera')
madre_canguro.meter_en_marsupio('llaves del auto')
madre_canguro.meter_en_marsupio(cangurito)

print(madre_canguro)