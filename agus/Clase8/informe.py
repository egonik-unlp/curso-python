#!/usr/bin/env python3
# informe_funciones.py

import fileparse
import csv
import sys
import lote
import formato_tabla

def leer_camion(file_name):
    with open(file_name) as f:
        filas = csv.reader(f)
        camion_dicts = fileparse.parse_csv(filas, types=[str, int, float])
        camion = [ lote.Lote(d['nombre'], d['cajones'], d['precio']) for d in camion_dicts]
        return camion
        
def leer_precios(file_name):
    with open(file_name) as f:
        filas = csv.reader(f)
        precios = fileparse.parse_csv(filas, types= [str, float], has_headers = False)
        return dict(precios)
        
def hacer_informe(camion, precios):
    informe = []
    for lote in camion:
        cambio = precios[lote.nombre] - lote.precio
        record = (lote.nombre, lote.cajones, precios[lote.nombre], cambio)
        informe.append(record)
    return informe

def imprimir_informe(data_informe, formateador):
    '''
    Imprime una tabla prolija desde una lista de tuplas
    con (nombre, cajones, precio, diferencia) 
    '''
    formateador.encabezado(['Nombre', 'Cantidad', 'Precio', 'Cambio'])
    for nombre, cajones, precio, cambio in data_informe:
        rowdata = [ nombre, str(cajones), f'{precio:0.2f}', f'{cambio:0.2f}' ]
        formateador.fila(rowdata)
        
def informe_camion(archivo_camion, archivo_precios, formato="txt"):
    '''
    Crea un informe por camion a partir de archivos camion y precio.
    El informe se imprime en formato txt por defecto, se puede pasar
    otro formato (csv o html) como tercer parámetro
    '''
    # Leer archivos con datos
    camion = leer_camion(archivo_camion)
    precios = leer_precios(archivo_precios)

    # Obtener los datos para un informe
    data_informe = hacer_informe(camion, precios)

    # Imprimir
    formateador = formato_tabla.crear_formateador(formato)
    imprimir_informe(data_informe, formateador)

def main(args):    
    if len(args) <3 :
        raise SystemExit(f'Uso adecuado: {args[0]} ' 'archivo_camion archivo_precios formato(opcional)')
    camion = args[1]
    precios = args[2]
    if len(args) == 4:
        formato = args[3]
    else:
        formato = "txt"
    informe_camion(camion, precios, formato)
    
if __name__ == "__main__":
    main(sys.argv)
