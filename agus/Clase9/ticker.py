# -*- coding: utf-8 -*-

from vigilante import vigilar
import csv
import informe
import formato_tabla

def parsear_datos(lines):
    rows = csv.reader(lines)
    
    rows = ([row[index] for index in [0,1,2]] for row in rows )
    
    #types = [str, float, float]
    #rows = ([func(val) for func, val in zip (types, row)] for row in rows)
    # Si cambio los tipos de dato, la función formateadora no los acepta
    
    headers = ["nombre", "precio", "volumen"]
    rows = (dict(zip(headers, row)) for row in rows)
    
    return rows

if __name__ == '__main__':
    camion = informe.leer_camion('Data/camion.csv')
    lines = vigilar('Data/mercadolog.csv')
    formateador = formato_tabla.crear_formateador("txt")
    rows = parsear_datos(lines)
    rows = (row for row in rows if row["nombre"] in camion)
    headers = ["Nombre", "Precio", "Volumen"]
    formateador.encabezado(headers)
    for row in rows:
        formateador.fila(row.values())
