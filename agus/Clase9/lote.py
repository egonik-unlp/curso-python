# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 15:27:27 2020

@author: Agustin
"""

class Lote:
    
    def __init__(self, nombre, cajones, precio):
        self.nombre = nombre
        self.cajones = cajones
        self.precio = precio
    
    def __str__(self):
        return f"nombre: {self.nombre}, precio: {self.precio}, cajones: {self.cajones}"
    
    def __repr__(self):
        return f"Lote('{self.nombre}', {self.cajones}, {self.precio})"    
    
    def costo(self):
        return self.cajones * self.precio
    
    def vender(self, n_cajones):
        if n_cajones <= self.cajones:
            self.cajones -= n_cajones
        else:
            raise ValueError("No se pueden vender más cajones de los que tiene el lote")
    