# Ejercicios de solución de errores:
#%%
# Ejercicio 3.1
# El error era de tipo semántico ubicado en el else 
# La función evaluaba la primera letra del string y en función de eso retornaba un valor.
# La solución que propongo es eliminar el else de adentro del while, 
# de modo que si la letra que se está evaluando no es "a", no se corte la ejecución del loop
def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False

print(tiene_a('UNSAM 2020'))
print(tiene_a('abracadabra'))
print(tiene_a('La novela 1984 de George Orwell'))

#%%
# Ejercicio 3.2
# El error es de sintaxis, la palabra reservada para el valor booleano falso es False, no "Falso",
# Además faltaban los ":" después de la definición de la función y después del while, y la evaluación
# del condicional estaba hecha con "=" en lugar de "=="
# La solución fue arreglar estos errores de sintaxis
def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False

print(tiene_a('UNSAM 2020'))
print(tiene_a('La novela 1984 de George Orwell'))

#%%
# Ejercicio 3.3
# El error es de tipo de dato. La función está planteada para recibir un string, por eso usa 
# el método "len" y una iteración. En caso de pasarle un número, no puede iterar.
# Mi solución fue utilizar str() para convertir el valor a tipo string
def tiene_uno(expresion):
    expresion = str(expresion)
    n = len(expresion)
    i = 0
    tiene = False
    while (i<n) and not tiene:
        if expresion[i] == '1':
            tiene = True
        i += 1
    return tiene


tiene_uno('UNSAM 2020')
tiene_uno('La novela 1984 de George Orwell')
tiene_uno(1984)

#%%
# Ejercicio 3.4
# El problema es que la función suma asigna el valor a una variable pero no devuelve nada
# Lo solucioné agregando la línea "return c"

def suma(a,b):
    c = a + b
    return c
    
a = 2
b = 3
c = suma(a,b)
print(f"La suma da {a} + {b} = {c}")

#%%
# Ejercicio 3.5

import csv
from pprint import pprint

def leer_camion(nombre_archivo):
    camion=[]
    registro={}
    with open(nombre_archivo,"rt") as f:
        filas = csv.reader(f)
        encabezado = next(filas)
        for fila in filas:
            registro[encabezado[0]] = fila[0]
            registro[encabezado[1]] = int(fila[1])
            registro[encabezado[2]] = float(fila[2])
            camion.append(registro)
    return camion

camion = leer_camion("Data/camion.csv")
pprint(camion)