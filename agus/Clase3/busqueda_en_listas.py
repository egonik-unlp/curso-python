def buscar_un_elemento (lista, elemento):
    for i, e in enumerate(lista):
        if e == elemento:
            return(i)
    return -1

def buscar_n_elemento (lista, elemento):
    n = 0
    for i, e in enumerate(lista):
        if e == elemento:
            n += 1
    return n

def maximo(lista):
    m = lista[0]
    for n in lista:
        if n > m:
            m = n
    return m

def minimo(lista):
    m = lista[0]
    for n in lista:
        if n < m:
            m = n
    return m