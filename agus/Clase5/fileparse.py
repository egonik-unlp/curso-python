import csv

def parse_csv(nombre_archivo, select=None, types=[], has_headers= True):
    '''
    Parsea un archivo CSV en una lista de registros
    '''
    with open(nombre_archivo) as f:
        filas = csv.reader(f)
        # Lee los encabezados del archivo
        registros = []
        if has_headers:
            encabezados = next(filas)
    
            # Si se indicó un selector de columnas,
            #    buscar los índices de las columnas especificadas.
            # Y achicar el conjunto de encabezados para diccionarios
    
            if select:
                indices = [encabezados.index(col_name) for col_name in select]
                encabezados = select
            else:
                indices = []
    
            for fila in filas:
                if not fila:    # Saltear filas vacías
                    continue
                # Filtrar la fila si se especificaron columnas
                if indices:
                    fila = [fila[index] for index in indices]
                # Convertir los strings si se especificaron tipos de datos
                if types:
                    fila = [func(val) for func, val in zip(types, fila) ]
                # Armar el diccionario
                registro = dict(zip(encabezados, fila))
                registros.append(registro)
        else: 
            for fila in filas:
                if not fila:    # Saltear filas vacías
                    continue
                # Convertir los strings si se especificaron tipos de datos
                if types:
                    fila = [func(val) for func, val in zip(types, fila) ]
                # Armar tupla
                registro = tuple(fila)
                registros.append(registro)

    return registros