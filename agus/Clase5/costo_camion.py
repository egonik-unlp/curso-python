import tabla_informe

def costo_camion(file_name):
    camion = tabla_informe.leer_camion(file_name)
    totalCost = sum([row["cajones"] * row["precio"] for row in camion])
    return totalCost