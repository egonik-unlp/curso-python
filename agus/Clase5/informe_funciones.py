import csv
import fileparse

def leer_camion(file_name):
    return fileparse.parse_csv(file_name, types=[str, int, float])

def leer_precios(file_name):
    precios = fileparse.parse_csv(file_name, types= [str, float], has_headers = False)
    return dict(precios)
        
def hacer_informe(camion, precios):
    informe = []
    for row in camion:
        cambio = precios[row["nombre"]] - row["precio"]
        record = (row["nombre"], row["cajones"], precios[row["nombre"]], cambio)
        informe.append(record)
    return informe

def imprimir_informe(informe):
    headers = ('Nombre', 'Cajones', 'Precio', 'Cambio')
    headerString = ""
    for header in headers:
        headerString += (f"{header:>10} ")
    print(headerString)
    print('---------- ---------- ---------- ----------')
    for nombre, cajones, precio, cambio in informe:
        print(f"{nombre:>10s}" f"{cajones:>10d}" f"{f'${precio:.2f}':>10s}" f"{cambio:>10.2f}")

precios = leer_precios("Data/precios.csv")
camion = leer_camion("Data/camion.csv")

informe = hacer_informe(camion, precios)
imprimir_informe(informe)