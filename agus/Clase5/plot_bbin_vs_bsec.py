import random
import matplotlib.pyplot as plt
import numpy as np

def busqueda_secuencial(lista,e):
    '''Si e está en la lista devuelve el índice de su primer aparición, 
    de lo contrario devuelve -1.
    También devuelve el numero de comparaciones que se hicieron
    '''
    comps = 0 #inicializo en cero la cantidad de comparaciones
    pos = -1
    for i,z in enumerate(lista):
        comps += 1 #sumo la comparación que estoy por hacer
        if z == e:
            pos = i
            break
    return pos, comps

def busqueda_binaria(lista, x, verbose = False):
    '''BÃºsqueda binaria
    PrecondiciÃ³n: la lista estÃ¡ ordenada
    Devuelve -1 si x no estÃ¡ en lista;
    Devuelve p tal que lista[p] == x, si x estÃ¡ en lista
    Tambien devuelve el numero de comparaciones que se hicieron
    '''
    if verbose:
        print(f'[DEBUG] izq |der |medio')
    comparaciones = 0
    pos = -1 # Inicializo respuesta, el valor no fue encontrado
    izq = 0
    der = len(lista) - 1
    while izq <= der:
        comparaciones += 1
        medio = (izq + der) // 2
        if verbose:
            print(f'[DEBUG] {izq:3d} |{der:>3d} |{medio:3d}')
        if lista[medio] == x:
            pos = medio     # elemento encontrado!
        if lista[medio] > x:
            der = medio - 1 # descarto mitad derecha
        else:               # if lista[medio] < x:
            izq = medio + 1 # descarto mitad izquierda
    return (pos, comparaciones)

def generar_lista(n,m):
    l = random.sample(range(m),k=n)
    l.sort()
    return l

def generar_elemento(m):
    return random.randint(0,m-1)

def experimento_secuencial_promedio(lista,m,k):
    comps_tot = 0
    for i in range(k):
        e = generar_elemento(m)
        comps_tot += busqueda_secuencial(lista,e)[1]

    comps_prom = comps_tot / k
    return comps_prom

def experimento_binario_promedio(lista,m,k):
    comps_tot = 0
    for i in range(k):
        e = generar_elemento(m)
        comps_tot += busqueda_binaria(lista,e)[1]
        
    comps_prom = comps_tot / k
    return comps_prom

m = 10000
k = 1000
largos = np.arange(256)+1 #estos son los largos de listas que voy a usar
comps_sec_promedio = np.zeros(256) #aca guardo el promedio de comparaciones sobre una lista de largo i, para i entre 1 y 256.
comps_bin_promedio = np.zeros(256)

for i, n in enumerate(largos):
    lista = generar_lista(n,m) # genero lista de largo n
    comps_sec_promedio[i] = experimento_secuencial_promedio(lista,m,k)
    comps_bin_promedio[i] = experimento_binario_promedio(lista,m,k)

#ahora grafico largos de listas contra operaciones promedio de búsqueda.
plt.plot(largos,comps_sec_promedio,label='Búsqueda Secuencial')
plt.plot(largos,comps_bin_promedio,label='Búsqueda Binaria')
plt.xlabel("Largo de la lista")
plt.ylabel("Cantidad de comparaiciones")
plt.title("Complejidad de la Búsqueda")
plt.legend()