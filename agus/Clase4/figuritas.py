import random
import numpy as np
import matplotlib.pyplot as plt

def crear_album(total):
    return np.zeros(total)

def album_incompleto(album):
    return (0 in album)

def comprar_figu(total):
    figu = random.randint(0, total-1)
    return figu

def comprar_paquete(total, fig_paquete):
    lista = []
    for i in range(fig_paquete):
        lista.append(random.randint(0, total-1))
    return np.array(lista)

def comprar_paquete_sin_reps(total, fig_paquete):
    lista = []
    for i in range(fig_paquete):
        lista.append(random.randint(0, total-1))
    return np.array(lista)

def cuantas_figus(total):
    album = crear_album(total)
    compradas = 0
    while album_incompleto(album):
        n = comprar_figu(total)
        album[n] += 1
        compradas += 1
    return compradas

def cuantos_paquetes(total, fig_paquete):
    album = crear_album(total)
    comprados = 0
    while album_incompleto(album):
        figs = comprar_paquete(total, fig_paquete)
        for fig in figs:
            album[fig] += 1
        comprados += 1
    return comprados

N = 1000
paquetes_hasta_llenar = np.array([cuantos_paquetes(670, 5) for i in range(N)])
compras_promedio = np.mean(paquetes_hasta_llenar)
print(f"Se necesita comprar, en promedio, {compras_promedio} paquetes para llenar el album")

limite_paquetes = 850
probabilidad = (paquetes_hasta_llenar <= limite_paquetes).sum()/N
print(f"La probabilidad de llenar el album comprando {limite_paquetes} o menos es {probabilidad}")

plt.hist(paquetes_hasta_llenar,bins=50)
