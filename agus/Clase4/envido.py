import random

valores = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12]
palos = ['oro', 'copa', 'espada', 'basto']
naipes = [(valor,palo) for valor in valores for palo in palos]

def generar_mano():
    return random.sample(naipes,k=3)

def envido(mano):
    palos_mano = [carta[1] for carta in mano] #hago una lista con los palos
    valores_mano = [carta[0] if carta[0] < 10 else 0 for carta in mano]
    mano_modificada = list(zip(valores_mano, palos_mano)) #10,11 y 12 valen 0
    flor = False
    palo_envido = ""
    
    for palo in palos_mano:
        reps = palos_mano.count(palo) # cuento cuantas veces se repite cada palo
        if reps == 2:
            palo_envido = palo # si se repite 2 veces hay envido de X palo
        elif reps == 3:
            flor = True # si se repite 3 veces hay flor

    if flor:
        valores_mano.remove(min(valores_mano)) 
        envido = 20 + sum(valores_mano) # si hay flor sumo las 2 cartas mas altas
    elif palo_envido:
        envido = 20 + sum([carta[0] for carta in mano_modificada if carta[1] == palo_envido])
        # si hay envido sumo las que sean del palo "palo_envido" es decir el que se repite 2 veces
    else:
        envido = max(valores_mano) #si no hay envido agarro la carta mas alta
    
    return envido
        
N = 100000
resultados_envido = [envido(generar_mano()) for i in range(N)]
envido_32_o_33 = [res for res in resultados_envido if res == 33]

probabilidad = len(envido_32_o_33)/N
print(f"La probabilidad de obtener 33 de envido es aproximadamente {probabilidad:.5f}")