import random

def tirar():
    tirada = []
    for i in range(5):
        tirada.append(random.randint(1,6)) 
    return tirada

def es_generala(tirada):
    primero = tirada[0]
    for dado in tirada:
        if dado != primero:
            return False
    return True

def guardar_y_tirar(tirada, guardado):
    resultado = []
    for dado in tirada:
        if dado == guardado:
            resultado.append(dado)
        else:
            resultado.append(random.randint(1,6))
    return resultado

def mas_repetido(tirada):
    return max(tirada,key=tirada.count)

def intentar_generala():
    tirada = tirar()
    if es_generala(tirada):
        return True
    else:
        repetido = mas_repetido(tirada)
        tirada2 = guardar_y_tirar(tirada, repetido)
        if es_generala(tirada2):
            return True
        else:
            repetido = mas_repetido(tirada2)
            tirada3 = guardar_y_tirar(tirada2, repetido)
            return es_generala(tirada3)
        
N = 100000 
# Lo probé con un millón y con 10 millones, y obviamente la precisión es mejor,
# pero tarda bastante y con 100000 basta para que se vea que funciona

G = sum([intentar_generala() for i in range(N)])
prob = G/N
print(f'Tiré {N} veces, de las cuales {G} saqué generala.')
print(f'Podemos estimar la probabilidad de sacar generala en tres tiradas mediante {prob:.6f}.')