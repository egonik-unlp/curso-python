import random
import numpy as np

def temperatura():
    return 37.5 + random.normalvariate(0,0.2)
N = 999
temperaturas = [temperatura() for i in range(N)]
for t in temperaturas:
    print(f"{t:.5f}")

print(f"El máximo valor obtenido fue: {max(temperaturas):.5f}")
print(f"El mínimo valor obtenido fue: {min(temperaturas):.5f}")
print(f"El valor promedio obtenido fue: {sum(temperaturas)/N:.5f}")
temperaturas.sort()
print(f"La mediana de las medidas fue: {temperaturas[round(len(temperaturas)/2)]:.5f}")

temp_array = np.array(temperaturas)
np.save("Data/Temperaturas", temp_array)