import csv
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np

dataFile = "Data/arbolado-en-espacios-verdes.csv"

def leer_parque (fileName, parque):
    with open(fileName, encoding="utf-8") as f:
        rows = csv.reader(f)
        headers = next(rows)
        list = []
        for arbol in rows:
            record = dict(zip(headers, arbol))
            if record["espacio_ve"] == parque:
                list.append(record)
        return list
    
def leer_arboles (fileName):
    with open(fileName, encoding="utf-8") as f:
        rows = csv.reader(f)
        headers = next(rows)
        list = []
        for arbol in rows:
            record = dict(zip(headers, arbol))
            list.append(record)
        return list
    
def especies(lista_arboles):
    speciesList = []
    for arbol in lista_arboles:
        speciesList.append(arbol["nombre_com"])
    speciesSet = set(speciesList)
    return speciesSet

def contar_ejemplares(lista_arboles):
    ejemplares = Counter()
    for arbol in lista_arboles:
        ejemplares[arbol["nombre_com"]] += 1
    return ejemplares

def medidas_de_especies(especies, arboleda):
    result = {especie: [(float(arbol["altura_tot"]), float(arbol["diametro"])) for arbol in arboleda if arbol["nombre_com"] == especie] for especie in especies}
    return result

def plotear_alturas(especie, arboles):
    altos = [float(arbol["altura_tot"]) for arbol in arboleda if arbol["nombre_com"] == especie]
    plt.hist(altos,bins=50)

def plotear_medidas(especie, arboleda):
    altos = [float(arbol["altura_tot"]) for arbol in arboleda if arbol["nombre_com"] == especie]
    diametros = [float(arbol["diametro"]) for arbol in arboleda if arbol["nombre_com"] == especie]
    plt.scatter(altos, diametros, s=15, alpha=0.5)    

arboleda = leer_arboles(dataFile)

plotear_medidas("Jacarandá", arboleda)