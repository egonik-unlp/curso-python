# -*- coding: utf-8 -*-

def hojas_ISO(n):
    '''
    Recibe un número entero positivo
    Devuelve un par con las dimensiones de una hoja de tamaño "A(n)" según normas ISO
    '''
    if n==0:
        return(1189, 841)
    else:
        medidas = hojas_ISO(n-1)
        if medidas[0] > medidas[1]:
            return (medidas[0] // 2, medidas[1])
        else:
            return (medidas[0], medidas[1] // 2)
