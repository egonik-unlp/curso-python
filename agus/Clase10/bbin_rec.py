# -*- coding: utf-8 -*-

def bbinaria_rec(lista, e):
    '''
    Recibe una lista ordenada de números y un número
    Devuelve True si el número está dentro de la lista, o false si no está
    '''
    if len(lista) == 0:
        res = False
    elif len(lista) == 1:
        res = lista[0] == e
    else:
        medio = len(lista)//2
        if lista[medio] == e:
            res = True
        elif lista[medio] < e:
            res = bbinaria_rec(lista[medio:], e)
        else:
            res = bbinaria_rec(lista[:medio], e)

    return res

