# -*- coding: utf-8 -*-


def pascal(n,k):
    '''
    Recibe dos enteros positivos n y k
    Devuelve el valor del triangulo de pascal en la fila n columna k
    Devuelve 0 si se recibe una combinación inválida como 0,3
    '''
    if k>n:
        return 0
    elif n==0 or k==0:
        return 1
    else:
        return pascal(n-1,k-1) + pascal(n-1,k)
    
    
    
