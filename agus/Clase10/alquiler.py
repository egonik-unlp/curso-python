# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

superficie = np.array([150.0, 120.0, 170.0, 80.0])
alquiler = np.array([35.0, 29.6, 37.4, 21.0])

def ajuste_lineal_simple(x,y):
    a = sum(((x - x.mean())*(y-y.mean()))) / sum(((x-x.mean())**2))
    b = y.mean() - a*x.mean()
    return a, b

a, b = ajuste_lineal_simple(superficie, alquiler)

grilla_x = np.linspace(start = min(superficie), stop = max(superficie), num = 1000)
grilla_y = grilla_x*a + b

g = plt.scatter(x = superficie, y = alquiler)
plt.title('Ajuste lineal alquiler vs superficie')
plt.plot(grilla_x, grilla_y, c = 'green')
plt.xlabel('superficie')
plt.ylabel('alquiler')

plt.show()

print(f"Ecuación de la recta: {a:.5f}x +({b:.5f})")
errores = alquiler - (a*superficie + b)
print("Errores = ", errores)
print("ECM:", (errores**2).mean())