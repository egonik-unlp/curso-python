import csv

def leer_camion(fileName):
    with open(fileName) as f:
        rows = csv.reader(f)
        headers = next(rows)
        list = []
        for nRow, row in enumerate(rows, start= 1):
            record = dict(zip(headers, row))
            list.append(record)
        return list


def costo_camion(fileName):
    with open(fileName) as f:
        rows = csv.reader(f)
        headers = next(rows)
        totalCost = 0
        for nRow, row in enumerate(rows, start= 1):
            record = dict(zip(headers, row))
            try:
                ncajones = int(record['cajones'])
                precio = float(record['precio'])
                totalCost += ncajones * precio
            except ValueError:
                print(f'Fila {nRow}: No pude interpretar: {row}')
    return totalCost

def leer_precios(fileName):
    with open(fileName) as f:
        precios = {}
        rows = csv.reader(f)
        for nRow, row in enumerate(rows, start= 1):
            try:
                precios[row[0]] = row[1]
            except:
                print(f"Fila {nRow}: No pude interpretar: {row}")
        return precios

def total_venta(camion, precios):
    total = 0
    for row in camion:
        precioVenta = float(precios[row["nombre"]])
        total += int(row["cajones"]) * precioVenta
    return total
        

costo = costo_camion("Data/camion.csv")
precios = leer_precios("Data/precios.csv")
camion = leer_camion("Data/camion.csv")
totalVenta = total_venta(camion, precios)

print(f"El costo del camión fue: {costo} \nLa recaudación de la venta fue: {totalVenta} \nEl balance fue: {totalVenta - costo}")


