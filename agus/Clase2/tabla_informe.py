import csv

def leer_camion(fileName):
    with open(fileName) as f:
        rows = csv.reader(f)
        headers = next(rows)
        list = []
        for nRow, row in enumerate(rows, start= 1):
            record = dict(zip(headers, row))
            list.append(record)
        return list


def costo_camion(fileName):
    with open(fileName) as f:
        rows = csv.reader(f)
        headers = next(rows)
        totalCost = 0
        for nRow, row in enumerate(rows, start= 1):
            record = dict(zip(headers, row))
            try:
                ncajones = int(record['cajones'])
                precio = float(record['precio'])
                totalCost += ncajones * precio
            except ValueError:
                print(f'Fila {nRow}: No pude interpretar: {row}')
    return totalCost

def leer_precios(fileName):
    with open(fileName) as f:
        precios = {}
        rows = csv.reader(f)
        for nRow, row in enumerate(rows, start= 1):
            try:
                precios[row[0]] = row[1]
            except:
                print(f"Fila {nRow}: No pude interpretar: {row}")
        return precios

def total_venta(camion, precios):
    total = 0
    for row in camion:
        precioVenta = float(precios[row["nombre"]])
        total += int(row["cajones"]) * precioVenta
    return total
        
def hacer_informe(camion, precios):
    informe = []
    for row in camion:
        cambio = float(precios[row["nombre"]]) - float(row["precio"])
        record = (row["nombre"], int(row["cajones"]), float(precios[row["nombre"]]), cambio)
        informe.append(record)
    return informe
        


costo = costo_camion("Data/camion.csv")
precios = leer_precios("Data/precios.csv")
camion = leer_camion("Data/camion.csv")
totalVenta = total_venta(camion, precios)

informe = hacer_informe(camion, precios)
headers = ('Nombre', 'Cajones', 'Precio', 'Cambio')

headerString = ""
for header in headers:
    headerString += (f"{header:>10} ")
print(headerString)
print('---------- ---------- ---------- ----------')
for nombre, cajones, precio, cambio in informe:
    print(f"{nombre:>10s}" f"{cajones:>10d}" f"{f'${precio:.2f}':>10s}" f"{cambio:>10.2f}")
