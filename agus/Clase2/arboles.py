import csv
from collections import Counter

dataFile = "Data/arbolado-en-espacios-verdes.csv"

def leer_parque (fileName, parque):
    with open(fileName, encoding="utf-8") as f:
        rows = csv.reader(f)
        headers = next(rows)
        list = []
        for arbol in rows:
            record = dict(zip(headers, arbol))
            if record["espacio_ve"] == parque:
                list.append(record)
        return list
    
def especies(lista_arboles):
    speciesList = []
    for arbol in lista_arboles:
        speciesList.append(arbol["nombre_com"])
    speciesSet = set(speciesList)
    return speciesSet

def contar_ejemplares(lista_arboles):
    ejemplares = Counter()
    for arbol in lista_arboles:
        ejemplares[arbol["nombre_com"]] += 1
    return ejemplares
    
    
parque1 = leer_parque(dataFile, "GENERAL PAZ")
parque2 = leer_parque(dataFile, "ANDES, LOS")
parque3 = leer_parque(dataFile, "CENTENARIO")

print(f"General Paz: \n{contar_ejemplares(parque1).most_common(5)}")
print(f"Los Andes: \n{contar_ejemplares(parque2).most_common(5)}")
print(f"Centenario: \n{contar_ejemplares(parque3).most_common(5)}")

