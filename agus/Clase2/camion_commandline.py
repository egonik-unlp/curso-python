import csv
import sys
if len(sys.argv) >= 2:
    fileName = sys.argv[1]
else:
    fileName = "Data/camion.csv"
def totalCost(fileName):
    with open(fileName) as f:
        next(f)
        rows = csv.reader(f)
        total = 0
        for nRow, row in enumerate(rows, start=1):
            try:
                boxes = int(row[1])
            except ValueError:
                boxes = 0
                print(f'Fila {nRow}: No pude interpretar: {row}')
                print("uwu")
            try:
                price = float(row[2])
            except ValueError:
                price = 0
                print(f'Fila {nRow}: No pude interpretar: {row}')
            total += price*boxes
    return total

print(f"El precio total es: {totalCost(fileName)}")