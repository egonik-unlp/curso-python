header = ""
for i in range(10):
    header += f"{i:3d} "

print(header)

for i in range(10):
    row = f"{i:3d}: "
    for j in range(10):
        row += f"{(i*j):3d }"
    print(row)