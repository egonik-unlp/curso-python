# -*- coding: utf-8 -*-

import datetime;
str_nacimiento = input("Ingres� tu fecha de nacimiento en formato dd/mm/AAAA: ")
nacimiento = datetime.datetime.strptime(str_nacimiento, "%d/%m/%Y")
ahora = datetime.datetime.now();

vida = ahora - nacimiento
vida_segundos = vida.total_seconds()

print(f"Tu tiempo total de vida es: {vida_segundos:.2f} segundos, o {vida_segundos/(24*60*60):.2f} d��as.")
