# -*- coding: utf-8 -*-
import os
import sys

def list_imgs(directorio):
    for root, dirs, files in os.walk(directorio):
        for file in files:
            _, extension = os.path.splitext(file) 
            # El guión bajo guarda el nombre del archivo sin la extensión, 
            # es necesario porque splitext() devuelve dos valores pero yo solo
            # quiero usar la extensión
            if extension == ".png":
                print("PNG!", os.path.join(root, file))
                
if __name__ == "__main__":
    print ("soy main xd")
    directorio = sys.argv[1]
    list_imgs(directorio)