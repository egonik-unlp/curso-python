# -*- coding: utf-8 -*-
import pandas as pd
import os

#Generar los datasets desde los archivos
directorio = 'Data'
archivo_veredas = 'arbolado-publico-lineal-2017-2018.csv'
archivo_parques = 'arbolado-en-espacios-verdes.csv'
df_veredas = pd.read_csv(os.path.join(directorio, archivo_veredas))
df_parques = pd.read_csv(os.path.join(directorio, archivo_parques))

#Tomar solamente los arboles de la especie Tipuana tipu y las columnas altura y diametro
selected_cols_veredas = ["altura_arbol", "diametro_altura_pecho"]
selected_cols_parques = ["altura_tot", "diametro"]

df_tipas_parques = df_parques[df_parques["nombre_cie"] == "Tipuana Tipu"][selected_cols_parques].copy()
df_tipas_veredas = df_veredas[df_veredas["nombre_cientifico"] == "Tipuana tipu"][selected_cols_veredas].copy()

#Renombrar columnas para compatibilizar
df_tipas_parques = df_tipas_parques.rename(columns={"altura_tot": "altura"})
df_tipas_veredas = df_tipas_veredas.rename(columns={"diametro_altura_pecho": "diametro", "altura_arbol": "altura"})

#Agregar columna ambiente
df_tipas_parques["ambiente"] = "parque"
df_tipas_veredas["ambiente"] = "vereda"

#Juntar en un mismo dataset
df_tipas = pd.concat([df_tipas_veredas, df_tipas_parques])

df_tipas.boxplot('diametro',by = 'ambiente')
df_tipas.boxplot('altura',by = 'ambiente')