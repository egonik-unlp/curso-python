# -*- coding: utf-8 -*-

def ord_burbujeo(lista):
    long = len(lista)
    n = long
    while n > 0:
        for i in range(1,n):
            if lista[i-1] > lista[i]:
                lista[i-1], lista[i] = lista [i], lista[i-1]
        n -= 1
        
'''
La complejidad del algoritmo es O(n^2), ya que el número de comparaciones
que hace es igual a la longitud de la lista, luego la longitud de
la lista menos el último elemento, luego la longitud menos los
dos últimos, y así siguiendo. La sumatoria de n + n-1 + n-2 ... hasta 0 
es igual a n^2/2
'''