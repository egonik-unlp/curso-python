# -*- coding: utf-8 -*-

import numpy as np
import timeit as tt
import matplotlib.pyplot as plt

def burbujeo(lista):
    """Ordena una lista de elementos comparables según el método de burbujeo.
        Devuelve el número de comparaciones realizadas.
    """
    long = len(lista)
    n = long
    while n > 0:
        for i in range(1,n):
            if lista[i-1] > lista[i]:
                lista[i-1], lista[i] = lista [i], lista[i-1]
        n -= 1

def secuencial(lista):

    """Ordena una lista de elementos según el método de selección.
        Devuelve el número de comparaciones realizadas
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""
    
    n = len(lista) - 1
    while n > 0:
        p = buscar_max(lista, 0, n)
        lista[p], lista[n] = lista[n], lista[p]
        n = n - 1

def buscar_max(lista, a, b):
    """Devuelve la posición del máximo elemento en un segmento de
       lista de elementos comparables, y el número de comparaciones realizadas
       La lista no debe ser vacía.
       a y b son las posiciones inicial y final del segmento"""
    pos_max = a
    
    for i in range(a + 1, b + 1):
        if lista[i] > lista[pos_max]:
            pos_max = i
    return pos_max

def insercion(lista):
    """Ordena una lista de elementos según el método de inserción.
       Devuelve el número de comparaciones realizadas.
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""
    for i in range(len(lista) - 1):
        if lista[i + 1] < lista[i]:
            reubicar(lista, i + 1)

def reubicar(lista, p):
    """Reubica al elemento que está en la posición p de la lista
       dentro del segmento [0:p-1].
       Devuelve el número de comparaciones realizadas.
       Pre: p tiene que ser una posicion válida de lista."""
    v = lista[p]
    j = p
    while j > 0 and v < lista[j - 1]:
        lista[j] = lista[j - 1]
        j -= 1
    lista[j] = v

def merge_sort(lista):
    """Ordena lista mediante el método merge sort.
       Pre: lista debe contener elementos comparables.
       Devuelve: una nueva lista ordenada."""
    
    if len(lista) < 2:
        lista_nueva = lista
    else:
        medio = len(lista) // 2
        izq = merge_sort(lista[:medio])
        der = merge_sort(lista[medio:])
        lista_nueva = merge(izq, der)
    return lista_nueva

def merge(lista1, lista2):
    """Intercala los elementos de lista1 y lista2 de forma ordenada.
       Pre: lista1 y lista2 deben estar ordenadas.
       Devuelve: una lista con los elementos de lista1 y lista2."""
    i, j = 0, 0
    resultado = []

    while(i < len(lista1) and j < len(lista2)):
        if (lista1[i] < lista2[j]):
            resultado.append(lista1[i])
            i += 1
        else:
            resultado.append(lista2[j])
            j += 1

    resultado += lista1[i:]
    resultado += lista2[j:]

    return resultado

def generar_listas(N):
    """
    Devuelve una lista de listas de longitud 1 hasta N
    """

    return [[np.random.randint(0,1001) for i in range(k)] for k in range(1,N+1)]

def comparacion_grafica(N):
    
    listas = generar_listas(N)
    
    seq_vector = np.array([tt.timeit(f'secuencial({lista.copy()})', globals = globals()) for lista in listas])
    ins_vector = np.array([tt.timeit(f'insercion({lista.copy()})', globals = globals()) for lista in listas])
    bub_vector = np.array([tt.timeit(f'burbujeo({lista.copy()})', globals = globals()) for lista in listas])
    mer_vector = np.array([tt.timeit(f'merge({lista.copy()})', globals = globals()) for lista in listas])
   
    plt.figure()
    plt.plot(seq_vector, "b--", label="secuencial")
    plt.plot(ins_vector, label="insercion")
    plt.plot(bub_vector, label="burbujeo")
    plt.plot(mer_vector, label="merge")
    plt.legend()

