# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

def comp_burbujeo(lista):
    """Ordena una lista de elementos comparables según el método de burbujeo.
        Devuelve el número de comparaciones realizadas.
    """
    comp = 0
    long = len(lista)
    n = long
    while n > 0:
        for i in range(1,n):
            comp += 1
            if lista[i-1] > lista[i]:
                lista[i-1], lista[i] = lista [i], lista[i-1]
        n -= 1
    return comp

def comp_secuencial(lista):

    """Ordena una lista de elementos según el método de selección.
        Devuelve el número de comparaciones realizadas
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""
    
    n = len(lista) - 1
    comp_totales = 0
    while n > 0:
        p = buscar_max(lista, 0, n)
        lista[p], lista[n] = lista[n], lista[p]
        comp_totales += n
        n = n - 1
    return comp_totales

def buscar_max(lista, a, b):
    """Devuelve la posición del máximo elemento en un segmento de
       lista de elementos comparables, y el número de comparaciones realizadas
       La lista no debe ser vacía.
       a y b son las posiciones inicial y final del segmento"""
    pos_max = a
    
    for i in range(a + 1, b + 1):
        if lista[i] > lista[pos_max]:
            pos_max = i
    return pos_max

def comp_insercion(lista):
    """Ordena una lista de elementos según el método de inserción.
       Devuelve el número de comparaciones realizadas.
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""
    comp_totales = 0
    for i in range(len(lista) - 1):
        comp_totales += 1
        if lista[i + 1] < lista[i]:
            comps = reubicar(lista, i + 1)
            comp_totales += comps
    return comp_totales

def reubicar(lista, p):
    """Reubica al elemento que está en la posición p de la lista
       dentro del segmento [0:p-1].
       Devuelve el número de comparaciones realizadas.
       Pre: p tiene que ser una posicion válida de lista."""
    v = lista[p]
    j = p
    comps = 0
    while j > 0 and v < lista[j - 1]:
        comps += 1
        lista[j] = lista[j - 1]
        j -= 1
    lista[j] = v
    return comps

def merge_sort(lista, comps_totales= 0):
    """Ordena lista mediante el método merge sort.
       Pre: lista debe contener elementos comparables.
       Devuelve: una nueva lista ordenada."""
    
    if len(lista) < 2:
        lista_nueva = lista
    else:
        medio = len(lista) // 2
        izq, comps_i = merge_sort(lista[:medio], comps_totales)
        der, comps_d = merge_sort(lista[medio:], comps_totales)
        lista_nueva, comps_m = merge(izq, der)
        comps_totales += comps_m
    return lista_nueva, comps_totales

def merge(lista1, lista2):
    """Intercala los elementos de lista1 y lista2 de forma ordenada.
       Pre: lista1 y lista2 deben estar ordenadas.
       Devuelve: una lista con los elementos de lista1 y lista2."""
    i, j = 0, 0
    comps = 0
    resultado = []

    while(i < len(lista1) and j < len(lista2)):
        comps = comps+1
        if (lista1[i] < lista2[j]):
            resultado.append(lista1[i])
            i += 1
        else:
            resultado.append(lista2[j])
            j += 1

    resultado += lista1[i:]
    resultado += lista2[j:]

    return resultado, comps

def generar_lista(N):
    return [np.random.randint(0,1001) for i in range(N)]

    
def comparacion_grafica():
    
    secuencial = []
    insercion = []
    burbujeo = []
    merge = []
    
    for n in range(1,256):
        lista = generar_lista(n)
        secuencial.append(comp_secuencial(lista.copy()))
        insercion.append(comp_insercion(lista.copy()))
        burbujeo.append(comp_burbujeo(lista.copy()))
        ordenada, merge_comps = merge_sort(lista.copy())
        merge.append(merge_comps)
    seq_vector = np.array(secuencial)
    ins_vector = np.array(insercion)
    bub_vector = np.array(burbujeo)
    mer_vector = np.array(merge)
    plt.figure()
    plt.plot(seq_vector, "b--", label="secuencial")
    plt.plot(ins_vector, label="insercion")
    plt.plot(bub_vector, label="burbujeo")
    plt.plot(mer_vector, label="merge")
    plt.legend()
    
        