import numpy as np
import matplotlib.pyplot as plt

def randomwalk(largo):
    pasos=np.random.randint(-1,2,largo)    
    return pasos.cumsum()

def many_walks(n_walks, largo):
    walks = []
    for i in range(n_walks):
        walks.append(randomwalk(largo))
    return np.array(walks)

walks = many_walks(20, 100000)

puntos_mas_alejados = [max(walk, key=abs) for walk in walks]
fila_mas_alejada = walks[puntos_mas_alejados.index(max(puntos_mas_alejados, key=abs))]
fila_menos_alejada = walks[puntos_mas_alejados.index(min(puntos_mas_alejados, key=abs))]

plt.figure()

plt.subplot(2,1,1)
plt.title("Caminatas aleatorias")
for walk in walks:
    plt.plot(walk)
plt.yticks([-500,0,500])
plt.xticks([])
plt.ylim(-1000,1000)
    
plt.subplot(2,2,3)
plt.title("Caminata más alejada")
plt.plot(fila_mas_alejada)
plt.yticks([-500,0,500])
plt.xticks([])
plt.ylim(-1000,1000)

plt.subplot(2,2,4)
plt.title("Caminata menos alejada")
plt.plot(fila_menos_alejada)
plt.yticks([])
plt.xticks([])
plt.ylim(-1000,1000)

plt.show()
