def valor_absoluto(n):
   
    '''
    Recibe un número y devuelve su valor absoluto

    '''
    if n >= 0:
        return n
    else:
        return -n
    
def suma_pares(l):
    '''
    Recibe una lista de números
    Devuelve la suma de todos los enteros pares de esa lista
    Invariante de ciclo: res es igual a la suma de los pares del segmento analizado de la lista
    '''
    res = 0
    for e in l:
        if e % 2 ==0:
            res += e
        else:
            res += 0

    return res


def veces(a, b):
    '''
    Recibe dos números y devuelve su producto.
    Precondición: b es entero y mayor que 0
    '''
    res = 0
    nb = b
    while nb != 0:
        #print(nb * a + res)
        res += a
        nb -= 1
    return res


def collatz(n):
    '''
    Recibe un número entero y aplica las operaciones de la conjetura de Collatz:
    Si el número es par, se divide por 2, si es impar se multiplica por 3 y se suma 1
    Se aplican estas operaciones secuencialmente hasta que el resultado sea 1.
    Devuelve el número de iteraciones necesarias para llegar a 1.
    Invariante del ciclo: n != 1
    '''
    res = 1

    while n!=1:
        if n % 2 == 0:
            n = n//2
        else:
            n = 3 * n + 1
        res += 1

    return res