#!/usr/bin/env python3
# informe_funciones.py

import fileparse
import csv
import sys
import lote

def leer_camion(file_name):
    with open(file_name) as f:
        filas = csv.reader(f)
        camion_dicts = fileparse.parse_csv(filas, types=[str, int, float])
        

def leer_precios(file_name):
    with open(file_name) as f:
        filas = csv.reader(f)
        precios = fileparse.parse_csv(filas, types= [str, float], has_headers = False)
        return dict(precios)
        
def hacer_informe(camion, precios):
    informe = []
    for row in camion:
        cambio = precios[row["nombre"]] - row["precio"]
        record = (row["nombre"], row["cajones"], precios[row["nombre"]], cambio)
        informe.append(record)
    return informe

def imprimir_informe(informe):
    headers = ('Nombre', 'Cajones', 'Precio', 'Cambio')
    headerString = ""
    for header in headers:
        headerString += (f"{header:>10} ")
    print(headerString)
    print('---------- ---------- ---------- ----------')
    for nombre, cajones, precio, cambio in informe:
        print(f"{nombre:>10s}" f"{cajones:>10d}" f"{f'${precio:.2f}':>10s}" f"{cambio:>10.2f}")
    
def main(args):    
    if len(args) != 3:
        raise SystemExit(f'Uso adecuado: {args[0]} ' 'archivo_camion archivo_precios')
    camion = args[1]
    precios = args[2]
    informe = hacer_informe(leer_camion(camion), leer_precios(precios))
    imprimir_informe(informe)
    
if __name__ == "__main__":
    main(sys.argv)