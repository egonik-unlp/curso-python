# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 18:59:41 2020

@author: 
"""

import informe_funciones as informe

def costo_camion (nombre_archivo, select = None, types=[str, int, float], has_headers=True):
    camion = informe.leer_camion(nombre_archivo)
    costo_total = sum([fila["cajones"]*fila["precio"] for fila in camion])  
    return costo_total

a= costo_camion("Data/camion.csv", select = ["nombre","cajones","precio"], types=[str, int, float], has_headers=True)
print(a)