# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 00:19:08 2020

@author: DELL
"""

def donde_insertar(lista, x, verbose = False):
    '''Búsqueda binaria
    Precondición: la lista está ordenada
    Devuelve la posocion que deberia ser insertado x,
    si x no está en lista;
    Devuelve p tal que lista[p] == x, si x está en lista
    '''
    if verbose:
        print(f'[DEBUG] izq |der |medio')
    pos = 0 # Inicializo respuesta, el valor no fue encontrado
    izq = 0
    der = len(lista) - 1
    if x in lista: 
        while izq <= der:
                medio = (izq + der) // 2
                if verbose:
                    print(f'[DEBUG] {izq:3d} |{der:>3d} |{medio:3d}')
                if lista[medio] == x:
                    pos = medio    # elemento encontrado!
                if lista[medio] > x:
                    der = medio - 1 # descarto mitad derecha
                else:               # if lista[medio] < x:
                    izq = medio + 1 # descarto mitad izquierda
    
        return pos    
    
    else:
         while izq <= der:
             medio = (izq + der) // 2
             if verbose:
                print(f'[DEBUG] {izq:3d} |{der:>3d} |{medio:3d}')
             if lista[medio] > x:
                der = medio - 1 # descarto mitad derecha
             else:               # if lista[medio] < x:
                izq = medio + 1 # descarto mitad izquierda   
         else:
            pos = izq
         return lista.insert(pos,x), pos

     
        
        