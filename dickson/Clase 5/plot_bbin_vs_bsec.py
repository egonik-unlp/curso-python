# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 03:28:24 2020

@author: DELL
"""

import random

def generar_lista(n, m):
    l = random.sample(range(m), k = n)
    l.sort()
    return l

def generar_elemento(m):
    return random.randint(0, m-1)

def busqueda_secuencial(lista, x):
    '''Si x está en la lista devuelve el índice de su primer aparición, 
    de lo contrario devuelve -1.
    '''
    comps = 0 # inicializo en cero la cantidad de comparaciones
    pos = -1
    for i,z in enumerate(lista):
        comps += 1 # sumo la comparación que estoy por hacer
        if z == x:
            pos = i
            break
    return pos, comps

def busqueda_binaria(lista, x, verbose = False):
    '''Búsqueda binaria
    Precondición: la lista está ordenada
    Devuelve -1,
    si x no está en lista;
    Devuelve p tal que lista[p] == x, si x está en lista
    '''
    if verbose:
        print(f'[DEBUG] izq |der |medio')
    pos = 0 # Inicializo respuesta, el valor no fue encontrado
    izq = 0
    comps=0
    der = len(lista) - 1
    if x in lista: 
        while izq <= der:
            medio = (izq + der) // 2
            if verbose:
              print(f'[DEBUG] {izq:3d} |{der:>3d} |{medio:3d}')
            if lista[medio] == x:
              pos = medio    # elemento encontrado!
            if lista[medio] > x:
              der = medio - 1 # descarto mitad derecha
            else:               # if lista[medio] < x:
              izq = medio + 1 # descarto mitad izquierda
        comps+=1            
        return pos, comps

m = 10000
n = 100
lista = generar_lista(n, m)

# acá comienza el experimento
x = generar_elemento(m)

comps = busqueda_secuencial(lista, x)[1]
comps = busqueda_binaria(lista, x)[1]    

def experimento_secuencial_promedio(lista, m, k):
    comps_tot = 0
    for i in range(k):
        x = generar_elemento(m)
        comps_tot += busqueda_secuencial(lista,x)[1]

    comps_prom = comps_tot / k
    return comps_prom

def experimento_binaria_promedio(lista, m, k):
    comps_tot = 0
    for i in range(k):
        x = generar_elemento(m)
        comps_tot += busqueda_binaria(lista,x)[1]

    comps_prom = comps_tot / k
    return comps_prom


import matplotlib.pyplot as plt
import numpy as np


k = 1000

largos = np.arange(256) + 1 # estos son los largos de listas que voy a usar
comps_promedio = np.zeros(256) # aca guardo el promedio de comparaciones sobre una lista de largo i, para i entre 1 y 256.

for i, n in enumerate(largos):
    lista1 = generar_lista(n, m) # genero lista de largo n
    comps_promedio[i] = experimento_secuencial_promedio(lista, m, k)
    
for i, n in enumerate(largos):
    lista2 = generar_lista(n, m) # genero lista de largo n
    comps_promedio[i] = experimento_binaria_promedio(lista, m, k)

# ahora grafico largos de listas contra operaciones promedio de búsqueda.
plt.plot(largos,comps_promedio,label = 'Búsqueda Secuencial')

plt.plot(largos,comps_promedio,label = 'Búsqueda Binaria')
plt.xlabel("Largo de la lista")
plt.ylabel("Cantidad de comparaiciones")
plt.title("Complejidad de la Búsqueda")
plt.legend()