# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 03:06:10 2020

@author: EMA
"""


import fileparse
import csv
#def costo_camion(nombre_archivo):
#    costo_total=0
#    with open(nombre_archivo, "rt") as f:
#        filas = csv.reader(f)
#        encabezados = next(filas)
#            
#        for n_fila, fila in enumerate(filas, start=1):
#            record = dict(zip(encabezados, fila))
#            try:
#                ncajones = int(record['cajones'])
#                precio = float(record['precio'])
#                costo_total += ncajones * precio
#         
#            except ValueError:
#                print(f'Fila {n_fila}: No pude interpretar: {fila}')
#
#        return costo_total
def leer_camion(nombre_archivo):
    return fileparse.parse_csv2("Data/camion.csv", select=['nombre','cajones','precio'], types=[str,int,float], has_headers=True)

def leer_precios(nombre_archivo):
    
    return fileparse.parse_csv2("Data/precios.csv", types=[str,float], has_headers=False)


def total_venta(camion, precios):
    total = 0
    for fila in camion:
        precioVenta = fila["precio"]
        total += fila["cajones"]*precioVenta
    return total

def informe(camion, precios):
    informe = []
    for i, fila, in enumerate(camion):
        cambio = precios[i][1] - fila["precio"]
        record = (fila["nombre"], fila["cajones"], fila["precio"], cambio)
        informe.append(record)
    return informe


#costo = costo_camion("Data/camion.csv")
precios = leer_precios("Data/camion.csv")
camion = leer_camion("Data/camion.csv")
totalVenta = total_venta(camion, precios)


#5.1
def imprimir_informe(informe):
    titulos = ("Nombre", "Cajones", "Precio", "Cambio")
    informe = informe(camion, precios)
    print('%10s %10s %10s %10s'  % titulos)
    for fila in informe:
            print("%10s %10d %10.2f %10.2f"  %fila)

#a= imprimir_informe(informe)
#print(a)
##%%
##5.2
#def imprimir_informe2(archivo1, archivo2):
#    precios = leer_precios(archivo2)
#    camion = leer_camion(archivo1)
#    informe2 = informe(camion, precios)
#    imprimir2 = imprimir_informe(informe2)           
#    return imprimir2
#
#impr = imprimir_informe2("Data/camion.csv","Data/precios.csv")

#%%    
#print(f"{titulos[0]:>10} {titulos[1]:>10} {titulos[2]:>10} {titulos[3]:>10}" )
#for fila in informe:
#    print(f"{fila[0]:>10s}" f"{fila[1]:>10d}" f"{f'${fila[2]:.2f}':>12s}" f"{fila[3]:>10.2f}")
#%%    
#print(f"{titulos[0]:>10} {titulos[1]:>10} {titulos[2]:>10} {titulos[3]:>10}" )    
#for nombre, cajones, precio, cambio in informe:
#    print(f"{nombre:>10s}" f"{cajones:>10d}" f"{f'${precio:.2f}':>10s}" f"{cambio:>10.2f}")