### -*- coding: utf-8 -*-
##"""
##Created on Mon Sep  7 01:40:08 2020
##
##@author: DELL
##"""
##
###5.3
##
##import csv
##
##def parse_csv(nombre_archivo):
##    '''
##    Parsea un archivo CSV en una lista de registros
##    '''
##    with open(nombre_archivo) as f:
##        rows = csv.reader(f)
##
##        # Lee los encabezados
##        headers = next(rows)
##        registros = []
##        for row in rows:
##            if not row:    # Saltea filas sin datos
##                continue
##            registro = dict(zip(headers, row))
##            registros.append(registro)
##
##    return registros
##%%
#    # fileparse.py
##import csv
##
##def parse_csv(nombre_archivo, select = None):
##    '''
##    Parsea un archivo CSV en una lista de registros.
##    Se puede seleccionar sólo un subconjunto de las columnas, determinando el parámetro select, que debe ser una lista de nombres de las columnas a considerar.
##    '''
##    with open(nombre_archivo) as f:
##        filas = csv.reader(f)
##
##        # Lee los encabezados del archivo
##        encabezados = next(filas)
##
##        # Si se indicó un selector de columnas,
##        #    buscar los índices de las columnas especificadas.
##        # Y en ese caso achicar el conjunto de encabezados para diccionarios
##
##        if select:
##            indices = [encabezados.index(nombre_columna) for nombre_columna in select]
##            encabezados = select
##        else:
##            indices = []
##
##        registros = []
##        for fila in filas:
##            if not fila:    # Saltear filas vacías
##                continue
##            # Filtrar la fila si se especificaron columnas
##            if indices:
##                fila = [fila[index] for index in indices]
##
##            # Armar el diccionario
##            registro = dict(zip(encabezados, fila))
##            registros.append(registro)
##
##    return registros
##%%
##5.5
##%%
#    # fileparse.py
#import csv
#
#def parse_csv(nombre_archivo, select = None, types=[str, int, float]):
#    '''
#    Parsea un archivo CSV en una lista de registros.
#    Se puede seleccionar sólo un subconjunto de las columnas, determinando el parámetro select, que debe ser una lista de nombres de las columnas a considerar.
#    '''
#    with open(nombre_archivo) as f:
#        filas = csv.reader(f)
#
#        # Lee los encabezados del archivo
#        encabezados = next(filas)
#
#        # Si se indicó un selector de columnas,
#        #    buscar los índices de las columnas especificadas.
#        # Y en ese caso achicar el conjunto de encabezados para diccionarios
#
#        if select:
#            indices = [encabezados.index(nombre_columna) for nombre_columna in select]
#            encabezados = select
#        else:
#            indices = []
#
#        registros = []
#        
#        for fila in filas:
#            if not fila:    # Saltear filas vacías
#                continue
#            if types:
#                fila = [func(val) for func, val in zip(types, fila) ]
#            # Filtrar la fila si se especificaron columnas
#            if indices:
#                fila = [fila[index] for index in indices]
#                
#            # Armar el diccionario
#            registro = dict(zip(encabezados, fila))
#            registros.append(registro)
#
#    return registros
#a=parse_csv('Data/camion.csv', select=['nombre', 'cajones', "precio"], types=[str, int, float])
#print(a)



#Ejercicio 5.6
import csv
def parse_csv2(nombre_archivo, select = None, types=[str, int, float], has_headers=True):
       
    '''
    Parsea un archivo CSV en una lista de registros.
    Se puede seleccionar sólo un subconjunto de las columnas, determinando el parámetro select, que debe ser una lista de nombres de las columnas a considerar.
    '''
    with open(nombre_archivo) as f:
        filas = csv.reader(f)

        # Lee los encabezados del archivo
        if has_headers:
            encabezados = next(filas)

        # Si se indicó un selector de columnas,
        #    buscar los índices de las columnas especificadas.
        # Y en ese caso achicar el conjunto de encabezados para diccionarios

            if select:
                indices = [encabezados.index(nombre_columna) for nombre_columna in select]
                encabezados = select
            else:
                 indices = []

            registros = []
        
            for fila in filas:
                if not fila:    # Saltear filas vacías
                    continue
                if types:
                    fila = [func(val) for func, val in zip(types, fila)]
                    # Filtrar la fila si se especificaron columnas
                    if indices:
                        fila = [fila[index] for index in indices]
                
            # Armar el diccionario
                registro = dict(zip(encabezados, fila))
                registros.append(registro)
        else:
            registros= []
            for fila in filas:
                if not fila:    # Saltear filas vacías
                    continue
                fila = [func(val) for func, val in zip(types, fila)]
                tupla=tuple(fila) 
                registros.append(tupla)
             
    return registros
#a = parse_csv2('Data/precios.csv', types=[str, float], has_headers=False)
#b =parse_csv2("Data/camion.csv", types=[str,int,float] , has_headers=True )
#print(a)
#print(b)