def valor_absoluto(n):
    '''
    Deveuelve el valor absoluto de un número real
    Precondicion: n es real
    Poscondición: devuelve un real
    '''
    if n >= 0:
        return n
    else:
        return -n
    
def suma_pares(l):
    '''
    Toma una lista de numeros enteros y devuelve la suma de los pares
    Precondicion: l es una lista de numeros enteros
    Poscondicion: devuelve la suma de los pares
    invariante de ciclo: res es la suma de los numeros pares analizados de la lista  
    '''
    
    res = 0
    for e in l:
        if e % 2 ==0:
            res += e
        else:
            res += 0
    return res

def veces(a, b):
    '''
    
    Devuelve el resultado del producto de dos numeros
    b distinto de 0 y entero
    '''
    res = 0
    nb = b
    while nb != 0:
        #print(nb * a + res)
        res += a
        nb -= 1
    return res

def collatz(n):
    '''
    Conjetura de Collatz
    Devuelve la cantidad de iteraciones necesarias para llegar a uno
    Si es par divide por dos y si es impar multiplica por 3 y suma 1
    invariante del ciclo : n es distinto de 1
    '''
    
    res = 1

    while n!=1:
        if n % 2 == 0:
            n = n//2
        else:
            n = 3 * n + 1
        res += 1

    return res