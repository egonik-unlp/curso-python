# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 04:01:37 2020

@author: DELL
"""

def sumar_enteros(desde, hasta):
    '''Calcula la sumatoria de los números entre desde y hasta.
       Si hasta < desde, entonces devuelve cero.

    Pre: desde y hasta son números enteros
    Pos: Se devuelve el valor de sumar todos los números del intervalo
        [desde, hasta]. Si el intervalo es vacío se devuelve 0
    '''
    if desde <= hasta:
        return (hasta+1)*(hasta/2) - (desde+1)*(desde/2)
    else:
        return 0