# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 03:48:44 2020

@author: EMA
"""

import numpy as np
import matplotlib.pyplot as plt

def randomwalk(largo):
    pasos=np.random.randint(-1,2,largo)    
    return pasos.cumsum()



def caminatas(n, largo):
    
    caminatas=[randomwalk(N) for i in range(n)]
    return caminatas

def maximos(caminatas):
    lista_de_max=[np.amax(np.absolute(caminata)) for caminata in caminatas]    
    return lista_de_max



N = 100000
n=12 
caminatas = caminatas(n,N)

listamax= maximos(caminatas)
M = listamax.index(max(listamax)) #indice de la mas alejada
m=listamax.index(min(listamax)) #indice de la menos alejada

   
plt.subplot(2, 1, 1)
for caminata in caminatas:    
    plt.plot(caminata)
plt.title("12 caminatas al azar")
plt.yticks([-500, 0, 500])
plt.xticks([])       


plt.subplot(2,2,3)
plt.title("Caminata menos alejada")
plt.plot(caminatas[m])
plt.yticks([-500, 0, 500])
plt.xticks([])
    
plt.subplot(2, 2, 4) 
plt.title("Caminata más alejada")
plt.plot(caminatas[M])
plt.yticks([-500, 0, 500])
plt.xticks([])




plt.show()