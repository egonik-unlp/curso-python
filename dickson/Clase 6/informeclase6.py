"""
Created on Wed Aug 19 03:06:10 2020

@author: EMA
"""


import fileparse
import sys
#def costo_camion(nombre_archivo):
#    costo_total=0
#    with open(nombre_archivo, "rt") as f:
#        filas = csv.reader(f)
#        encabezados = next(filas)
#            
#        for n_fila, fila in enumerate(filas, start=1):
#            record = dict(zip(encabezados, fila))
#            try:
#                ncajones = int(record['cajones'])
#                precio = float(record['precio'])
#                costo_total += ncajones * precio
#         
#            except ValueError:
#                print(f'Fila {n_fila}: No pude interpretar: {fila}')
#
#        return costo_total
def leer_camion(nombre_archivo):
    camion = fileparse.parse_csv2(nombre_archivo, select=['nombre','cajones','precio'], types=[str,int,float], has_headers=True)
    return camion
def leer_precios(nombre_archivo):
    
    precios= fileparse.parse_csv2(nombre_archivo, types=[str,float], has_headers=False)
    return precios


def total_venta(camion, precios):
    total = 0
    for fila in camion:
        precioVenta = fila["precio"]
        total += fila["cajones"]*precioVenta
    return total

def informe(camion, precios):
    informe = []
    
    for i, fila, in enumerate(camion):
        cambio = precios[i][1] - fila["precio"]
        record = (fila["nombre"], fila["cajones"], fila["precio"], cambio)
        informe.append(record)
    return informe


#costo = costo_camion(nombre_archivo1)
#precios = leer_precios(nombre_archivo)
#camion = leer_camion(nombre_arhivo)
#totalVenta = total_venta(camion, precios)
#5.1
def imprimir_informe(informe):
    
    titulos = ("Nombre", "Cajones", "Precio", "Cambio")
    print('%10s %10s %10s %10s'  % titulos)
    for fila in informe:
            print("%10s %10d %10.2f %10.2f"  %fila)



def main(argv):
    if len(argv) !=3:
        print("Uso inadecuado")
        print("Deben incorporarse otros elementos ademas del módulo")
    else:
        camion  = leer_camion(argv[1]) 
        precios = leer_precios(argv[2]) 
        info    = informe(camion, precios)
        imprimir_informe(info)            
if __name__ == "__main__":

    main(sys.argv)
