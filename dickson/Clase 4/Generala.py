# -*- coding: utf-8 -*-
"""
Created on Sun Aug 30 22:30:29 2020

@author: DELL
"""

import random


def tirar(n):
       
    tirada=[random.randint(1,6) for _ in range(n)]
    
    return(tirada)


def valormax(d):  #devuelve el dado mas repetido
    k=list(d.keys())
    v=list(d.values())
    
    return k[v.index(max(v))]


def generala():
    
    tirada = tirar(5)
    dados = [1,2,3,4,5,6]
    for i in range (2):
        repetidos ={clave: tirada.count(clave) for clave in dados} # diccionario con el num del dado  y sus repeticiones
        r =valormax(repetidos)                                     # me da el dado mas repetido si hay dos con misma cantidad de repeticiones da el primero
        vecesrep= repetidos[r]                                     # me devuelve las veces que se repitio ese dado
  
        tirada= [r]*vecesrep +tirar(5-vecesrep)       # genero una lista con el numero mas repetido
                                                               
    return  tirada


def es_generala2() :
    tirada1 = generala()
    m=False 
    if max(tirada1)==min(tirada1):
        m=True
    return m
N=1000000
G=sum([es_generala2()for i in range (N)])
prob = G/N
print(f'Tiré {N} veces, de las cuales {G} saqué generala.')
print(f'Podemos estimar la probabilidad de sacar generala es {prob:.6f}.')