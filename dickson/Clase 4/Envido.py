# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 00:32:27 2020

@author: DELL
"""
import random
valores = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12]
palos = ['oro', 'copa', 'espada', 'basto']
naipes = [(valor,palo) for valor in valores for palo in palos]

def envidoi(naipes,i):
    sotas=[10,11,12]
    n=False
    m = random.sample(naipes,k=3)
    if m[0][1] == m[1][1] and (m[0][0] + m[1][0]+ 20) == i and m[0][0] not in sotas and m[1][0] not in sotas: 
        n=True 
    elif m[0][1] == m[2][1] and (m[0][0] + m[2][0]+ 20) == i and m[0][0] not in sotas and m[2][0] not in sotas: 
        n=True
    elif m[1][1] == m[2][1] and (m[1][0] + m[2][0]+ 20) == i and m[1][0] not in sotas and m[2][0] not in sotas:
        n=True
    else:
        n=False
    return n
    
    


N = 1000000
i=32
G=sum([envidoi(naipes,i)for j in range (N)])
prob = G/N


print(f'Reparti {N} veces, de las cuales {G} saqué envido {i}.')            # no me guarda i en el f string
print(f'Podemos estimar la probabilidad de sacar envido de {i} {prob:.6f}.')