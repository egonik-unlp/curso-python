# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 11:35:15 2020

@author: DELL
"""

import os
import matplotlib.pyplot as plt

import csv
os.path.join('Data', 'arbolado-en-espacios-verdes.csv')
def leer_arboles(nombre_archivo, parque, encoding= "utf8"):
    
    with open(nombre_archivo , "rt", encoding= "utf8") as f:
        filas = csv.reader(f)
        encabezados = next(filas)
        arboleda=[{clave: valor for clave, valor in zip(encabezados, fila)} for fila in filas if fila[10]== parque]   
        
    return arboleda
    

arboleda = leer_arboles("Data/arbolado-en-espacios-verdes.csv","GENERAL PAZ")
altos = [float(arbol["altura_tot"]) for arbol in arboleda if arbol["nombre_com"]=="Jacarandá"]

plt.hist(altos,bins=25)

#%%
import os
import matplotlib.pyplot as plt
import numpy as np
import csv
os.path.join('Data', 'arbolado-en-espacios-verdes.csv')
def leer_arboles(nombre_archivo, parque, encoding= "utf8"):
    
    with open(nombre_archivo , "rt", encoding= "utf8") as f:
        filas = csv.reader(f)
        encabezados = next(filas)
        arboleda=[{clave: valor for clave, valor in zip(encabezados, fila)} for fila in filas if fila[10]== parque]   
        
    return arboleda
arboleda = leer_arboles("Data/arbolado-en-espacios-verdes.csv","GENERAL PAZ")

h = np.array([float(arbol["altura_tot"]) for arbol in arboleda if arbol["nombre_com"]=="Jacarandá"])
d = np.array([float(arbol["diametro"]) for arbol in arboleda if arbol["nombre_com"]=="Jacarandá"])
x=h
y=d
colors = np.random.rand(len(x))    #me tira error
#area = (10 * np.random.rand(x,y))**2 #me tira error

plt.scatter(d,h, c=colors, alpha =0.5)
plt.xlabel('Diámetro (cm)')
plt.ylabel('Alto (m)')
plt.title('Relación diámetro-alto para Jacarandás')
plt.show()
#%%
import os
import matplotlib.pyplot as plt

import csv
os.path.join('Data', 'arbolado-en-espacios-verdes.csv')
def leer_arboles(nombre_archivo, parque, encoding= "utf8"):
    
    with open(nombre_archivo , "rt", encoding= "utf8") as f:
        filas = csv.reader(f)
        encabezados = next(filas)
        arboleda=[{clave: valor for clave, valor in zip(encabezados, fila)} for fila in filas if fila[10]== parque]   
        
    return arboleda
arboleda = leer_arboles("Data/arbolado-en-espacios-verdes.csv","GENERAL PAZ")

especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']

def medidas_de_especies(especies,arboleda):
    d_h_especie = {especie: [(arbol['diametro'], arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com'] == especie] for especie in especies} 
    return d_h_especie
medidas=  medidas_de_especies(especies,arboleda)

def plot_d_h_especies(nombre_archivo, especies):
     for especie in especies:
        plt.scatter(*zip(*medidas[especie]), alpha = 0.40, label = especie) 
        plt.xlabel('Diámetro (cm)')
        plt.ylabel('Alto (m)')
        plt.title('Relación diámetro-alto para '+ especie)
        plt.ylim(0,30) 
        plt.xlim(0,60)
     plt.legend()
     plt.show()
    
plot_d_h_especies('arbolado-en-espacios-verdes.csv', especies)
       
   