# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 03:54:52 2020

@author: DELL
"""
#4.11
import random
N=99
G=[round(random.normalvariate(37.5,0.2),2) for i in range(N)]
G.sort()        
    
 
print(G)

#%%
#4.13
import random
import numpy as np
N=999
G=[round(random.normalvariate(37.5,0.2),2) for i in range(N)]
G.sort()        

T = np.array(G.sort())
np.save("Data/Temperaturas", T)
