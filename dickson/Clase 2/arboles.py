import csv
def leer_parque(nombre_archivo, parque, encoding= "utf8"):
    arboles=[]
    with open(nombre_archivo , "rt", encoding= "utf8") as f:
        filas = csv.reader(f)
        encabezados = next(filas)
            
        for n_fila, fila in enumerate(filas, start=1):
                record = dict(zip(encabezados, fila))
                if record["espacio_ve"] == parque:
                    arboles.append(record)

    return arboles
parque = leer_parque("Data/arbolado-en-espacios-verdes.csv","GENERAL PAZ")
print(parque)