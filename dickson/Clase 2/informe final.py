# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 03:06:10 2020

@author: EMA
"""
import csv
def costo_camion(nombre_archivo):
    costo_total=0
    with open(nombre_archivo , "rt") as f:
        filas = csv.reader(f)
        encabezados = next(filas)
            
        for n_fila, fila in enumerate(filas, start=1):
            record = dict(zip(encabezados, fila))
            print(record)
            try:
                ncajones = int(record['cajones'])
                precio = float(record['precio'])
                costo_total += ncajones * precio
         
            except ValueError:
                print(f'Fila {n_fila}: No pude interpretar: {fila}')

        return costo_total
costo = costo_camion("Data/camion.csv")
print("costo total:", costo)