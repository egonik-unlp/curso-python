# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 18:28:01 2020

@author: DELL
"""
import csv
from collections import Counter
n_arbol= Counter()

def leer_parque(nombre_archivo, parque, encoding= "utf8"):
    arboles=[]
    with open(nombre_archivo , "rt") as f:
        filas = csv.reader(f)
        encabezados = next(filas)
            
        for n_fila, fila in enumerate(filas, start=1):
                record = dict(zip(encabezados, fila))
                if record["espacio_ve"] == parque:
                    arboles.append(record)

    return arboles
lista_arboles =leer_parque("Data/arbolado-en-espacios-verdes.csv","CENTENARIO")
print(lista_arboles)

#/////////////////////////////////////////////////////////////
def contar_arboles(lista):
    for lineas in lista_arboles:
        n_arbol[lineas["nombre_com"]]+=1
    return n_arbol

contador = contar_arboles(lista_arboles) ## lista con  numero de arboles
print (contador)
arbol_fr = contador.most_common() ## Lsta con 3 arboles mas comunes
print(arbol_fr)
#///////////////////////////////////////////
altura=[]
def obtener_altura(lista, especie):
    for linea in lista:
          if linea["nombre_com"] == especie:
             altura.append(float(linea["altura_tot"]))
    return altura
    print(linea)    
    #return[altura.append(float(linea["altura_tot"])) for linea in lista_arboles if linea["nombre_com"]== especie]
lista_altura = obtener_altura(lista_arboles, "Jacarandá")
print(lista_altura)
#//////////////////////////////////////////
def altura_max(lista, especie):
    return max(lista)
alt_max= altura_max(lista_altura, "Jacaranda")
print(alt_max)
#/////////////////////////////////////////// 
def altura_prom(lista, especie):
    return sum(lista)/len(lista)
prom=altura_prom(lista_altura, "Jararandá")     
print(prom) 
#//////////////////////////////////////////// 
#inclinacion=[]
def obtener_inclinacion(lista, especie):
    #for linea in lista:
      #    if linea["nombre_com"] == especie:
     #        inclinacion.append(float(linea["inclinacio"]))
    #return inclinacion         
    return [((linea["inclinacio"])) for linea in lista if linea["nombre_com"]== especie]
lista_inclinacion = obtener_inclinacion(lista_arboles,"Jacaranda")
print(lista_inclinacion)