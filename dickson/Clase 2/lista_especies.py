# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 13:12:08 2020

@author: DELL
"""

import csv
def leer_especie(nombre_archivo, parque, encoding= "utf8"):
    especies_arboles=[]
    with open(nombre_archivo , "rt", encoding= "utf8") as f:
        filas = csv.reader(f)
        encabezados = next(filas)
            
        for n_fila, fila in enumerate(filas, start=1):
                record = dict(zip(encabezados, fila))
                if record["espacio_ve"] == parque:
                    especies_arboles.append(record["nombre_com"])
                    
    return set(especies_arboles)

    
especies = leer_especie("Data/arbolado-en-espacios-verdes.csv","GENERAL PAZ")
print(especies)