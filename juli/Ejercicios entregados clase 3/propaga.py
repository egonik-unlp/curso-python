#Ejercicio 3.9
def propagar(lista):
    i=-1
    for n in lista:
        i+=1
        if n==1:
            for x in lista:
                j=i
                if j>0 and lista[j-1]==0:
                    lista[j-1]=1
                    j=j-1
            for x in lista:
                k=i
                if k<len(lista)-1 and lista[k+1]==0:
                    lista[k+1]=1
                    k=k+1
            #otra forma:        
            # while j>0 and lista[j-1]==0:
            #     lista[j-1]=1
            #     j=j-1
            # k=i
            # while k<len(lista)-1 and lista[k+1]==0:
            #     lista[k+1]=1
            #     k=k+1
    return lista


l=[ 0, 0, 0,-1, 1, 0, 0, 0,-1, 0, 1, 0, 0]
# print(propagar(l))
# f=[ 0, 0, 0, 1, 0, 0]     
# print(propagar(f))      
# picante=[ 0, 0,-1, 0, 1, 0, -1, -1, 1, -1, 0,-1, 0, 1, 0, -1, 1, 0]
# print(propagar(picante)) 
