#E.3.18
import csv
def leer_arboles(nombre_archivo, parque, encoding="utf8"):
    with open(nombre_archivo, encoding="utf8") as f:
        rows=csv.reader(f)
        encabezado=next(rows)
        arboleda=[{clave:valor for clave, valor in zip(encabezado, row)} for row in rows if row[10] == parque]
    return arboleda 
print(leer_arboles('Data/arbolado.csv', "GENERAL PAZ", encoding="utf8"))
arboleda=leer_arboles('Data/arbolado.csv', "GENERAL PAZ", encoding="utf8")

#E.3.19
# H=[float(arbol['altura_tot']) for arbol in arboleda]
# print(H)
H_Jacarandá=[float(arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com']=='Jacarandá']
print(H_Jacarandá)
D_Jacarandá=[float(arbol['diametro']) for arbol in arboleda if arbol['nombre_com']=='Jacarandá']
print(D_Jacarandá)

#E.3.20
print(" ")
L_tuplas=[(float(arbol['altura_tot']), int(arbol['diametro'])) 
          for arbol in arboleda if arbol['nombre_com']=='Jacarandá']
print(L_tuplas)
# print(type(L_tuplas[0]))

#E.3.21
def leer_arboles(nombre_archivo):
    with open(nombre_archivo) as f:
        rows=csv.reader(f)
        encabezado=next(rows)
        arboleda=[{clave:valor for clave, valor in zip(encabezado, row)}
                  for row in rows]
    return arboleda 
print(leer_arboles('Data/arbolado.csv', encoding="utf8"))
arboleda=leer_arboles('Data/arbolado.csv')

especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']

def medidas_de_especies(e,a):
    d={especie:[(float(arbol['altura_tot']), int(arbol['diametro'])) 
              for arbol in arboleda if arbol['nombre_com']==especie] 
     for especie in especies }
    return d
# print(medidas_de_especies(especies,arboleda))
d=medidas_de_especies(especies,arboleda)
# print(d["Jacarandá"])