#Ejercicio 3.8
def invertir_lista(lista):
    invertida = []
    n=len(lista)-1
    for e in lista:
        if n>=0:
            elemento=lista[n]
            invertida.append(elemento)
        n=n-1
    return invertida
num=[1, 2, 3, 4, 5]
cap=['Bogotá', 'Rosario', 'Santiago', 'San Fernando', 'San Miguel']
print(invertir_lista(num))
print(invertir_lista(cap))