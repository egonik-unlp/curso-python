#Ejercicios 3.6 y 3.7
#Ejercicio 3.6
def buscar_u_elemento(lista, e):
    n=-1
    i=0
    for x in lista: 
        if i>n and x==e:
            n = i
        i = i + 1
    return n
f = [1,2,3,2,3,4]
print(buscar_u_elemento(f, 3))
#%%
def buscar_n_elemento(lista, e):
    i=0
    for x in lista: 
        if x==e:
            i=i+1
    return i
f = [1,2,3,2,3,4]
print(buscar_n_elemento(f, 3))
#%%
#Ejercicio 3.7
def maximo(lista):
    m=-999999999
    for e in lista:
        if e >= m:
            m=e
    return m
g = [1,2,3,2,3,4]
f = [1,2,3,8,2,3,4]
print(maximo(g))
print(maximo(f))
print(maximo([-5,-4]))
print(maximo([-5,4]))
#%%
def minimo(lista):
    m=999999999
    for e in lista:
        if e <= m:
            m=e
    return m
g = [0,2,3,2,3,4]
f = [1,2,3,8,2,3,4]
print(minimo(g))
print(minimo(f))
print(minimo([-5,-4]))
print(minimo([-89,4]))