#solucion_de_errores.py
#Ejercicios de errores en el código
#%%
#Ejercicio 3.1. Función tiene_a().
#Comentario: El error era semántico y estaba ubicado dentro del while a la altura del else.
#lo que hace el programa es salir del if luego de analizar solo el primer caracter.
#Lo corregí usando print luego de cada paso importante del código
#A continuación va el código corregido:
def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False      
print(tiene_a('UNSAM 2020'))
print(tiene_a('abracadabra'))
print(tiene_a('La novela 1984 de George Orwell'))
#%%
#Ejercicio 3.2. Función tiene_a(), nuevamente.
#Comentario: El error era de sintáxis y estaba en la linea en la cual se define la función.
#en la línea del while, en la del if y en la última. Para este último caso debía ir "False".
#Faltaban los dos puntos! (línea 27, 30 y 31). en la 31, además, debía ir "==" en vez de "=".
#Fui ejecutando el programa y viendo el tipo de error informado.
#A continuación va el código corregido:
def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False
print(tiene_a('UNSAM 2020'))
print(tiene_a('La novela 1984 de George Orwell'))
#%%
#Ejercicio 3.3. Función tiene_uno().
#Comentario: El error es en tiempo de ejecución del tipo TypeError y está en el ultimo print (linea 54).
#El valor ingresado al argumento de la función definida debería ser un string. Dentro de la función definida, la 
#función len() no es aplicable a numeros enteros.
#Identifiqué el error, leyendo el Traceback.
#A continuación va el código corregido:
def tiene_uno(expresion):
    n = len(expresion)
    i = 0
    tiene = False
    while (i<n) and not tiene:
        if expresion[i] == '1':
            tiene = True
        i += 1
    return tiene
print(tiene_uno('UNSAM 2020'))
print(tiene_uno('La novela 1984 de George Orwell'))
print(tiene_uno("1984"))
#%%
#Ejercicio 3.4. Función suma().
#Comentario: Error semántico. Para que la función funcione debería incorporarse un return identado.
#Lo identifiqúe ya que al correr el programa (sin que se trabe), arrojó como resultado un "None".
#Es decir, al invocar a la función, esta no devuleve ningún valor.
#A continuación va el código corregido:
def suma(a,b):
    n = a + b
    return n  
a = 2
b = 3
c = suma(a,b)
print(f"La suma da {a} + {b} = {c}")
#%%
#Ejercicio 3.5. Función leer_camion().
#Comentario: Dentro del for, al reasignar los valores a las claves, también
#también se están reasignandon estos elementos en registro, y por ende, se
#modifican los elementos (diccionarios) incorporados en la lista camion.
#A continuación va el código corregido:
import csv
from pprint import pprint

def leer_camion(nombre_archivo):
    camion=[]
    with open(nombre_archivo,"rt") as f:
        filas = csv.reader(f)
        encabezado = next(filas)
        for fila in filas:
            registro={}
            registro[encabezado[0]] = fila[0]
            registro[encabezado[1]] = int(fila[1])
            registro[encabezado[2]] = float(fila[2])
            camion.append(registro)
            print(camion)
            print(" ")
    return camion
camion = leer_camion("Data/camion.csv")
pprint(camion)
#dentro del for, por cada fila nueva, registro es una variable nueva
#y no modifica los elementos previamente incorporados en la lista.