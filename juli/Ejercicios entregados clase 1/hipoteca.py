saldo = 500000.0
tasa = 0.05
pago_mensual = 2684.11
total_pagado = 0.0 
mes = 0
pago_extra_mes_comienzo = 61
pago_extra_mes_fin = 108
while saldo > 0:
    mes = mes + 1
    if mes >= pago_extra_mes_comienzo and mes <= pago_extra_mes_fin:
        pago_extra = 1000
    elif saldo < pago_mensual:
        pago_mensual = saldo * (1+tasa/12)
        saldo = saldo * (1+tasa/12) - pago_mensual
        total_pagado = total_pagado + pago_mensual
        print(mes, 'Total pagado: ', round(total_pagado, 2), "Saldo restante: ", saldo)
        break
    else:
        pago_extra = 0
    saldo = saldo * (1+tasa/12) - pago_mensual - pago_extra
    total_pagado = total_pagado + pago_mensual + pago_extra
    print(mes, 'Total pagado: ', round(total_pagado, 2), "Saldo restante: ", round(saldo, 2))