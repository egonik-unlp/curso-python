import random
import numpy as np
#Ejercicio 4.15:
# def crear_album(figus_total):
#     H=np.zeros(figus_total, dtype=np.int64)
#     return H

#Ejercicio 4.16:
def album_incompleto(A):
    if 0 in A:
        return True
    return False

#Ejercicio 4.17:
def comprar_figu(figus_total,n):
    l=[]
    for i in range(n):
        l.append(random.randint(1, figus_total))
    return l  
#%%
#Ejercicio 4.18:
def cuantas_figus(figus_total):
    H=crear_album(figus_total)
    i=0
    while 0 in H:
        figu=random.randint(0, figus_total-1)
        H[figu]=1
        i=i+1
    return i

#Ejercicio 4.19:
n_repeticiones=1000
G=[cuantas_figus(6) for i in range(n_repeticiones)]
# print(G)
# print(np.mean(G))

n_repeticiones=100
G=[cuantas_figus(760) for i in range(n_repeticiones)]
# print(G)
# print(np.mean(G))
#%%
#Ejercicio 4.20:
def paquete():
    paquete=[]
    for i in range(5):
        paquete.append(random.randint(1,760))
    return paquete

#Ejercicio 4.21:
def comprar_paquete(figus_total, figus_paquete):
    paquete=[]
    for i in range(figus_paquete):
        paquete.append(random.randint(0,figus_total-1))
    return np.array(paquete)

#Ejercicio 4.23:
def crear_album(figus_total):
    H=np.zeros(figus_total, dtype=np.int64)
    return H
#%%
def cuantos_paquetes(figus_total, figus_paquete):
    H=crear_album(figus_total)
    i=0
    while 0 in H:
        figus=comprar_paquete(figus_total, figus_paquete)
        H[figus]=1
        i=i+1
    return i

#Ejercicio 4.24:
n_repeticiones=1000
G=[cuantos_paquetes(760, 5) for i in range(n_repeticiones)]

#Ejercicio 4.27:
G.sort()
print(G[900])
# print(G)
# print(np.mean(G))
#%%
#Ejercicio 4.25:
def cuantos_paquetes(figus_total, figus_paquete):
    H=crear_album(figus_total)
    i=0
    while 0 in H:
        figus=comprar_paquete(figus_total, figus_paquete)
        H[figus]=1
        i=i+1
        if 0 not in H and i<=850:
            return True
    return False

n_repeticiones=100
K=[cuantos_paquetes(760, 5) for i in range(n_repeticiones)]
G=sum([cuantos_paquetes(760, 5) for i in range(n_repeticiones)])
# print(K)
# print(G)
print(G/n_repeticiones)

#Ejercicio 4.27:
#Aprox 1300 paquetes para tener una chance del 90 %.
#(lo hice variando el i en "cuantos_paquetes")
#Ver en el cubiculo anterior que lo hice como explicaron en clase.


#%%
#Ejercicio 4.26:
import matplotlib.pyplot as plt

def cuantos_paquetes(figus_total, figus_paquete):
    H=crear_album(figus_total)
    i=0
    while 0 in H:
        figus=comprar_paquete(figus_total, figus_paquete)
        H[figus]=1
        i=i+1
    return i

n_repeticiones=100
G=[cuantos_paquetes(760, 5) for i in range(n_repeticiones)]

plt.hist(G,bins=10)
#%%
#Ejercicio 4.28:
def comprar_paquete1(figus_total, figus_paquete):
    paquete=random.sample([range(figus_total)], k=figus_paquete)
    return np.array(paquete)
print(comprar_paquete(760, 5))
def cuantos_paquetes(figus_total, figus_paquete):
    H=crear_album(figus_total)
    i=0
    while 0 in H:
        figus=comprar_paquete(figus_total, figus_paquete)
        H[figus]=1
        i=i+1
        if 0 not in H and i<=850:
            return True
    return False

# n_repeticiones=100
# K=[cuantos_paquetes(760, 5) for i in range(n_repeticiones)]
# G=sum([cuantos_paquetes(760, 5) for i in range(n_repeticiones)])
# print(K)
# print(G)
# print(G/n_repeticiones)













    








