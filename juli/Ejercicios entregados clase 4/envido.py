import random
#Ejercicio 4.8
valores = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12]
palos = ['oro', 'copa', 'espada', 'basto']
naipes = [(valor,palo) for valor in valores for palo in palos]

Viejas=[10, 11, 12]

def repartida31():
    T=random.sample(naipes,k=3)
    if T[0][0]+T[1][0] == 11 and T[1][0] not in Viejas and T[0][0] not in Viejas and T[0][1]==T[1][1]:
        boolean=True
    elif T[0][0]+T[2][0] == 11 and T[0][0] not in Viejas and T[2][0]  not in Viejas and T[0][1]==T[2][1]:
        boolean=True
    elif T[2][0]+T[1][0] == 11 and T[2][0] not in Viejas and T[1][0] not in Viejas and T[2][1]==T[1][1]:
        boolean=True
    else:
        boolean=False
    return boolean

N=1000000
G=sum([repartida31() for i in range(N)])
print("repartí", N, "veces, obtuve 31", G, "veces. La probabilidad es ", G/N)
#%%
def repartida32():
    T=random.sample(naipes,k=3)
    if T[0][0]+T[1][0] == 12 and T[1][0] not in Viejas and T[0][0] not in Viejas and T[0][1]==T[1][1]:
        boolean=True
    elif T[0][0]+T[2][0] == 12 and T[0][0] not in Viejas and T[2][0] not in Viejas and T[0][1]==T[2][1]:
        boolean=True
    elif T[2][0]+T[1][0] == 12 and T[2][0] not in Viejas and T[1][0] not in Viejas and T[2][1]==T[1][1]:
        boolean=True
    else:
        boolean=False
    return boolean

N=1000000
R=sum([repartida32() for i in range(N)])
print("repartí", N, "veces, obtuve 32", R, "veces. la probabilidad es ", R/N)
#%%
def repartida33():
    T=random.sample(naipes,k=3)
    if T[0][0]+T[1][0] == 13 and T[1][0] not in Viejas and T[0][0] not in Viejas and T[0][1]==T[1][1]:
        boolean=True
    elif T[0][0]+T[2][0] == 13 and T[0][0] not in Viejas and T[2][0] not in Viejas and T[0][1]==T[2][1]:
        boolean=True
    elif T[2][0]+T[1][0] == 13 and T[2][0] not in Viejas and T[1][0] not in Viejas and T[2][1]==T[1][1]:
        boolean=True
    else:
        boolean=False
    return boolean

N=1000000
X=sum([repartida33() for i in range(N)])
print("repartí", N, "veces, obtuve 33", X, "veces. La probabilidad es ", X/N)

































