#Ejercicio 2.22
import csv
def leer_parque(nombre_archivo, parque, encoding="utf8"):
    with open(nombre_archivo, encoding="utf8") as f:
        filas=csv.reader(f)
        e=next(filas)
        j=[]
        for linea in filas:
            record = dict(zip(e, linea))
            if record["espacio_ve"] == parque:
                j.append(record)
            else:
                None
    return j
# print(leer_parque('Data/arbolado.csv', "GENERAL PAZ", encoding="utf8"))
# print(len(leer_parque('Data/arbolado.csv', "GENERAL PAZ", encoding="utf8"))) #da 690

l = leer_parque('Data/arbolado.csv', "BARRIO GENERAL SAN MARTIN", encoding="utf8")

#Ejercicio 2.23
def especies(lista_arboles):
    k = []
    i=-1
    for n in lista_arboles:
        i = i + 1
        e = lista_arboles[i]['nombre_com'] 
        k.append(e)
    return set(k)
# print(especies(l))
# print(len(especies(l)))

#E2.24
#total de cada tipo de arbol.
from collections import Counter
cla = Counter()
def contar_ejemplares(lista_arboles):
    i = -1
    d = dict()
    for n in lista_arboles:
        i = i + 1
        k = lista_arboles[i]['nombre_com']
        cla[k] = cla[k] + 1
        d[k]=cla[k]
    return d         
# print(contar_ejemplares(l))
    
#total de los 5 arboles más frecuentes.
cla = Counter()    
def contar_ejemplares_mc(lista_arboles):
    i = -1
    for n in lista_arboles:
        i = i + 1
        k = lista_arboles[i]['nombre_com']
        cla[k] = cla[k] + 1
    return cla.most_common(5)

# print(contar_ejemplares(l))

#E2.25
def obtener_alturas(lista_arboles, especie):
    i = -1
    D = []
    for n in lista_arboles:
        i = i + 1
        if lista_arboles[i]['nombre_com'] == especie:
            k = float(lista_arboles[i]['altura_tot'])
            D.append(k)
        else:
            None
    return D
li = obtener_alturas(l, 'Jacarandá')
# print(li)    
# print(max(li))
# print(len(li))
# defino calculador de promedios de altura.
def prom_arboles(lista):
    i=0    
    for n in lista:   
        i = i + n
    prom = i/len(lista)
    return prom
# print(prom_arboles(li))

#E2.26
def obtener_inclinaciones(lista_arboles, especie):
    i = -1
    D = []
    for n in lista_arboles:
        i = i + 1
        if lista_arboles[i]['nombre_com'] == especie:
            k = float(lista_arboles[i]['inclinacio'])
            D.append(k)
        else:
            None
    return D
# print(obtener_inclinaciones(l, 'Jacarandá'))



l = leer_parque('Data/arbolado.csv', "ANDES, LOS", encoding="utf8")
#Ejercicio 2.23
# def especies(lista_arboles):
#     k = []
#     i=-1
#     for n in lista_arboles:
#         i = i + 1
#         e = lista_arboles[i]['nombre_com'] 
#         k.append(e)
#     return set(k)

#E2.27
def especimen_mas_inclinado(lista_arboles):
    inclinación_máxima = -1
    for n in lista_arboles:
        if float(n["inclinacio"]) > inclinación_máxima:
           inclinación_máxima = float(n["inclinacio"])
           especie = n['nombre_com']
    return especie
# print(especimen_mas_inclinado(l))


# #E2.28
# def especimen_promedio_mas_inclinado(lista_arboles):
#     for n in lista_arboles:
#         asd = (n['nombre_com'], float(n["inclinacio"]))
# print(especimen_promedio_mas_inclinado(l))
ang = Counter()
an = Counter()
def especimen_promedio_mas_inclinado(lista_arboles):
    LT = [(n['nombre_com'], float(n["inclinacio"])) for n in lista_arboles]
    m = []
    n = set(m)
    l = []
    f = []
    for x in LT:
        m.append(x[0])
        ang[x[0]] = ang[x[0]] + 1
        l.append(ang.values())
        an[float(x[1])] = an[float(x[1])] + float(x[1])
        f.append(an.values())
#     print(n)
#     print(l)
#     print(f)

# print(especimen_promedio_mas_inclinado(l))
#%%
#E.3.18
def leer_arboles(nombre_archivo, parque, encoding="utf8"):
    with open(nombre_archivo, encoding="utf8") as f:
        rows=csv.reader(f)
        encabezado=next(rows)
        arboleda=[{clave:valor for clave, valor in zip(encabezado, row)} for row in rows if row[10] == parque]
    return arboleda 
print(leer_arboles('Data/arbolado.csv', "GENERAL PAZ", encoding="utf8"))
arboleda=leer_arboles('Data/arbolado.csv', "GENERAL PAZ", encoding="utf8")

# E.3.19
H=[float(arbol['altura_tot']) for arbol in arboleda]
print(H)
H_Jacarandá=[float(arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com']=='Jacarandá']
print(H_Jacarandá)
D_Jacarandá=[float(arbol['diametro']) for arbol in arboleda if arbol['nombre_com']=='Jacarandá']
print(D_Jacarandá)

#E.3.20
print(" ")
L_tuplas=[(float(arbol['altura_tot']), int(arbol['diametro'])) 
          for arbol in arboleda if arbol['nombre_com']=='Jacarandá']
print(L_tuplas)
print(type(L_tuplas[0]))

#E.3.21
# def leer_arboles(nombre_archivo, encoding="utf8"):
#     with open(nombre_archivo, encoding="utf8") as f:
#         rows=csv.reader(f)
#         encabezado=next(rows)
#         arboleda=[{clave:valor for clave, valor in zip(encabezado, row)}
#                   for row in rows]
#     return arboleda 
# print(leer_arboles('Data/arbolado.csv', encoding="utf8"))
# arboleda=leer_arboles('Data/arbolado.csv')

# especies= ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']

def medidas_de_especies(e,a):
    d={especie:[(float(arbol['altura_tot']), int(arbol['diametro'])) 
              for arbol in arboleda if arbol['nombre_com']==especie] 
     for especie in especies }
    return d
# print(medidas_de_especies(especies,arboleda))
d=medidas_de_especies(especies,arboleda)
# print(d["Jacarandá"])
#%%
#Ejercicio 4.30:
import os
import matplotlib.pyplot as plt
os.path.join('Data', 'arbolado-en-espacios-verdes.csv')
arboleda = leer_arboles('Data/arbolado.csv', "GENERAL PAZ", encoding="utf8")
H_Jacarandá=[float(arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com']=='Jacarandá']
# plt.hist(H_Jacarandá,bins=15)

#Ejercicio 4.31
import random
import numpy as np
H=np.array([float(arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com']=='Jacarandá'])

D=np.array([float(arbol['diametro']) for arbol in arboleda if arbol['nombre_com']=='Jacarandá'])

import matplotlib.pyplot as plt
plt.scatter(D,H)




























