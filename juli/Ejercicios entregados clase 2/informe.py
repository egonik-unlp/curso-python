#Ejercicio 2.19
import csv
def costo_camion(archivo):
    precio = 0
    with open(archivo, "rt") as AA:
        lineas = csv.reader(AA)
        e = next(lineas)
        for n, linea in enumerate(lineas, start=1):
            record = dict(zip(e, linea))
            try:
                l= int(record["cajones"])*float(record["precio"]) 
                precio = precio + l                
            except ValueError:                     
                print("Fila:", n, "no puede interpretar: ", linea) 
        return precio
t = costo_camion("Data/camion.csv")
print(t)
#'Data/"Data/fecha_camion.csv" = 47671.15
#'Data/"Data/camion.csv" = 47671.15
#'Data/"Data/missing.csv" =Fila: 4 no puede interpretar:  ['Mandarina', '', '51.23']
                    #Fila: 7 no puede interpretar:  ['Naranja', '', '70.44']
                    #30381.15