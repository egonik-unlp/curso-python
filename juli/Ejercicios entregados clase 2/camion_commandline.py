#Ejerciciov 2.8
import sys
def costo_camion(archivo):
    AA = open(archivo, "rt")
    next(AA)
    precio = 0
    for linea in AA:
        try:
            l= float(linea.split(",")[1])*float(linea.split(",")[2])
            precio = precio + l
        except ValueError:
            print("No hay cajones de " + linea.split(",")[0])      
    return precio

print(costo_camion('Data/missing.csv'))

#'Data/camion.csv' = 47671.15
#"Data/missing.csv" = 'No hay cajones de Mandarina
                     #No hay cajones de Naranja
                     #30381.15

if len(sys.argv) == 2:
    nombre_archivo = sys.argv[1]        #si estoy ejecutando desde terminal, y agrego un archivo
else:                                   #luego de camion_commandline.py (luego de haber abierto python)
    nombre_archivo = 'Data/camion.csv'  #estas lineas permiten que se "procese" ese archivo.
costo = costo_camion(nombre_archivo)    #si yo no pongo nada (solo camion_commandline.py + enter),
print('Costo total: $', costo)          #estas lineas permiten que se "procese" por defaul 'Data/camion.csv'.