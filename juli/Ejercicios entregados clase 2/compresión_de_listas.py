#E.2.10
# nums = [1,2,3,4]
# cuadrados = [ x * x for x in nums ]
# print(cuadrados)
# dobles = [ 2 * x for x in nums if x > 2 ]
# print(dobles)

#E.2.11
def leer_camion(nombre_archivo):
    camion = []
    with open(nombre_archivo, 'rt') as AA:
        next(AA)
        for linea in AA:
            d = {}
            l = linea.split(',')
            d['nombre'] = l[0]
            d['cajones'] = float(l[1])
            d['precio'] = float(l[2])
            camion.append(d)
    return camion
# print(leer_camion('Data/camion.csv'))
# camion = leer_camion('Data/camion.csv')
# costo = sum([ s['cajones'] * s['precio'] for s in camion ])
# print(costo)

import csv
def leer_precios(nombre_archivo):
    with open(nombre_archivo, 'rt') as f:
        d = {}    
        lineas = csv.reader(f)
        for linea in lineas:
            try:
                d[linea[0]] = float(linea[1])
            except IndexError:
                d['v'] = 'v'
    return d
# print(leer_precios('Data/precios.csv'))
precios = leer_precios('Data/precios.csv')
# valor = sum([ s['cajones'] * precios[s['nombre']] for s in camion ])
# print(valor)


#E.2.12
# mas100 = [ s for s in camion if s['cajones'] > 100 ]
# print(mas100)

# MaNa = [s for s in camion if s['nombre']=='Naranja' or s['nombre']=='Mandarina']
# print(MaNa)
# myn = [s for s in camion if s["nombre"] in {"Naranja", "Mandarina"}]
# print(myn)

# F10k=[s for s in camion if s["precio"]*s["cajones"]>10000]
# print(F10k)


#E.2.13
# nombre_cajones=[(s['nombre'], s['cajones']) for s in camion]
# print(nombre_cajones)
# nombre_cajones1={(s['nombre'], s['cajones']) for s in camion}
# print(nombre_cajones1)

# nombres=[s['nombre'] for s in camion]
# print(nombres)
# nombres1={s['nombre'] for s in camion} #te agrupa los repetidos, como con sets
# print(nombres1)

# stock = { nombre: 0 for nombre in nombres }
# print(stock) 

# for s in camion:
#     stock[s['nombre']] += s['cajones'] #forma de agrupar nombres y precios.
# print(stock)

# camion_precios = {nombre: precios[nombre] for nombre in nombres}
# print(camion_precios)


#E.3.14
f = open('Data/fecha_camion.csv')
rows = csv.reader(f)
row = next(rows)
# print(headers)

select = ['nombre', 'cajones', 'precio']
# print(select)

indices=[row.index(n) for n in select]
# print(indices)

# row = next(rows)
# # print(row)
# record =  {n: row[index] for n, index in zip(select, indices)}   # comprensión de diccionario
# print(record)

camion = [{n: row[index] for n, index in zip(select, indices)}
          for row in rows]
print(camion)
print(" ")
# kujb=camion.append(record)
# print(kujb)

































