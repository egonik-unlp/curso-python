# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 16:43:36 2020

@author: Administrador
"""

# y=[2,3,4,5,6,9]
# print(y)
# print(y)
# t=y*2
# print(t)
# y.append(11)
# print(y)

a = [1,2,3]
b = a
a = [4,5,6]
# b = a # quitar o no este comentario hace la diferencia.
print(a)      
print(b)

# >>> a.append(5)
# >>> a
# [2, 3, [100, 101], 4, 5]
# >>> b
# [2, 3, [100, 101], 4]
# A pesar de esto, los elementos de a y de b siguen siendo compartidos.

# >>> a[2].append(102)
# >>> b[2]
# [100,101,102]
# >>>
# >>> a[2] is b[2]
# True

# Cambiar el elemento no es lo mismo que agregar uno nuevo, el cambio ocurre
#para a y b, pero si agrego elemento nuevo a a, b permanece inalterado.

# A veces vas a necesitar hacer una copia de un objeto así como de todos los objetos que contenga. Llamamos a esto una copia pofunda (deep copy) Podés usar el módulo copy para esto:

# >>> a = [2,3,[100,101],4]
# >>> import copy
# >>> b = copy.deepcopy(a)
# >>> a[2].append(102)
# >>> b[2]
# [100,101]
# >>> a[2] is b[2]
# False
#%%
#E.3.17
import csv
f = open('Data/dowstocks.csv')
rows = csv.reader(f)
headers = next(rows)
row = next(rows)


#print(headers)
# print(row)


x=row[2].split("/")
row[2]=(int(x[0]), int(x[1]), int(x[2]))


types=[str, float, tuple, str, float, float, float, float, int]

dic={clave:conv(dato) for clave, conv, dato in zip(headers, types, row)}
print(dic)
# print(" ")





# print(dic['name'])
# print(dic['date'])


# types = [str, float, str, str, float, float, float, float, int]
# converted = [func(val) for func, val in zip(types, row)]
# record = dict(zip(headers, converted))
# print(record)























