#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 19:33:09 2020

@author: gonik
"""


import os
import csv
os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')

def leer_camion(nombre_archivo):
    '''Computa el precio total del camion (cajones*precio) de un archivo'''
    total = 0.0

    with open(nombre_archivo, 'rt') as f:
        items = []
        rows = csv.reader(f)
        headers = next(rows)
        for row in rows:
            ncajones = int(row[1])
            precio = float(row[2])
            nodo = {
                'nombre': row[0],
                'cantidad':ncajones,
                'precio': precio
                }
            items.append(nodo)
        return items
    
    
def leer_precios(archivo_csv):
    
    with open (archivo_csv, 'rt') as file:
        rows = csv.reader(file)
        d = {}
        for row in rows:
            d[row[0]] = round(float(row[1]),2)
            
    return d

def sim_ventas(lista_dict,dict_precios):
    total = 0.0
    vendidos = 0.0
    for nodo in lista_dict:
        li = list(nodo.values())
        tot_item = li[1]*li[2]
        total += tot_item 
        tot_vendidos = dict_precios[li[0]]*li[1]
        vendidos+= tot_vendidos
    return total, vendidos

        


    
    
en_local = leer_camion('Data/camion.csv')
costo_lc_mercado = sum([s['cantidad']*precios[s['nombre']] for s in en_local])
precios = leer_precios('Data/precios.csv')

costo, ganancia = sim_ventas(en_local,precios)
balance = ganancia - costo

print(f'Se invirtieron ${costo:0.2f} y se vendieron ${ganancia:0.2f}, dando un balance ${balance:0.2f}')



