#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 18:40:42 2020

@author: gonik
"""


import sys

def costo_camion(arch):
    import os
    os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
    with open(arch, 'rt' ) as file:
        headers = next(file) ##no se usa
        suma_total = 0
        for line in file:
            try:
                list_line= line.split(',')
                suma_total+= int(list_line[1])*float(list_line[2])
            except ValueError:
                print(f'Hay datos no válidos para el item {list_line[0]}')
    print('suma',suma_total)
    return suma_total 

if len(sys.argv) == 2:
    archivo = sys.argv[1]
else:
    archivo = 'Data/camion.csv'
print('ARCHIVO',archivo)

costo = costo_camion(archivo)


print(f'El costo total de los items contenidos en el camión es de $ {costo:0.2f}')
      