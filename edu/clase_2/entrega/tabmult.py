"""
Created on Sun Aug 23 00:39:24 2020
@author: gonik
"""

base = '  '
for n in range(10):
    base += (f'{n:>3d}')
print(base)
print('---------------------------------')

base2 = 0

for i in range(10):
    l = (0,)
    for j in range(9): 
        base2 +=  i
        l += (base2,)
       
    #print('0','%3s %3s %3s %3s %3s %3s %3s %3s %3s ' % l)         
    A,B,C,D,E,F,G,H,I,J = l    
    print(f'{i}:{A:>3d}{B:>3d}{C:>3d}{D:>3d}{E:>3d}{F:>3d}{G:>3d}{H:>3d}{I:>3d}{J:>3d}') 
    base2 = 0    
