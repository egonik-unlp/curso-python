#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 19:14:09 2020

@author: gonik
"""

import os
import csv

os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')

def leer_camion(nombre_archivo):
    '''Computa el precio total del camion (cajones*precio) de un archivo'''
    total = 0.0

    with open(nombre_archivo, 'rt') as f:
        items = []
        rows = csv.reader(f)
        headers = next(rows)
        for row in rows:
            ncajones = int(row[1])
            precio = float(row[2])
            nodo = {
                'nombre': row[0],
                'cajones':ncajones,
                'precio': precio
                }
            items.append(nodo)
        return items
    
    

lista = leer_camion('Data/camion.csv')

print(lista)
