#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 19:19:48 2020

@author: gonik
"""

import csv
import os



def leer_precios(archivo_csv):
    os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
    with open (archivo_csv, 'rt') as file:
        rows = csv.reader(file)
        headers = next(rows) ###no se usa
        d = {}
        for row in rows:
            d[row[0]] = round(float(row[1]),2)
            
    return d


precios = leer_precios('Data/precios.csv')
