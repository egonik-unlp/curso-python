#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 22:58:11 2020

@author: gonik
"""

import os
import csv
os.chdir('/home/gonik/Documents/python_unsam/Ejercicios/')
    
def leer_precios(archivo):
    file = 'Data/' + archivo
    import os
    import csv
    os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
    with open(file, 'rt') as file:
        generador = csv.reader(file)
        lista_de_precios = []
        d = {}
        for item in generador:
            d[item[0]] = round(float(item[1]),2)
    return d
precios = leer_precios('precios.csv')
#%%

def diccionario(lista):
    d = {}
    d['Nombre'] = lista[0]
    d['Cantidad'] = int(lista[1])
    d['Precio'] = round(float(lista[2]),2)
    return d
#%%

def costo_camion(archivo):
    costo_total = 0
 
    file = 'Data/'+ archivo
    filas = []
    d = {}
    with open(file, 'rt') as file:        
       rows = csv.reader(file)
       headers = next(rows)
       cantidad = []
       for n_fila, fila in enumerate(rows, start=1):
            record = dict(zip(headers, fila))
            try:
                ncajones = int(record['cajones'])
                precio = float(record['precio'])
                costo_total += ncajones * precio
                cantidad.append(ncajones)
            # Esto atrapa errores en los int() y float() de arriba.
            except ValueError:
                print(f'Fila {n_fila}: No pude interpretar: {fila}')
       
    #print(f'El costo total es ${costo_total:1.2f}')
    return  costo_total, cantidad
costo_total = costo_camion('camion.csv')
gasto_total = sum([round(costos[i]['Cantidad']*costos[i]['Precio'],2) for i in range(len(costos))])
suma_ganancias = 0
for item in costos:
     suma_ganancias += item['Cantidad']*(precios[item['Nombre']])
balance = round(suma_ganancias - gasto_total,2)
print(f'El gasto fue de {gasto_total}, la ganancia bruta fue de {suma_ganancias} y el balance fue de {balance}')
    
