# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.

"""

def costo_camion(file):
    import os
    os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
    with open(file, 'rt' ) as file:
        #headers = next(file) ##suplido por el start de enumearte
        for nline, line in enumerate(file, start = 1):
            try:
                suma_total = 0
                list_line= line.split(',')
                suma_total+= int(list_line[1])*float(list_line[2])
            except ValueError:
                print(f'Hay datos no válidos para el item {list_line[0]} en la linea {nline}')
    return suma_total 


costo = costo_camion('Data/missing.csv')