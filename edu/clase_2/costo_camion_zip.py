#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 16:10:44 2020

@author: gonik
"""

###ejercicio entrega 2.19

import csv
import os
import sys


os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')

def costo_camion(archivo_csv):
    with open(archivo_csv, 'rt') as file:
        rows = csv.reader(file)
        headers = next(rows)
        ctotal = 0
        for nline, line in enumerate(rows):
            record = dict(zip(headers, line))
           
            try:
                name = record['nombre']
                precio = float(record['precio'])
                ncajones = int(record['cajones'])
                ctotal += precio * ncajones
            except ValueError:
                print(f'en la linea {nline} hay un error, el contenido {name} no fue procesado')
    return ctotal


if len(sys.argv) == 2:
    archivo = sys.argv[1]
else:
    archivo = 'Data/camion.csv'


costo = costo_camion(archivo)

print(f'El costo total ${costo:0.2f}')