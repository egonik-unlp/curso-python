#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 20:48:40 2020

@author: gonik
"""
import random as rd
import timeit as tt
import numpy as np
import matplotlib.pyplot as plt


from streamlined.insercion import ord_insercion1 as insercion
from streamlined.burbujeo import burbujeo
from streamlined.seleccion import ord_seleccion as seleccion
from streamlined.merge2 import merge_sort as merge
def generar_listas(Nmax):
    return [[rd.randint(1,1e3) for i in range(N) ] for N in range(1,Nmax)]


mux = [insercion,burbujeo,seleccion, merge]
global metodos

def comp(num,Nmax, met):
    listas = generar_listas(Nmax)
    global lista
    global mu
    te_ins, te_burb, te_sel, te_merge = [[tt.timeit('mu(lista.copy())', number = num, globals = globals()) for lista in listas] for mu in met]
    return te_ins, te_burb, te_sel, te_merge



x,y,j,p = comp(1,256, mux)

