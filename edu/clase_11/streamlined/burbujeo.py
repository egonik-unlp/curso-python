#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 16:26:47 2020

@author: gonik
"""

def burbujeo(lista):
    ct = len(lista)
    cp = 0
    while cp < ct:
        for i in range((ct - cp) -1 ):
            if lista[i] > lista[i + 1]:
                lista[i], lista[i + 1]  = lista[i + 1], lista[i]
        cp+=1      

