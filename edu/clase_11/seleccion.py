#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 16:11:03 2020

@author: gonik
"""

def ord_seleccion(lista, debug = False):
    """Ordena una lista de elementos según el método de selección.
       Pre: los elementos de la lista deben ser comparables.
       Post: la lista está ordenada."""

    # posición final del segmento a tratar
    n = len(lista) - 1

    # mientras haya al menos 2 elementos para ordenar
    nct = 0
    while n > 0:
        # posición del mayor valor del segmento
        p, nc = buscar_max(lista, 0, n)
        nct += nc
        # intercambiar el valor que está en p con el valor que
        # está en la última posición del segmento
        lista[p], lista[n] = lista[n], lista[p]
        if debug:
            print("DEBUG: ", p, n, lista)

        # reducir el segmento en 1
        n = n - 1 

        
    return nct

def buscar_max(lista, a, b):
    """Devuelve la posición del máximo elemento en un segmento de
       lista de elementos comparables.
       La lista no debe ser vacía.
       a y b son las posiciones inicial y final del segmento"""

    pos_max = a
    nc = 0
    for i in range(a + 1, b + 1):
        nc += 1
        if lista[i] > lista[pos_max]:
            pos_max = i
            
    return pos_max, nc