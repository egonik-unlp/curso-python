#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 16:26:47 2020

@author: gonik
"""
import numpy as np
import matplotlib.pyplot as plt
import random as rd
N = 40

def burbujeo(lista):
    ct = len(lista)
    cp = 0
    comp = 0
    while cp < ct:
        for i in range((ct - cp) -1 ):
            comp+=1
            if lista[i] > lista[i + 1]:
                lista[i], lista[i + 1]  = lista[i + 1], lista[i]
        cp+=1      
    return comp
if __name__ == 'main':
    v = np.zeros((N,2))
    for i in range(N):
        v[i,0],v[i,1]  = i, burbujeo([rd.randint(1,N) for i in range(i)])
    plt.plot(v)
    
    lista_1 = [1, 2, -3, 8, 1, 5]
    lista_2 = [1, 2, 3, 4, 5]
    lista_3 = [0, 9, 3, 8, 5, 3, 2, 4]
    lista_4 = [10, 8, 6, 2, -2, -5]
    lista_5 = [2, 5, 1, 0]
        
    l = [lista_1,lista_2,lista_3,lista_4,lista_5]
    
    for i in l:
        print(i, burbujeo(i.copy())[0])