#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 17:15:17 2020

@author: gonik
"""
import numpy as np
from seleccion import ord_seleccion as seleccion
from insercion import ord_insercion as insercion
from burbujeo import burbujeo

import random as rd

def generar_lista(N):
    return[rd.randint(-N,N) for i in range(N)]

def comp(k ,N, metodos = [seleccion, insercion, burbujeo] ):
    vector = np.zeros((k, len(metodos)))
    for rep in range(k):
        lista = generar_lista(N)
        for idx, metodo in enumerate(metodos):
            vector[rep, idx] = metodo(lista)
    return vector

metodos = [seleccion, insercion, burbujeo]
vector = comp(100,10)
for idx, metodo in enumerate(metodos):
    print(f'Promedio con método {metodo.__name__} = {vector[:,idx].mean()}')
          
          
