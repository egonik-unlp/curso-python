#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 20:48:40 2020

@author: gonik
"""
import random as rd
import timeit as tt
import numpy as np
import matplotlib.pyplot as plt


from streamlined.insercion import ord_insercion1 as insercion
from streamlined.burbujeo import burbujeo
from streamlined.seleccion import ord_seleccion as seleccion
from streamlined.merge2 import merge 
def generar_listas(Nmax):
    return [[rd.randint(1,1e3) for i in range(N) ] for N in range(1,Nmax)]


metodos = [insercion,burbujeo,seleccion, merge]
def comp(num,Nmax, metodos):
    listas = generar_listas(Nmax)
    global lista
    te_ins, te_burb, te_sel, te_merge = [np.array([tt.timeit('m(lista.copy())', number = num, globals = globals()) for lista in listas]) for m in metodos]
    return te_ins, te_burb, te_sel, te_merge



x,y,j,p = comp(1,256, metodos)

num = 1
lis = generar_listas(256)
lupe = []
for lista in lis:
    print(lista)
    lupe.append(tt.timeit('merge(lista)', number = num, globals = globals()))
    