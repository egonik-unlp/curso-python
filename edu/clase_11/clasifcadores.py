#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 12:53:43 2020

@author: gonik
"""
import seaborn as sns
from sklearn.datasets import load_iris
iris_dataset = load_iris()

import pandas as pd

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(
iris_dataset['data'], iris_dataset['target'], random_state = 0)

from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors = 1)

knn.fit(X_train, y_train)
import numpy as np
X_new = np.array([[5, 2.9, 1, 0.2]])

import matplotlib.pyplot as plt
plt.scatter(X_train[:, 1], X_train[:, 3], c = y_train)
plt.scatter(X_new[:, 1], X_new[:, 3], c = 'red')
y_pred = knn.predict(X_test)
print("Predicciones para el conjunto de Test:\n", y_pred)
print("Etiquetas originales de este conjunto:\n", y_test)
print("K-Nearest Neighbours set score: {:.2f}".format(knn.score(X_test, y_test)))
   

from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()

clf.fit(X_train, y_train)
yhat2 = clf.predict(X_test)
print("Tree Classifier test set score: {:.2f}".format(clf.score(X_test, y_test)))
