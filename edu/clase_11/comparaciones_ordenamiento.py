#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 17:45:49 2020

@author: gonik
"""

import os
os.chdir('/home/gonik/Documents/repo_curso/curso_python/edu/clase_11')

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import random as rd
from seleccion import ord_seleccion as seleccion
from insercion import ord_insercion1 as insercion1
from burbujeo import burbujeo
from merge import merge_sort as merge

os.chdir('/home/gonik/Documents/repo_curso/curso_python/edu/clase_11')

listas = [[rd.randint(1,1e3) for i in range(N) ] for N in range(256)]

metodos = [seleccion, insercion1, burbujeo]

comparaciones_seleccion, comparaciones_insercion, comparaciones_burbujeo = [np.array([metodo(i.copy()) for i in listas]) for metodo in metodos]
comparaciones_merge = np.array([merge(i.copy())[1] for i in listas])

fig, ax = plt.subplots(figsize = (10,6 ), dpi = 300)
ax.plot(comparaciones_seleccion, c = 'r', ls = 'dashdot', label = 'Ordenamiento por seleccion')
ax.plot(comparaciones_insercion,  c = 'b', label = 'Ordenamiento por insercion')
ax.plot(comparaciones_burbujeo, c = 'g',ls = '--',  label = 'Ordenamiento por burbujeo')
ax.plot(comparaciones_merge, c = 'black',  label = 'Ordenamiento por merge')

ax2 = fig.add_axes([0.15, 0.4, 0.2, 0.2])
ax2.plot(comparaciones_seleccion, c = 'r', ls = 'dashdot')
ax2.plot(comparaciones_insercion,  c = 'b')
ax2.plot(comparaciones_burbujeo, c = 'g',ls = '--')
ax2.plot(comparaciones_merge, c = 'black')
ax2.set_ylim((0,300))
ax2.set_yticks([])
ax2.set_xticks([i for i  in np.linspace(0,200,3)])
ax.set_xlabel('longitud de lista')
ax.set_ylabel('numero de comparaciones')
ax.set_title('comparacion de distintos métodos de ordenamiento de listas')
ax.legend()
plt.show()


