#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 01:29:29 2020

@author: gonik
"""

import matplotlib.pyplot as plt
import requests
import io
from sklearn import linear_model

enlace = 'https://raw.githubusercontent.com/python-unsam/UNSAM_2020c2_Python/master/Notas/10_Recursion/longitudes_y_pesos.csv'
r = requests.get(enlace).content
data_lyp = pd.read_csv(io.StringIO(r.decode('utf-8')))

modelo = linear_model.LinearRegression(fit_intercept=0)
modelo.fit(data_lyp[['longitud']], data_lyp[['peso']])

x = np.linspace(data_lyp.longitud.min(), data_lyp.longitud.max(), 50).reshape(-1,1)

yhat = modelo.predict(x)


data_lyp.plot.scatter('longitud', 'peso', label = 'Datos')
ECM = np.power(data_lyp.peso - data_lyp.longitud*modelo.coef_[0][0], 2).mean() 

plt.plot(x,yhat, c = 'green' ,label = f'ajuste lineal, ECM = {ECM:0.4f}')
plt.legend()
plt.plot()

print('ECM = ',ECM)
