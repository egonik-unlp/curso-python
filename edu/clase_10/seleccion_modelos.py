#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 03:12:06 2020

@author: gonik
"""
import numpy as np
from sklearn import linear_model
#%% Datos Sintéticos
np.random.seed(3141) # semilla para fijar la aleatoriedad
N=50
indep_vars = np.random.uniform(size = N, low = 0, high = 10)
r = np.random.normal(size = N, loc = 0.0, scale = 8.0) # residuos
dep_vars = 2 + 3*indep_vars + 2*indep_vars**2 + r # relación cuadrática
#%%Fiteos

def pot(x,n):
    X=x.reshape(-1,1)
    for i in range(n-1):
        X=np.concatenate((X,(x**(i+2)).reshape(-1,1)),axis=1)
    return X
def AIC(k, ecm, num_params):
    '''Calcula el AIC de una regresión lineal múltiple de 'num_params' parámetros, ajustada sobre una muestra de 'k' elementos, y que da lugar a un error cuadrático medio 'ecm'.'''
    aic = k * np.log(ecm) + 2 * num_params
    return aic

def datasets(X,P):
    return [pot(X,i) for i in range(P)]

modelo = linear_model.LinearRegression()

def fiteos(x,y, modelo, N):
    lista = datasets(x,N)
    vector = np.zeros(len(lista))
    print('-------------------------')
    for nodo in lista:
        modelo.fit(nodo,dep_vars)
        yhat = modelo.predict(nodo)
        ECM = np.power(dep_vars - yhat, 2).mean()
        aic = AIC(nodo.shape[1],ECM, nodo.shape[1] + 1)
        vector[nodo.shape[1]] = aic
        print(f'Grado del Polinomio = {nodo.shape[1]} \n Cantidad de Parámetros: {nodo.shape[1] +1 } \n ECM: {ECM:0.3f} \n AIC = {aic:0.3f}' )
        print('-------------------------')
    return vector
MF = fiteos(indep_vars, dep_vars,modelo,20)
print(f'El grado que minimiza el polinomio es: {np.argmin(MF) + 1 }')