#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 22:41:42 2020

@author: gonik
"""

def pascal(n, k):
    if n == 0 or n == 1:
        return 1
    if k == 0 or k == n:
        return 1
    else:
        li = pascal(n-1, k - 1)
        ld = pascal(n-1, k )
        return li + ld
        
    