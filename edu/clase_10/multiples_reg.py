#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 01:59:23 2020

@author: gonik
"""
import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt

#%% Datasets
np.random.seed(3141) # semilla para fijar la aleatoriedad

N=50
indep_vars = np.random.uniform(size = N, low = 0, high = 10)
r = np.random.normal(size = N, loc = 0.0, scale = 8.0) # residuos
dep_vars = 2 + 3*indep_vars + 2*indep_vars**2 + r # relación cuadrática


x = indep_vars
xc = indep_vars **2
emp = lambda x: x.reshape(-1,1)
data = np.concatenate([emp(x),emp(xc)],axis = 1)
#%% Modelado
x = emp(x)
xc = emp(xc)
grillax =emp( np.linspace(0, 50,1000))

modelo = linear_model.LinearRegression()

modelo.fit(x, dep_vars)
yhat1 = modelo.predict(x)
ECM1 = np.power(dep_vars - yhat1,2).mean()
yg1 = modelo.predict(grillax)


modelo.fit(xc, dep_vars)
yhat2 = modelo.predict(xc)
ECM2 = np.power(dep_vars - yhat2,2).mean()
yg2 = modelo.predict(grillax**2)

modelo.fit(data, dep_vars)
yhat3 = modelo.predict(data)
ECM3 = np.power(dep_vars - yhat3,2).mean()
yg3 = modelo.predict(np.concatenate([grillax, grillax**2], axis = 1))

#%% Gráfico

plt.style.use('seaborn-pastel')
parameters = {'ytick.labelsize': 18,'xtick.labelsize': 18,'legend.fontsize' :22}
plt.rcParams.update(parameters)
fig = plt.figure(figsize = (16,12))
plt.scatter(x, dep_vars, c = 'blue', label = 'Datos')
plt.plot(grillax, yg1, c = 'red', label = rf'Modelo lineal, $y \sim  x $ , ECM = {ECM1:0.3f}')
plt.plot(grillax, yg2, c = 'purple', label = rf'Modelo lineal,  $y \sim x^2$ , ECM = {ECM2:0.3f}')
plt.plot(grillax, yg3, c = 'green', label = rf'Modelo lineal,  $y \sim x^2 + x $ , ECM = {ECM3:0.3f}')
plt.ylim((0,max(dep_vars) + 20))
plt.xlim((0, max(indep_vars + 2)))
plt.legend()
plt.show()