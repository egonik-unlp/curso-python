#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 21:22:36 2020

@author: gonik
"""



def maximo(lista):
    if len(lista) == 1:
        maxi =  lista[0]
    else:
        maxi = maximo(lista[1:])
        if lista[1] > maxi:
            maxi = lista[1]
        else:
            maxi = maximo(lista[1:])
    return maxi
        
q = [2, 4, 5, 3, 13, 6, 19, 3, 17, 17, 2, 8]
R = maximo(q)
