#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 16:17:21 2020

@author: gonik
"""

def posiciones_de(a,b):
    LC = len(a)
    LS = len(b)
    if LS > LC:
        lista = []
    else:
        lista = posiciones_de(a[:-1], b) 
        if a[-LS:] == b:
            lista.append(LC - LS) 
    return lista
    
    
posiciones_de('Un tete a tete con Tete', 'te')
