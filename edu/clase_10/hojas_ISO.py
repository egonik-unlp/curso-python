#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 00:04:26 2020

@author: gonik
"""

def hoja_a(n):
    if n == 0:
        dim =  (841,1189)
    else:
        if n % 2  == 1:
            dim = (hoja_a(n-1)[1]//2,hoja_a(n-1)[0])
        elif n % 2 ==0:
            dim = (hoja_a(n-1)[0], hoja_a(n-1)[1]//2)
    return dim
    
a4 = hoja_a(6)
