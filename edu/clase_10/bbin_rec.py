#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 12:58:31 2020

@author: rgrimson
"""

def bbinaria_rec(lista, e):
    if len(lista) == 0:
        res = False
    elif len(lista) == 1:
        res = lista[0] == e
    else:
        medio = len(lista)//2
        
        
        if e == lista[medio]:
            res = True

        elif e > lista[medio]:
            res = bbinaria_rec(lista[medio:], e)
        elif e < lista[medio]:
            res = bbinaria_rec(lista[:medio], e)
        
    return res

for J in range(35):
    print(J, bbinaria_rec([i for i in range(20)], J))