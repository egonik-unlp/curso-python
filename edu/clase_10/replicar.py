#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 21:52:09 2020

@author: gonik
"""

def replicar(lista, n):
    if not lista:
        nlista = []
    else:
        nlista = replicar(lista[:-1],n)
        for i in range(n):
            nlista.append(lista[-1])
        
    return nlista
    