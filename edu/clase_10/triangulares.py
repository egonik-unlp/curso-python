#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 00:40:38 2020

@author: gonik
"""

def rec_triang(n,k = 1, suma = 0):
    """
    Precondición: n > 0
    Devuelve: sumatoria entre 1 y n
    """
    res = 0
    if k < n:
     res = k + rec_triang(n, k + 1)
    return res