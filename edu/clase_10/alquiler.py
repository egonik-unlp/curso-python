#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 23:21:35 2020

@author: gonik
"""
import matplotlib.pyplot as plt
import numpy as np



def ajuste_lineal_simple(x,y):
    a = sum(((x - x.mean())*(y-y.mean()))) / sum(((x-x.mean())**2))
    b = y.mean() - a*x.mean()
    return a, b

superficie = np.array([150.0, 120.0, 170.0, 80.0])
alquiler = np.array([35.0, 29.6, 37.4, 21.0])

plt.scatter(superficie,alquiler)
x = np.linspace(superficie.min(), superficie.max(), 100)
a, b = ajuste_lineal_simple(superficie, alquiler)
y = a*x + b
plt.plot(x,y)
plt.show()


errores = alquiler - (a*superficie + b)
ECM = np.mean(errores**2)

print('ECM', ECM)