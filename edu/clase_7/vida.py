#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
import sys


def main (string_fecha, futuro = False):
    t_object = datetime.strptime(string_fecha,'%d/%m/%Y')
    hoy_ahora = datetime.now()
    td = hoy_ahora - t_object 
    if futuro:
        td = -td
    return td


def imprimir(dateobject):
    print(f'Llevo {dateobject.total_seconds():0.2f} segundos de vida')

if __name__ == '__main__' :
    if len(sys.argv) != 2:
	    print('Uso inorrecto, invocar funcion agregando string de fdn')
    else:
        imprimir(main(sys.argv[1]))

