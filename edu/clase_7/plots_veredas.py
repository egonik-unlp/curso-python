#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 21:48:24 2020

@author: gonik
"""
import pandas as pd
import numpy as np
import os 
import seaborn as sns

os.chdir('/home/gonik/Documents/python_unsam/Ejercicios/Data')


cols_sel = ['nombre_cientifico', 'ancho_acera', 'diametro_altura_pecho', 'altura_arbol']
especies_seleccionadas = ['Tilia x moltkei', 'Jacaranda mimosifolia', 'Tipuana tipu']

df = pd.read_csv('arbolado-publico-lineal-2017-2018.csv')
df_cols = df[cols_sel]
df_sf = df_cols[df_cols.nombre_cientifico.isin(especies_seleccionadas)]



df_sf.boxplot('diametro_altura_pecho', by = 'nombre_cientifico')
df_sf.boxplot('altura_arbol', by = 'nombre_cientifico')


sns.pairplot(data = df_sf, hue = 'nombre_cientifico')
