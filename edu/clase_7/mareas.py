#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 12:57:06 2020

@author: gonik
"""

import matplotlib.pyplot as plt
import seaborn as sns
from scipy import signal
import pandas as pd
import numpy as np
import os

directorio = '/home/gonik/Documents/python_unsam/Ejercicios/Data'
archivo = 'OBS_SHN_SF-BA.csv'

path = os.path.join(directorio,archivo)

df = pd.read_csv(path, index_col= ['Time'], parse_dates = True)

#df['12-25-2014':].plot()
#df['10-15-2014':'12-15-2014'].plot()
dh = df['12-25-2014':].copy()

delta_t = -1 # tiempo que tarda la marea entre ambos puertos
delta_h = np.mean(X.T.H_SF - X.T.H_BA) # diferencia de los ceros de escala entre ambos puertos
pd.DataFrame([dh['H_SF'].shift(delta_t) - delta_h, dh['H_BA']]).T.plot() 
R = pd.DataFrame([dh['H_SF'].shift(delta_t) - delta_h, dh['H_BA']]).T