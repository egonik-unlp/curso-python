#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 23:14:08 2020

@author: gonik
"""

import datetime

feriados = ['12/10/2020', '23/11/2020', '7/12/2020', '8/12/2020', '25/12/2020']
def dias_habiles(inicio, fin, feriados):
    fechar = lambda x: datetime.datetime.strptime(x,'%d/%m/%Y')
    inicio = fechar(inicio)
    fin = fechar(fin)
    feriados = [fechar(feriado) for feriado in feriados]
    dia = datetime.timedelta(days = 1)
    hoy = inicio
    habiles = []
    while hoy <= fin:
        if hoy not in feriados and hoy.weekday() != 0 and hoy.weekday() != 6:
            habiles.append(hoy)
        hoy += dia
    return habiles
