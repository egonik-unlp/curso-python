#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 20:30:36 2020

@author: gonik
"""
import sys
import os

def recorrer(folder):
    for root, dirs, files in os.walk(folder):
        for name in files:
            if name[-3:] == 'png':
                print(name)

if __name__ == '__main__':
    if len(sys.argv) != 2 :
        print(f'Uso incorrecto. Uso correcto {sys.argv[0]} "path/a/explorar"')
    else:
        recorrer(sys.argv[1])