#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 18:14:02 2020

@author: gonik
"""
import vida
from datetime import datetime


def dias_primavera():
    current_spring = '21/09/' + datetime.now().strftime('%Y')
    dif = vida.main(current_spring, futuro = True).days
    if dif < 0:
        dif +=365     
    print(f'Faltan {dif} días para que empiece la proxima primavera')



if __name__ == '__main__' :
    dias_primavera()
