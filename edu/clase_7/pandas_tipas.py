#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 22:23:17 2020

@author: gonik
"""
import os
import pandas as pd
import numpy as np


directorio = '/home/gonik/Documents/python_unsam/Ejercicios/Data'
archivo_veredas = 'arbolado-publico-lineal-2017-2018.csv'
archivo_parques = 'arbolado-en-espacios-verdes.csv'
path_veredas = os.path.join(directorio, archivo_veredas)
path_parques = os.path.join(directorio, archivo_parques)

df_parques = pd.read_csv(path_parques)
df_veredas = pd.read_csv(path_veredas)

sel_p = ['nombre_cie', 'altura_tot', 'diametro']
sel_v = ['nombre_cientifico','altura_arbol', 'diametro_altura_pecho']

df_p_mezza = df_parques[df_parques.nombre_cie == 'Tipuana Tipu'].copy()
df_v_mezza = df_veredas[df_veredas.nombre_cientifico == 'Tipuana tipu'].copy()
df_tipas_veredas = df_v_mezza[sel_v].copy()
df_tipas_veredas['ambiente'] = 'vereda'
df_tipas_veredas.rename(columns = {'nombre_cientifico':'nombre_cientifico','altura_arbol':'altura', 'diametro_altura_pecho':'diametro_al_pecho'},inplace = True)
df_tipas_parques = df_p_mezza[sel_p].copy()
df_tipas_parques['ambiente'] = 'parque'
df_tipas_parques.rename(columns = {'nombre_cie':'nombre_cientifico','altura_tot':'altura', 'diametro':'diametro_al_pecho'},inplace = True)

df_tipas = pd.concat([df_tipas_veredas, df_tipas_parques])


df_tipas.boxplot('diametro_al_pecho',by = 'ambiente')
df_tipas.boxplot('altura',by = 'ambiente')