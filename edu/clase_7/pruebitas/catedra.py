#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 20:00:15 2020

@author: gonik
"""
from google.colab import drive
drive.mount('/content/drive')
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import preprocessing





directorio = '/home/gonik/Documents/catedra'
archivo = 'calificaciones_catedra.csv'
grupos = 'grupos2.csv'

path = os.path.join(directorio, archivo)
path_grupos = os.path.join(directorio, grupos)


df = pd.read_csv(path)
df_grupos = pd.read_csv(path_grupos)

df_grupos.rename(columns = {'Apellido(s)':'Apellidos'}, inplace = True)
df.rename(columns = {'Apellido(s)':'Apellidos'}, inplace = True)

diccionario = {apellido:grupo for apellido, grupo in zip(df_grupos['Apellidos'],df_grupos['Grupo']) }
select = df.columns.to_list()
eliminar = ['Número de ID', 'Institución', 'Departamento','Dirección de correo', 'Última descarga de este curso']
for i in eliminar:
    select.remove(i)
df_paso1 = df[select].copy()
df_paso1['Grupo'] = 0
for i in range(len(df_paso1.Grupo)):
    df_paso1['Grupo'][i] = diccionario[df_paso1['Apellidos'][i]]
df_final = df_paso1.copy()    

select2 = ['Apellidos','Grupo','Cuestionario:Cuestionario Control 1 (Real)','Cuestionario:Evaluación de Control (Real)', 'Total del curso (Real)']
df_ec = df_final[select2].copy()
df_ec.rename(columns = {'Cuestionario:Cuestionario Control 1 (Real)':'ccontrol1', 'Cuestionario:Evaluación de Control (Real)': 'ccontrol2','Total del curso (Real)': 'nota_total'}, inplace = True)

df_ec.replace('-',int(0), inplace = True)
df_ec.replace('D?Angelo', 'Dangelo', inplace = True)
diccionario['Dangelo'] = 'Grupo A'
a_num = ['ccontrol1', 'ccontrol2', 'nota_total']

for i in a_num:
    df_ec[i] = pd.to_numeric(df_ec[i])
sns.set_theme(style="darkgrid")

fig, ax = plt.subplots(nrows = 3, figsize = (20,15))
sns.boxplot(x = df_ec['ccontrol1'], hue = df_ec['Grupo'], ax= ax[0])
sns.boxplot(x = df_ec['ccontrol2'], hue = df_ec['Grupo'], ax= ax[1])
ax[1].set_xlabel('esteplot')
sns.kdeplot(data=iris, ax = ax[2])
plt.show()

sns.pairplot(data = df_ec[df_ec.nota_total <= 300], hue = 'Grupo')
#%%
#Analisis 
x = df_ec[['ccontrol1', 'ccontrol2', 'nota_total']].values
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
df_norm = pd.DataFrame(x_scaled)
df_ecn = pd.concat([df_ec['Apellidos'], df_norm], axis = 1)
df_ecn.rename(columns = {0:'ccontrol1',1:'ccontrol2',2:'nota_total'}, inplace  = True)
df_ecn_g = df_ecn.copy()
df_ecn_g['Grupo'] = 0

for i in range(len(df_ecn_g)):
    df_ecn_g['Grupo'][i] = diccionario[df_ecn_g['Apellidos'][i]]