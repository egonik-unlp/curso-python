#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 23:08:29 2020

@author: gonik
"""
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import signal
import pandas as pd
import numpy as np
import os

directorio = '/home/gonik/Documents/python_unsam/Ejercicios/Data'
archivo = 'OBS_SHN_SF-BA.csv'

path = os.path.join(directorio,archivo)

df = pd.read_csv(path, index_col= ['Time'], parse_dates = True)

#df['12-25-2014':].plot()
#df['10-15-2014':'12-15-2014'].plot()
dh = df['12-25-2014':].copy()

delta_t = -1 # tiempo que tarda la marea entre ambos puertos
delta_h = np.mean(X.T.H_SF - X.T.H_BA) # diferencia de los ceros de escala entre ambos puertos
pd.DataFrame([dh['H_SF'].shift(delta_t) - delta_h, dh['H_BA']]).T.plot() 
R = pd.DataFrame([dh['H_SF'].shift(delta_t) - delta_h, dh['H_BA']]).T

inicio = '2014-01'
fin = '2014-06'
alturas_sf = df[inicio:fin]['H_SF'].to_numpy()
alturas_ba = df[inicio:fin]['H_BA'].to_numpy()

def calcular_fft(y, freq_sampleo = 24.0):
    '''y debe ser un vector con números reales
    representando datos de una serie temporal.
    freq_sampleo está seteado para considerar 24 datos por unidad.
    Devuelve dos vectores, uno de frecuencias 
    y otro con la transformada propiamente.
    La transformada contiene los valores complejos
    que se corresponden con respectivas frecuencias.'''
    N = len(y)
    freq = np.fft.fftfreq(N, d = 1/freq_sampleo)[:N//2]
    tran = (np.fft.fft(y)/N)[:N//2]
    return freq, tran

freq_sf, fft_sf = calcular_fft(alturas_sf)
freq_sf[0]
peaks_sf = signal.find_peaks(np.abs(fft_sf), prominence = 8)
frec_sf_per  = freq_sf[peaks_sf[0][0]]


#%%
plt.plot(freq_sf, np.abs(fft_sf))
plt.xlabel("Frecuencia")
plt.ylabel("Potencia (energía)")
plt.xlim(0,4)
plt.ylim(0,20)
# me quedo solo con el último pico
pico_sf = signal.find_peaks(np.abs(fft_sf), prominence = 8)[0][-1]
# es el pico a analizar, el de la onda de mareas
# marco ese pico con un circulito rojo
plt.scatter(freq_sf[pico_sf], np.abs(fft_sf)[pico_sf], facecolor = 'r')
plt.show()


#%%
fig,ax = plt.subplots(2,1, figsize = (20,12))

ax[0].plot(R)
ax[0].set_title('Alturas de Marea en puertos de SF y BA', fontsize = 24)
ax[0].set_xlabel("altura de marea", fontsize = 18)
ax[0].set_ylabel("Tiempo (dias)", fontsize = 18)
ax[1].plot(freq_sf[1:], np.abs(fft_sf[1:]))
ax[1].set_title('Transformada de Fourier en puerto SF', fontsize = 24)
ax[1].set_xlabel("Frecuencia", fontsize = 18)
ax[1].set_ylabel("Potencia (energía)", fontsize = 18)
plt.tight_layout()
plt.show()