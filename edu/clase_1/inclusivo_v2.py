#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 23:23:21 2020

@author: gonik
"""




frase = str(input('ingresa una frase = '))
frase_t = ''
palabras = frase.split()

for palabra in palabras:
    if len(palabra) > 1:
        if palabra[-2] == 'o' or palabra[-2] == 'a':
            frase_t+=palabra[0:-2] + 'e' + palabra[-1:]+' '
        elif palabra[-1] == 'o' or palabra[-1] =='a': 
            frase_t+=palabra[0:-1] + 'e' + ' '
        else:
            frase_t+=palabra +' '
    else:
        frase_t+=palabra +' '        
print(frase_t)