#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 01:25:05 2020

@author: gonik
"""

import sys
import os



os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
sys.path.append('/home/gonik/Documents/repo_curso/curso_python/edu')


from clase_9.vigilante import vigilar
from clase_9 import  informe_poo as informe
from clase_8.lote import Lote
from clase_8 import formato_tabla 
import csv



def parsear_datos(lines,headers):
    
    rows = csv.reader(lines)
    rows = ([row[index] for index in [0, 1, 2]] for row in rows)
    #rows = ([func(val) for func, val in zip([str, float, float],row)] for row in rows)
    rows = (dict(zip(headers,row)) for row in rows)
    return rows


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Uso incorrecto')
    else:
        headers = ["nombre", "precio", "volumen"]
        lines = vigilar(sys.argv[1])
        names = informe.leer_camion(sys.argv[2])
        rows = parsear_datos(lines,headers)
        rows = (row for row in rows if row['nombre'] in names)
        formateador = formato_tabla.crear_formateador(sys.argv[3])
        formateador.headings(headers)
        for row in rows:
            formateador.row(row.values())

    