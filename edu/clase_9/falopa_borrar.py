#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 01:20:02 2020

@author: gonik
"""

os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
from clase_9.vigilante import vigilar
import csv


def filematch(lines, substr):
        for line in lines:
            if substr in line:
                yield line
                
lineas = vigilar('Data/mercadolog.csv')
filas = csv.reader(lineas)
for fila in filas:
    print(fila)

