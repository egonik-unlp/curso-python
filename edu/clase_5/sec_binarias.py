#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 19:20:27 2020

@author: gonik
"""

def incrementar(s):
    carry = 1
    l = len(s)
    
    for i in range(l-1,-1,-1):
        if (s[i] == 1 and carry == 1):
            s[i] = 0
            carry = 1
        else:
            s[i] = s[i] + carry
            carry = 0
    return s

def todas(len_s):
    inicial = [0 for i in range(len_s)]
    final = [1 for i in range(len_s)]
    res = inicial
    lista = []
    lista.append(res)
    while res != final:
        res = incrementar(res)
        lista.append(res.copy())
    return lista