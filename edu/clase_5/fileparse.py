import csv

def parses_csv(nombre_archivo, select = None, types = [str,int,float], has_headers = True):
    '''
    Parsea un archivo CSV en una lista de registros
    '''
    with open(nombre_archivo) as f:
        rows = csv.reader(f)

        # Lee los encabezados
        if has_headers:
            
            headers = next(rows)
        
        if has_headers:    
            if select:
                indices = [headers.index(nombre_columna) for nombre_columna in select]
                headers = select
            else:
                indices = []
        else:
            indices = None
        
        registros = []
        for row in rows:
            if not row:    # Saltea filas sin datos
                continue
            if indices:
                row = [row[indice] for indice in indices]    
            if types:
                row = [func(val) for func, val in zip(types,row)]
            
            if has_headers: 
                registro = dict(zip(headers, row))
            
            else:
                registro = tuple(i for i in row)
                
            
            registros.append(registro)

    return registros
