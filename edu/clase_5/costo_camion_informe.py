# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import csv
import sys
import os

import informe_fileparse

def main(args)
    os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
    camion = informe_fileparse.leer_camion('Data/camion.csv')
    suma_total = sum([nodo['cajones']*nodo['precio'] for nodo in camion])
    print(f'El costo total es $ {suma_total:^.2f}')
    
    
if  __name__ == '__main__':
    if len(sys.argv) !=2:
        raise SystemExit(f'Uso adecuado: {sys.argv[0]} ' 'archivo_camion')
    else:
        main(sys.argv)