# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
def sumar_enteros(desde, hasta):
    '''Calcula la sumatoria de los números entre desde y hasta.
       Si hasta < desde, entonces devuelve cero.
    
    Pre: desde y hasta son números enteros
    Pos: Se devuelve el valor de sumar todos los números del intervalo
        [desde, hasta]. Si el intervalo es vacío se devuelve 0 '''
    ret = 0
    if hasta > desde:
        for i in range(desde,hasta):
            ret += i
    return ret + hasta

def sumar_enteros_tr(desde, hasta):
    tr_d = (desde*(desde + 1))/2
    tr_h = (hasta*(hasta + 1))/2
    return int(tr_h - tr_d + desde)
         

    