import csv
import gzip
import os

os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')

def parse_csv(iterable, select = None, types = [str,int,float], has_headers = True):
    '''
    Parsea un archivo CSV en una lista de registros
    '''
    rows = iter(iterable)

    # Lee los encabezados
    if has_headers:
        
        headers = next(rows).replace('\n','').split(',')
    
    if has_headers:    
        if select:
            indices = [headers.index(nombre_columna) for nombre_columna in select]
            headers = select
        else:
            indices = []
    else:
        indices = None
    
    registros = []
    for row in rows:
        row = row.replace('"', '').replace('\n','').split(',')
        if not row:    # Saltea filas sin datos
            continue
        if indices:
            row = [row[indice] for indice in indices]    
        if types:
            row = [func(val) for func, val in zip(types,row)]
        
        if has_headers: 
            registro = dict(zip(headers, row))
        
        else:
            registro = tuple(i for i in row)
            
        
        registros.append(registro)

    return registros

lines = gzip.open('Data/camion.csv.gz','rt')
camion = parse_csv(lines)
