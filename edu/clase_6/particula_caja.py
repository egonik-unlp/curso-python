#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 03:52:15 2020

@author: gonik
"""

import numpy as np
cuan = lambda x: (2*x-1)*np.sin(x**(1/2)) - 2*(x - x**2)**(1/2)*np.cos(x**(1/2)) 
x = np.linspace(0,1,100)
plt.plot(x,cuan(x))
plt.xlim((0,1))