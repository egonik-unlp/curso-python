#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 20:04:18 2020

@author: gonik
"""

import numpy as np
import matplotlib.pyplot as plt

def randomwalk(largo):
    pasos=np.random.randint (-1,2,largo)    
    return pasos.cumsum()

N = 100000
def trayectorias(largo,cantidad):
    return [randomwalk(largo) for i in range(cantidad)]



def desviadx(lista_trayectorias):
    max_global = 1e-30
    min_global = 1e30
    mas_desviada = False
    menos_desviada = False
    for vector in lista_trayectorias:
        v_abs = np.abs(vector)
        val_ac = sum(v_abs)
        if val_ac > max_global:
            mas_desviada = vector.copy()
            max_global = val_ac
        if val_ac < min_global:
            menos_desviada = vector.copy()
            min_global = val_ac
    return mas_desviada, menos_desviada


caminantes = trayectorias(N,12)
mas_desviada, menos_desviada = desviadx(caminantes)


fig, ax = plt.subplots(2,2, sharey = 'row' , figsize = (20,15), dpi = 120)
fig.subplots_adjust(wspace=0.05)
plt.subplot(211)
for i in caminantes:
    plt.plot(i)
plt.xlabel('tiempo', fontsize=18)
plt.title('12 caminatas al azar', fontsize = 24)
plt.ylabel('Distancia al origen', fontsize = 18)
plt.xticks(fontsize = 18)
plt.yticks(fontsize = 18)
escala = fig.gca().get_ylim()

ax[1,0].plot(mas_desviada)
ax[1,0].set_title('Trayectoria más desviada',fontsize = 24)
ax[1,0].set_ylabel('Distancia al origen', fontsize = 18)
ax[1,1].plot(menos_desviada)
ax[1,1].set_title('Trayectoria menos desviada', fontsize = 24)

for i in range(2):
    ax[1,i].set_xlabel('tiempo', fontsize=18)
    ax[1,i].set_ylim(escala)
    ax[1,i].tick_params(labelsize = 18)
    ax[1,i].tick_params(labelsize = 18)
    


plt.tight_layout()
plt.show()

