#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 19:33:09 2020

@author: gonik
"""
import sys
import fileparse
import os
import csv
os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')

def leer_camion(nombre_archivo):
    '''Computa el precio total del camion (cajones*precio) de un archivo'''
    with open(nombre_archivo) as file:
        camion = fileparse.parse_csv(file)
    return camion
    
    
def leer_precios(archivo_csv):
    with open(archivo_csv) as file:
        precios = dict(fileparse.parse_csv(file, types = [str,float], has_headers = False))
    return precios
            


def hacer_informe(d_costos, precios):
    return [(nodo['nombre'],nodo['cajones'],precios[nodo['nombre']], -(nodo['precio'] - precios[nodo['nombre']])  )for nodo in d_costos]
        
    
    
def calculos(arch_camion, arch_precios):    
    en_local = leer_camion(arch_camion)
    precios = leer_precios(arch_precios)
    informe = hacer_informe(en_local, precios)

    underscore = 10*'-'
    headers = ('Nombre', 'Cajones', 'Precio', 'Cambio')
    print(f'{headers[0]:<10s} {headers[1]:<10s} {headers[2]:<10s} {headers[3]:<10s}')
    print(f'{underscore:<10s} {underscore:<10s} {underscore:<10s} {underscore:<10s}') 
    for nombre, cajones, precio, vuelto in informe:
        print(f'{nombre:<10s} {cajones:<10d} {f"${precio:.2f}":>10s} {vuelto:>10.2f}')
   
def main(args):
    calculos(args[1],args[2])

if  __name__ == '__main__':
    if len(sys.argv) !=3:
        raise SystemExit(f'Uso adecuado: {sys.argv[0]} ' 'archivo_camion archivo_precios')
    else:
        main(sys.argv)

