#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 13 01:14:22 2020

@author: tom
"""

def valor_absoluto(n):
'''
Esta funcion calcula el valor absoluto del valor numerico que se ingrese
pre = se ingresa un valor numerico
ṕos = se devuelve el modulo de ese valor, sea int o float


'''
    if n >= 0: 
        return n
    else:
        return -n

def suma_pares(l):

'''
esta funcion recibe una lista y retorna la sumatoria de sus miembros pares
pre = se le entrega un objeto tipo lista
pos = se retorna el valor de la sum, de tipo igual a los miembros de la lista
inv = a lo largo de la ejecucion del programa, res almacena ell valor de la suma de los nros pares
'''
    res = 0
    for e in l:
        if e % 2 ==0:
            res += e
        else:
            res += 0

    return res




def veces(a, b):
"""
Esta función hace la sumatoria de a ,b veces (se podría decir que es un algoritmo para
calcular productos mediante sumas). 

pre = a y b deben ser numeros, b debe ser int
pos = se devuelve el resultado como un numero

inv = a lo largo de la ejecucion del programa se almacena el resultado parcial de la 
sumatoria en res.

"""


    res = 0
    nb = b
    while nb != 0:
        #print(nb * a + res)
        res += a
        nb -= 1
    return res



def collatz(n):
    """
    
    
    """
    res = 1

    while n!=1:
        if n % 2 == 0:
            n = n//2
        else:
            n = 3 * n + 1
        res += 1

    return res


q = collatz(20)