#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 16:59:23 2020

@author: gonik
"""
import numpy as np
import os
import csv
from collections import Counter as counter

x = 'Data/arbolado-en-espacios-verdes.csv'

os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')

def leer_parque(archivo, parque):
    with open(archivo, 'rt') as file:
        l_arboles = []
        rows = csv.reader(file)
        headers = next(rows)
        for row in rows:    
            record = dict(zip(headers,row))
            record['altura_tot'] = float(record['altura_tot'])
            record['inclinacio'] = float(record['inclinacio'])
            if record['espacio_ve'] == parque:
                l_arboles.append(record)
    return l_arboles

lista_gp = leer_parque(x,'ANDES, LOS')

def especies(lista):
    return set([nodo['nombre_com'] for nodo in lista])

# especies(leer_parque(x,'GENERAL PAZ'))

def cont_arboles(lista):
    c_arboles = counter()
    for item in lista:
        c_arboles[item['nombre_com']]+=1
    return c_arboles

def obtener_alturas(lista,especie):
    return [nodo['altura_tot'] for nodo in lista if nodo['nombre_com'] == especie]


def avg_lista(lista):
    return sum(lista)/len(lista)

def obtener_inclinaciones(lista, especie):
    return [nodo['inclinacio']for nodo in lista if nodo['nombre_com'] == especie]

def especimen_mas_inclinado(lista):
    l1 = [(nodo['inclinacio'],nodo['nombre_com']) for nodo in lista]
    return max(l1)


def especimen_mas_inclinado_promedio(lista):
    l1 = [(especie,obtener_inclinaciones(lista,especie)) for especie in especies(lista)] 
    l2 =[(avg_lista(nodo[1]), nodo[0]) for nodo in l1] 
    return max(l2)

#%% Clase 3


def leer_arboles(archivo):
    with open(archivo, 'rt') as file:
        rows = csv.reader(file)
        headers = next(rows) 
        lista = [dict(zip(headers,row)) for row in rows] 
    return lista


arboleda = leer_arboles(x)
def jacaranda_altos(l_d):
    d = 'diametro'
    at = 'altura_tot'
    j = 'Jacarandá'
    nc = 'nombre_com'
    return [(float(a[d]),float(a[at])) for a in l_d if a[nc] == j]

def arboles_altos(l_d, esp):
    d = 'diametro'
    at = 'altura_tot'
    nc = 'nombre_com'
    return [(float(a[d]),float(a[at])) for a in l_d if a[nc] == esp]

### tuve que abreviar porque no se como hacer para que python no me separe en dos lineas
lista_especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']

def medidas_de_especies(l_d, esp):
    d ={e: arboles_altos(l_d,e) for e in esp}
    
    return d 

#%%
import matplotlib.pyplot as plt
def hist_jac(arboleda):
        alturas_jac = [int(arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com'] == 'Jacarandá']
        plt.hist(alturas_jac, bins = 25)
        plt.xlabel('alturas de jacarandas en bsas (m)')
        plt.ylabel('numero de eventos')
        plt.show()

def scatter_jac(arboleda):
    alturas_jac = [int(arbol['altura_tot']) for arbol in arboleda if arbol['nombre_com'] == 'Jacarandá']
    diametro_jac = [int(arbol['diametro']) for arbol in arboleda if arbol['nombre_com'] == 'Jacarandá']
    plt.hist(alturas_jac, bins = 25)
    plt.xlabel('alturas de jacarandas en bsas (m)')
    plt.ylabel('Diámeto de los jacarandás en bsas( cm)')
    plt.show()

def medidas_tres_arboles(arboleda):
    especies = ['Eucalipto', 'Palo borracho rosado', 'Jacarandá']
    medidas = medidas_de_especies(especies, arboleda)
    for medida in medidas.values():
        d = [nodo[0] for nodo in medida]
        a = [nodo[1] for nodo in medida]
        plt.plot(d,a)
    plt.xlabel('prueba')
    plt.show()
        
        
