#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 20:45:12 2020

@author: gonik
"""

import numpy as np
import random as rd
import matplotlib.pyplot as plt

def crear_album(figus_total):
    return np.zeros(figus_total)


def album_incompleto(A):
    return 0 in A

def comprar_figu(figus_total):
    return rd.randint(0, figus_total)


def cuantas_figus(figus_total):
    album = crear_album(figus_total)
    compras = 0
    while album_incompleto(album):
        album[comprar_figu(figus_total)-1]+=1
        compras+=1
    return compras

def repeticiones(figus_total, rep):
    lista = [cuantas_figus(figus_total) for i in range(rep)]
    return np.mean(lista)

def comprar_paquete(figus_total, fig_pack):
    return np.array([rd.randint(0, figus_total-1) for i in range(fig_pack)])

def comprar_paquete_sr(figus_total, fig_pack):
    return np.array([rd.choices(figus_total, k = fig_pack)])


def cuantos_paquetes(figus_total, figus_paquete):
    album = crear_album(figus_total)
    compras = 0
    while album_incompleto(album):
        pack = comprar_paquete(figus_total, figus_paquete)
        album[pack]+=1
        compras+=1
    return compras


def cuantos_paquetes_sr(figus_total, figus_paquete):
    album = crear_album(figus_total)
    compras = 0
    while album_incompleto(album):
        pack = comprar_paquete(figus_total, figus_paquete)
        album[pack]+=1
        compras+=1
    return compras


def repeticiones_paquetes(figus_total, figus_paquete,rep):
    lista = [cuantos_paquetes(figus_total, figus_paquete) for i in range(rep)]
    return np.array(lista)

def repeticiones_paquetes_sr(figus_total, figus_paquete,rep):
    lista = [cuantos_paquetes_sr(figus_total, figus_paquete) for i in range(rep)]
    return np.array(lista)


def prob_850(v):
    return len(v[v<=850])/len(v)


def chances_90(v):
    v.sort()
    return v[int(0.9*len(v))]


def cinco_amigues(figus_total,figus_paquete,rep):
    return np.array([cuantos_paquetes(figus_total *5, figus_paquete)/5 for i in range(rep)])

                     

                     
vector = repeticiones_paquetes (670,5,1000)
vector_sr = repeticiones_paquetes_sr(670,5,1000)
vector_amigues = cinco_amigues(670,5,100)
plt.hist(vector, bins = 35) 
chances = chances_90(vector)
chances_sr = chances_90(vector_sr)

prob850 = prob_850(vector)*100
print(f'hay {prob580}% de probabilidades de llenar el album con menos de 850 figus')
print(f'para tener un 90% de chances hay que comprar {chances} paquetes, con repes')
print(f'para tener un 90% de chances hay que comprar {chances_sr} paquetes, sin repes')
