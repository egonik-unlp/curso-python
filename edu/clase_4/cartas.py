#!/usr/bin/env pythons
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 21:29:37 2020

@author: gonik
"""
import random as rd
import numpy as np
from itertools import combinations


numeros = np.arange(1,13)
palos = ['basto', 'espada','oro', 'copa']
mazo = [(num, palo) for num in numeros for palo in palos]



def vaciar_mazo(lista):
    mazo = lista.copy()
    manos = []
    while len(mazo) > 3:
        mano = rd.sample(mazo,3)
        manos.append(mano)
    return manos


def envido(manos):
    for mano in manos:
        pos = combinations(mano)
    return pos


comb = envido(vaciar_mazo(mazo))
for i in comb:
    prin