#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 16:45:47 2020

@author: gonik
"""
import random as rd
import sys
import numpy as np
from collections import Counter as counter


def palos(tupla):
    return tupla[1]

def barajar(baraja):
    mano = rd.sample(baraja, k =3)
    mano.sort(key = palos)
    return mano

def puntos(sel_mano):
    tp = {i:(i if i< 8 else 0) for i in range(1,13)}
    return [tp[nodo] for nodo in sel_mano]

def mas_frecuente(mano):
    aux = counter()
    for i in mano:
        aux[i[1]]+=1
    return aux.most_common(1)[0]

def tanto(mano):
    falopa = False
    mf = mas_frecuente(mano)
    if mf[1] == 3:
        aux = puntos([nodo[0] for nodo in mano])
        aux.sort(reverse = True)
    elif mf[1] == 2:
         aux = puntos([i[0] for i in mano if i[1] == mf[0]])
    else:
        aux = max(puntos([nodo[0] for nodo in mano]))
        falopa = True
    if falopa:
        puntaje = aux
    else:
        puntaje = 20 + sum(aux)
    return puntaje

def envido(baraja):
    mano = barajar(baraja)
    punt = tanto(mano)
    return punt in [31,32,33]
    
def main(n):
    valores = [1,2,3,4,5,6,7,10,11,12]
    palos = ('oro', 'basto','espada', 'copas' )
    baraja = [(n,p) for n in valores for p in palos]
    exitos = sum([envido(baraja) for i in range(n)]) 
    print(f'Hice {n} tiradas, en las cuales saque 31,32 o 33 puntos de envido en {exitos} oportunidades. Esto me da una probabilidad de {exitos/n*100:0.4f}%')
    return exitos/n*100

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('uso incorrecto, debe ingresar el n de repeticiones. uso correcto= envido.py n, siendo "n" el nro de repeticiones')
    else:
        main(int(sys.argv[1]))
