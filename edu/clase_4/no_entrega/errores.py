def propagar_al_vecino(l):
    modif = False
    n = len(l)
    for i,e in enumerate(l):
        if e==1 and i<n-1 and l[i+1]==0:
            l[i+1] = 1
            modif = True
        if e==1 and i>0 and l[i-1]==0:
            l[i-1] = 1
            modif = True
    return modif

def propagar(l):
    m = l.copy()
    veces=0
    while propagar_al_vecino(l):
        veces += 1

    print(f"Repetí {veces} veces la función propagar_al_vecino.")
    print(f"Con input {m}")    
    print(f"Y obtuve  {l}")
    return m
#%%
propagar([0,0,0,0,1])
propagar([0,0,1,0,0])
propagar([1,0,0,0,0])



"""
Me da la impresion que este es O 2


1. Por la condicion i<n-1. No evalúa el ultimo lugar. En el segundo if i>0, asi que no se evalúa i>0

2. Si bien las listas son simétricas, la forma de recorrer de la función no lo es. El sentido de recorrid
siempre es izq -> der (la propagación es para ambos lado). Lo anterior ocaciona que si la propagación se hiciese
de der -> izq se necesitaría una recorrida por cada propagación.
3.
    a. (n-1)
    b. 2n?
    c.  cuadrática (creo) 2n(n-1)
    
"""
#%%


def propagar_a_derecha(l):
    n = len(l)
    for i,e in enumerate(l):
        if e==1 and i<n-1:
            if l[i+1]==0:
                l[i+1] = 1
    return l
#%
def propagar_a_izquierda(l):
    return propagar_a_derecha(l[::-1])[::-1]
#%
def propagar(l):
    ld=propagar_a_derecha(l)
    lp = propagar_a_izquierda(ld)
    return lp
#%%
l = [0,0,0,-1,1,0,0,0,-1,0,1,0,0]
print("Estado original:  ",l)
print("Porpagando...")
lp=propagar(l)
print("Estado original:  ",l)
print("Estado propagado: ",lp)


"""
1. Porque no se hizo una copia de l. Además como no se cambió el nombre de la variable, l pasó a ser 
una variable de alcance global.
2. Porque l solo se usó para propagar hacia la derecha, para propagar hacia la izq, se uso ld, que es copia
de l
3.
def propagar_a_derecha(la <- cambio1):
    l = l.copy() <- cambio2
    n = len(l)
    for i,e in enumerate(l)

"""

#%%

def trad2s(l):
    '''traduce una lista con 1,0 y -1 
    a una cadena con 'f', 'o' y 'x' '''
    d={1:'f', 0 :'o', -1:'x'}
    s=''.join([d[c] for c in l])
    return s

def trad2l(ps):
    '''traduce cadena con 'f', 'o' y 'x'
    a una lista con 1,0 y -1'''
    inv_d={'f':1, 'o':0, 'x':-1}
    l = [inv_d[c] for c in ps]
    return l

def propagar(l, debug = True):
    s = trad2s(l)
    if debug:
        print(s)#, end = ' -> ')
    W=s.split('x')
    PW=[w if ('f' not in w) else 'f'*len(w) for w in W]
    ps=''.join(PW)
    if debug:
        print(ps)
    return trad2l(ps)

#%%
l = [0,0,0,-1,1,0,0,0,-1,0,1,0,0]
lp = propagar(l)
print("Estado original:  ",l)
print("Estado propagado: ",lp)

"""

1. Porque al separar el string en los distintos sub-strings, se separan por la 'x' que es omitida

2.
    PW=[w if ('f' not in w) else 'f'*len(w) for w in W]
    ps='x'.join(PW) #<- Agregué una 'x' en la unión de los strings
    if debug:
3. El algoritmo es lineal Por que? COMPLETAR


"""