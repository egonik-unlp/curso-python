#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 14:12:10 2020

@author: gonik
"""
import datetime
import random as rd
from collections import Counter as counter


def tirar(n):
    l = []
    for i in range(n):
        l.append(rd.randint(1,6))
    return l

    

def tirada_generala_ns(n):
    l1 = tirar(n)
    nums = counter()
    for i in l1:
        nums[i]+=1
    buenas = [i for i in nums.items() if i[1] > 1]
    if len(buenas) > 1:
        tirada = buenas[rd.randint(0,1)]
    elif len(buenas) == 1:
        tirada = buenas[0]
    else:
        #print('tooy a qui', len(buenas))
        tirada = False
    return tirada


def generala():
    l = []
    for i in range(3):
        if len(l) == 0:       
            x = tirada_generala_ns(5)
            #print(x)
            if x != False:
                l1 = [x[0] for i in range(x[1])]
                l.extend(l1)
        else:
            if (5 - len(l)) > 0:
                x = tirada_generala_ns(5- len(l))
                if x != False:
                    l1 = [x[0] for i in range(x[1])]
                    if l1[0] == l[0]:
                        l.extend(l1)
           
        
    return l

def es_generala(l):
    if len(l) != 0:
        return sum(l)/len(l) == l[0] and len(l) == 5
    else:
        return False

N = 1000000
ini  = datetime.datetime.now()
G = sum([es_generala(generala()) for i in range(N)])
prob = G/N
print(f'Tiré {N} veces, de las cuales {G} saqué generala.')
print(f'Podemos estimar la probabilidad de sacar generala  mediante {prob:.6f}.')
fin = datetime.datetime.now()


long1 = fin - ini



        