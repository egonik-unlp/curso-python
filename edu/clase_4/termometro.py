#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 19:22:07 2020

@author: gonik
"""

import random as rd

def sim_termo(n):
    
    return [rd.normalvariate(0,0.2) + 37.5  for i in range(n)]

termometro = sim_termo(99)
termometro.sort()
maximo = max(termometro)
minimo = min(termometro)
mediana = termometro[49]
promedio = sum(termometro)/len(termometro)

print (f'el valor máximo es {maximo:0.2f}, el mínimo es {minimo:0.2f}, el valor promedio es {promedio:0.2f} y la mediana es {mediana:0.2f}')

