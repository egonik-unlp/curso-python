#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 20:50:05 2020

@author: gonik
"""


class FormatoTabla:
    
    
    def headings(headers):
        '''
        Crea el encabezado de la tabla.
        '''
    pass

    def row(rowdata):
        '''
        Crea una única fila de datos de la tabla.
        '''
    pass


def crear_formateador(nombre):
    if nombre == 'txt':
        formateador = FormatoTablaTXT()
    elif nombre == 'csv':
        formateador = FormatoTablaCSV()
    elif nombre == 'html':
        formateador = FormatoTablaHTML()
    else:
        raise RuntimeError(f'Unknown format {nombre}')
    return formateador            
def imprimir_tabla(camion, columnas, formateador):
    formateador.headings(columnas)
    for nodo in camion:
        formateador.row([str(getattr(nodo, columna)) for columna in columnas])
       
        
       
    
    
    
class FormatoTablaTXT(FormatoTabla):
    '''
    Generar una tabla en formato TXT
    '''
    def headings(self, headers):
        for h in headers:
            print(f'{h:>10s}', end=' ')
        print()
        print(('-'*10 + ' ')*len(headers))

    def row(self, data_fila):
        for d in data_fila:
            print(f'{d:>10s}', end=' ')
        print()
        
        
class FormatoTablaCSV(FormatoTabla):
    '''
    Generar una tabla en formato CSV
    '''
    def headings(self, headers):
        print(','.join(headers))

    def row(self, data_fila):
        print(','.join(data_fila))
        
        
class FormatoTablaHTML(FormatoTabla):
    '''
    Generar una tabla en formato CSV
    '''
    def headings(self, headers):
        print('<tr><th>','</th><th>'.join(headers),'</tr></th>',sep = '')

    def row(self, data_fila):
        print('<tr><td>','</td><td>'.join(data_fila),'</td></tr>', sep = '')        
        
