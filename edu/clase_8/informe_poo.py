#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 19:33:09 2020

@author: gonik
"""
import sys
import os
import csv
os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
sys.path.append('/home/gonik/Documents/repo_curso/curso_python/edu/')
from clase_6 import fileparse
from clase_8 import lote #podria prescindir del from (misma carpeta)
from clase_8 import formato_tabla

def leer_camion(nombre_archivo):
    '''Computa el precio total del camion (cajones*precio) de un archivo'''
    with open(nombre_archivo) as file:
        camion = [lote.Lote(d['nombre'],d['cajones'],d['precio']) for d in  fileparse.parse_csv(file)]
    return camion
    
    
def leer_precios(archivo_csv):
    with open(archivo_csv) as file:
        precios = dict(fileparse.parse_csv(file, types = [str,float], has_headers = False))
    return precios
            


def hacer_informe(d_costos, precios):
    return [(c.nombre,c.cajones,precios[c.nombre], -(c.precio - precios[c.nombre])  )for c in d_costos]
        


def informe_camion(archivo_camion, archivo_precios, fmt = 'txt'):
    '''
    Crea un informe a partir de un archivo de camión
    y otro de precios de venta.
    '''
    # Leer archivos con datos
    camion = leer_camion(archivo_camion)
    precios = leer_precios(archivo_precios)

    # Crear los datos para el informe
    data_informe = hacer_informe(camion, precios)

    # Imprimir el informe
    formateador = formato_tabla.crear_formateador(fmt)
    imprimir_informe(data_informe, formateador)







def imprimir_informe(data_informe, formateador):
    '''
    Imprime una tabla prolija desde una lista de tuplas
    con (nombre, cajones, precio, diferencia) 
    '''
    formateador.headings(['Nombre', 'Cantidad', 'Precio', 'Cambio'])
    for nombre, cajones, precio, cambio in data_informe:
        rowdata = [ nombre, str(cajones), f'{precio:0.2f}', f'{cambio:0.2f}' ]
        formateador.row(rowdata)


def main(camion,precios,formato = 'txt'):
    informe_camion(camion, precios,formato)

if  __name__ == '__main__':
    if len(sys.argv) <3:
        raise SystemExit(f'Uso adecuado: {sys.argv[0]} ' 'archivo_camion archivo_precios' 'formato de output')
    elif len(sys.argv) == 3:
        main(sys.argv[1],sys.argv[2])
    elif len(sys.argv) == 4:
        main(sys.argv[1],sys.argv[2],sys.argv[3])
    else:
        raise SystemExit(f'Uso adecuado: {sys.argv[0]} ' 'archivo_camion archivo_precios' 'formato de output')


