#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 02:34:21 2020

@author: gonik
"""

class Punto():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f'({self.x}, {self.y})'

    # Used with `repr()`
    def __repr__(self):
        return f'Punto({self.x}, {self.y})'
    def __add__(self, b):
      return Punto(self.x + b.x, self.y + b.y)

class Rectangulo(Punto):
    def __init__(self,pto1, pto2):
        self.pto1 = pto1
        self.pto2 = pto2
        self.recta1 = abs(pto2.x - pto1.x)
        self.recta2 = abs(pto2.y - pto1.y)
    def altura(self):
        return self.recta2
    def base(self):
        return self.recta1
    def area(self):
        return self.recta1*self.recta2
    def __str__(self):
        return(f'{self.pto1},{self.pto2}')
    def __repr__(self):
        return(f'Rectangulo(Punto{self.pto1},Punto{self.pto2})')
    def desplazar(self, desplazamiento):
        self.pto1 += desplazamiento
        self.pto2 += desplazamiento
    def rotar(self):
        self.pto2.y = - self.pto2.x
        self.pto2.x = self.pto2.y