#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 04:31:50 2020

@author: gonik
"""

class Cola:
    '''Representa a una cola, con operaciones de encolar y desencolar.
    El primero en ser encolado es tambien el primero en ser desencolado.
    '''

    def __init__(self):
        '''Crea una cola vacia.'''
        self.items = []

    def encolar(self, x):
        '''Encola el elemento x.'''
        self.items.append(x)

    def desencolar(self):
        '''Elimina el primer elemento de la cola 
        y devuelve su valor. 
        Si la cola esta vacia, levanta ValueError.'''
        if self.esta_vacia():
            raise ValueError('La cola esta vacia')
        return self.items.pop(0)

    def esta_vacia(self):
        '''Devuelve 
        True si la cola esta vacia, 
        False si no.'''
        return len(self.items) == 0
    
class TorreDeControl():
    def __init__(self):
        self.partidas = Cola()
        self.arribos = Cola()

    def esperan_aterrizar(self):
        return not self.arribos
    def esperan_irse(self):
        return not self.partidas


    def nuevo_arribo(self,vuelo):
        self.arribos.encolar(vuelo)
    def nueva_partida(self, vuelo):
        self.partidas.encolar(vuelo)
    def asignar_pista(self):
        if self.esperan_aterrizar:
            atterizado = self.arribos.desencolar()
            print(f'{atterizado} ha aterrizado')
        elif self.esperan_irse:
            despegue = self.partidas.desencolar()
            print(f'{despegue} ha despegado')
        else:
            raise ValueError('La cola esta vacia')
    def ver_estado(self):
        print(f'Vuelos esperando para aterrizar:', end = ',')
        for i in self.arribos.items:
            print(i, end = '')
        print(f'\nVuelos esperando para despegar:', end = ',')
        for i in self.partidas.items:
            print(i, end = '')
            
    
            
            
        