#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 19:02:38 2020

@author: gonik
"""
import os
import sys

os.chdir('/home/gonik/Documents/python_unsam/Ejercicios/')
sys.path.append('/home/gonik/Documents/repo_curso/curso_python/edu/')
from clase_6 import fileparse
from clase_8 import lote
with open('Data/camion.csv') as lineas:
     camion_dicts = fileparse.parse_csv(lineas, select = ['nombre', 'cajones', 'precio'], types = [str, int, float])
camion = [lote.Lote(d['nombre'],d['cajones'],d['precio']) for d in camion_dicts]
suma = sum([c.costo() for c in camion])