#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 18:40:04 2020

@author: gonik
"""

class Lote():
    def __init__(self,nombre,cajones,precio):
        self.nombre = nombre
        self.cajones = cajones
        self.precio = precio
    def costo(self):
        return self.cajones * self.precio
    def vender(self, a_vender):
        self.cajones-= a_vender
    def __repr__(self):
        return f'Lote({self.nombre},{self.cajones},{self.precio})'
    