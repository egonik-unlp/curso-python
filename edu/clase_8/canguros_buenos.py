#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 12:22:39 2020

@author: gonik
"""
class Canguro:
    def __init__(self, name):
        self.name = name
        self.contenido_marsupio = []
        
    def meter_en_marsupio(self, obj):
        '''
        Agrega objetos a la lista 'contenido_marsupio'. 
        '''
        self.contenido_marsupio.append(obj)
    
    def __str__(self):
        return f'{self.name} tiene en su marsupio: {self.contenido_marsupio}'





#%% 'Canguro Malo', ya corregido
class Canguro:
    """Un Canguro es un marsupial."""
    
    def __init__(self, nombre):
        """Inicializar los contenidos del marsupio.

        nombre: string
        contenido: contenido inicial del marsupio, lista.
        """
        
        self.nombre = nombre
        self.contenido_marsupio = []

    def __str__(self):
        """devuelve una representación como cadena de este Canguro.
        """
        t = [ self.nombre + ' tiene en su marsupio:' ]
        for obj in self.contenido_marsupio:
            s = '    ' + object.__str__(obj)
            t.append(s)
        return '\n'.join(t)

    def meter_en_marsupio(self, item):
        """Agrega un nuevo item al marsupio.

        item: objecto a ser agregado
        """
        self.contenido_marsupio.append(item)

'''
El error ocurría debido a que si le programadxr no inicializaba lx cangurx 
con algún contenido, se utilizaba por defecto una lista vacía 'contenido', común a 
todas las instancias de la clase.

Para corregir este comportamiento, cambié el método __init__ para que con cada 
instancia se genere una lista vacía única.
'''

#%%
madre_canguro = Canguro('Madre')
cangurito = Canguro('gurito')
madre_canguro.meter_en_marsupio('billetera')
madre_canguro.meter_en_marsupio('llaves del auto')
madre_canguro.meter_en_marsupio(cangurito)

print(madre_canguro)
print(cangurito)