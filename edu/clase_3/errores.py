#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 16:53:42 2020

@author: gonik
"""

def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        else:
            return False
        i += 1
'''
El problema que tiene el primer inciso es que si bien está pensado para iterar sobre toda la expresión 
que se le pase como variable, esta escrita de tal forma que le brinda un punto de salida desde la primera 
iteración, entonces solo evalúa si el primer caracter es 'a'
'''


tiene_a('UNSAM 2020')
tiene_a('abracadabra')
tiene_a('La novela 1984 de George Orwell')

## una posible solucion seria 
def tiene_a(expresion):
    n = len(expresion)
    i = 0
    hay_a = False
    while i<n:
        if expresion[i] == 'a':
            hay_a = True
        i += 1
    return hay_a


#%%
def tiene_a(expresion):
    n = len(expresion)
    i = 0
    while i<n:
        if expresion[i] == 'a':
            return True
        i += 1
    return False

tiene_a('UNSAM 2020')
tiene_a('La novela 1984 de George Orwell')
        
"""
En este caso, esta función tiene varios problemas
-Faltan los ':' en las estructuras de control
-La expresión en el if no da como resultado una variable booleana porque no se hace ninguna comparación.
esta escrito '='en lugar de '=='
-Una de las salidas devuelve una expresion no definida 'Falso' en lugar del boolean 'False'

"""
#%%
def tiene_uno(expresion):
    n = len(expresion)
    i = 0
    tiene = False
    while (i<n) and not tiene:
        if expresion[i] == '1':
            tiene = True
        i += 1
    return tiene


tiene_uno('UNSAM 2020')
tiene_uno('La novela 1984 de George Orwell')
tiene_uno(1984)
"""
El problema en este caso es que la función retorna True solamente si la expresión que le pasamos tiene
1 pero en un tipo string. Cuando la expresión que se le pasa es 1984, en este caso el 1 es un 'int'. Para solucionar
el problema se podria agregar la linea expresion = str(expresion) para obligar a 
que todos los inputs de la funcion se traten como string-
"""
#%%
def suma(a,b):
    c = a + b
    
a = 2
b = 3
c = suma(a,b)
print(f"La suma da {a} + {b} = {c}")

"""
La suma no da lo que debería porque la función no tiene punto de salida, y a su vez la variable 'c' tiene 
existencia solo dentro de la función, entonces suma(a,b) retorna nonetype. Para solucionar el 
problema se podría agregar return c a la funcion
"""
#%%
import csv
import os
from pprint import pprint

os.chdir('/home/gonik/Documents/python_unsam/Ejercicios')
def appendar(nodo,lista):
    lista.append(nodo)
    return lista

def leer_camion(nombre_archivo):
    camion=[]
    registro={}
    with open(nombre_archivo,"rt") as f:
        filas = csv.reader(f)
        encabezado = next(filas)
        for i, fila in enumerate(filas):
            registro={}
            registro[encabezado[0]] = fila[0]
            registro[encabezado[1]] = int(fila[1])
            registro[encabezado[2]] = float(fila[2])
<<<<<<< HEAD
            appendar(registro,mionca)
           
    print(mionca)
    return mionca
=======
            camion.append(registro)
    return camion
>>>>>>> 1e262b7cba88b47576bdb379b37a129ab356e097

camion = leer_camion("Data/camion.csv")
pprint(camion)
 """
En este caso el problema es que a lo largo de todo el programa se crea un solo 
objeto diccionario, que es sobreescrito en cada iteración del for y por tanto
cada nodo de la lista a medida que se ejecuta el for es sobreescrito, para solucionar
el problema, se podría generara el registro nuevamente en cada iteracion del for
 """
