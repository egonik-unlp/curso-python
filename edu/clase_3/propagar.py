#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 20:02:57 2020

@author: gonik
"""

l1 = [ 1, 0, 0,-1, 1, 0, 0, 0,-1, 0, 1, 0, 0]
l2 = [ 0, 0, 0,-1, 1, 1, 1, 1,-1, 1, 1, 1, 1]
l3 = [ 0, 0, 0, 1, 0, 0]
l4 = [ 1, 1, 1, 1, 1, 1]


def sumadeapares(lista):
    hay = False
    for i in range(len(lista)):
        if i < len(lista) - 1:
            if lista[i] + lista[i+1] == 1:
                hay = True
        else:
            if lista[i] + lista[i-1] == 1:
                hay = True
    return hay


def propagar(lista):
    while sumadeapares(lista):
        for i in range(len(lista)):
            if i < len(lista) -1:
                if lista[i] == 0 and lista[i+1] ==1:
                    lista[i] = 1
            else:
                if lista[i] == 0 and lista[i-1] ==1:
                    lista[i] = 1
        for i in range(len(lista)-1, 0, -1):   
            if lista[i] == 1 and lista[i-1] == 0:
                lista[i-1] = 1
        
        if lista[1] == 1:
            lista[0] = 1
    return lista


propagada = propagar(l1)
