#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 19:47:33 2020

@author: gonik
"""
import string

l = list(string.digits)
l2 = ['Bogotá', 'Rosario', 'Santiago', 'San Fernando', 'San Miguel']
l1 = [int(i) for i in l] 
def invertir_lista(lista):
    invertida = []
    for num in range(len(lista)):
        aux = lista[-1]
        invertida.append(aux)    
        lista.pop()
    return invertida


l3 = invertir_lista(l1)
l4 = invertir_lista(l2)